
Contact:
Janne Savukoski
janne@savukoski.name
+358405684246


                    Part name: ISPAdapter
                     Revision: 1
                   Board size: 68mm x 38mm
              Board thickness: 1.6mm
                   Board type: Two layer
       Outer copper thickness:
       Inner copper thickness:
Solder mask color (top & bot):
 Silkscreen color (top & bot):


No special instructions.


Layer order:

TOP:    ISPAdapter.GTL
BOTTOM: ISPAdapter.GBL


Other files:

ISPAdapter.GML  Outline
ISPAdapter.GTS  Solder mask TOP
ISPAdapter.GBS  Solder mask BOTTOM
ISPAdapter.GTO  Silkscreen TOP
ISPAdapter.drd  Excellon drill
ISPAdapter.drl  Drill rack


Thank you!

Regards,
Janne Savukoski
