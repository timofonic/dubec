<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="5" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="14" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="yes" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dubec">
<description>By Janne Savukoski 2014</description>
<packages>
<package name="0402+">
<smd name="A" x="-0.55" y="0" dx="0.5" dy="0.7" layer="1" stop="no"/>
<smd name="C" x="0.55" y="0" dx="0.5" dy="0.7" layer="1" stop="no"/>
<text x="-0.85" y="0.4" size="0.3" layer="25" align="top-left">&gt;NAME</text>
<text x="-0.85" y="-0.4" size="0.3" layer="27">&gt;VALUE</text>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.05" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.05" layer="51"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.05" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.05" layer="51"/>
<text x="1" y="-0.7" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
<wire x1="-1" y1="0.55" x2="1" y2="0.55" width="0.13" layer="21"/>
<wire x1="1" y1="0.55" x2="1" y2="-0.55" width="0.13" layer="21"/>
<wire x1="1" y1="-0.55" x2="-1" y2="-0.55" width="0.13" layer="21"/>
<wire x1="-1" y1="-0.55" x2="-1" y2="0.55" width="0.13" layer="21"/>
<rectangle x1="-0.85" y1="-0.4" x2="-0.25" y2="0.4" layer="29"/>
<rectangle x1="0.25" y1="-0.4" x2="0.85" y2="0.4" layer="29"/>
</package>
<package name="0402">
<description>IPC-SM-782 RES/CAP; narrowed by 2mm</description>
<smd name="1" x="-0.5" y="0" dx="0.4" dy="0.7" layer="1" stop="no"/>
<smd name="2" x="0.5" y="0" dx="0.4" dy="0.7" layer="1" stop="no"/>
<text x="-0.75" y="0.35" size="0.254" layer="25" align="top-left">&gt;NAME</text>
<text x="-0.75" y="-0.35" size="0.254" layer="27">&gt;VALUE</text>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.05" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.05" layer="51"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.05" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.05" layer="51"/>
<wire x1="-0.9" y1="0.55" x2="0.9" y2="0.55" width="0.13" layer="21"/>
<wire x1="0.9" y1="0.55" x2="0.9" y2="-0.55" width="0.13" layer="21"/>
<wire x1="0.9" y1="-0.55" x2="-0.9" y2="-0.55" width="0.13" layer="21"/>
<wire x1="-0.9" y1="-0.55" x2="-0.9" y2="0.55" width="0.13" layer="21"/>
<rectangle x1="-0.75" y1="-0.4" x2="-0.25" y2="0.4" layer="29"/>
<rectangle x1="0.25" y1="-0.4" x2="0.75" y2="0.4" layer="29"/>
</package>
<package name="1210">
<description>IPC-SM-782 RES/CAP</description>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.1" y="1.05" size="0.6" layer="25" align="top-left">&gt;NAME</text>
<text x="-2.1" y="-1.05" size="0.6" layer="27">&gt;VALUE</text>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.127" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.127" layer="51"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.127" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.127" layer="51"/>
<wire x1="-2.4" y1="1.55" x2="-2.4" y2="-1.55" width="0.13" layer="21"/>
<wire x1="-2.4" y1="-1.55" x2="2.4" y2="-1.55" width="0.13" layer="21"/>
<wire x1="2.4" y1="-1.55" x2="2.4" y2="1.55" width="0.13" layer="21"/>
<wire x1="2.4" y1="1.55" x2="-2.4" y2="1.55" width="0.13" layer="21"/>
</package>
<package name="JST-SH_6-TOP">
<smd name="4" x="0.5" y="-1.3" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="-0.5" y="-1.3" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.5" y="-1.3" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="2.5" y="-1.3" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-1.5" y="-1.3" dx="0.6" dy="1.55" layer="1"/>
<smd name="1" x="-2.5" y="-1.3" dx="0.6" dy="1.55" layer="1"/>
<smd name="X2" x="3.8" y="1.2" dx="1.2" dy="1.8" layer="1"/>
<smd name="X1" x="-3.8" y="1.2" dx="1.2" dy="1.8" layer="1"/>
<wire x1="-3.15" y1="-0.9" x2="-4" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-4" y1="-0.9" x2="-4" y2="0" width="0.127" layer="21"/>
<wire x1="3.15" y1="-0.9" x2="4" y2="-0.9" width="0.127" layer="21"/>
<wire x1="4" y1="-0.9" x2="4" y2="0" width="0.127" layer="21"/>
<wire x1="-2.9" y1="2" x2="2.9" y2="2" width="0.127" layer="21"/>
<text x="-4" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-4" y="-4.2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="2525CZ">
<smd name="P$1" x="-2.7686" y="0" dx="1.8288" dy="3.429" layer="1"/>
<smd name="P$2" x="2.7686" y="0" dx="1.8288" dy="3.429" layer="1"/>
<text x="-2.85" y="2.65" size="1" layer="25" align="top-left">&gt;NAME</text>
<text x="-2.85" y="-2.65" size="1" layer="27">&gt;VALUE</text>
<wire x1="-3.429" y1="2.1066" x2="-3.429" y2="3.25" width="0.13" layer="21"/>
<wire x1="-3.429" y1="3.25" x2="3.429" y2="3.25" width="0.13" layer="21"/>
<wire x1="3.429" y1="3.25" x2="3.429" y2="2.1066" width="0.13" layer="21"/>
<wire x1="-3.429" y1="-3.25" x2="-3.429" y2="-2.1066" width="0.13" layer="21"/>
<wire x1="3.429" y1="-3.25" x2="3.429" y2="-2.1066" width="0.13" layer="21"/>
<wire x1="-3.429" y1="-3.25" x2="3.429" y2="-3.25" width="0.13" layer="21"/>
</package>
<package name="10-MLF">
<smd name="8" x="1.35" y="0" dx="0.5" dy="0.25" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="9" x="1.35" y="0.5" dx="0.5" dy="0.25" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="7" x="1.35" y="-0.5" dx="0.5" dy="0.25" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="10" x="1.35" y="1" dx="0.5" dy="0.25" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="6" x="1.35" y="-1" dx="0.5" dy="0.25" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="3" x="-1.35" y="0" dx="0.5" dy="0.25" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="2" x="-1.35" y="0.5" dx="0.5" dy="0.25" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="1" x="-1.35" y="1" dx="0.5" dy="0.25" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="4" x="-1.35" y="-0.5" dx="0.5" dy="0.25" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="5" x="-1.35" y="-1" dx="0.5" dy="0.25" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="11" x="0" y="0" dx="0.3" dy="0.6" layer="1" stop="no" thermals="no" cream="no"/>
<text x="-1.3" y="-1.3" size="0.4" layer="25" rot="R90" align="top-left">&gt;NAME</text>
<text x="1.3" y="-1.3" size="0.4" layer="27" rot="R90">&gt;VALUE</text>
<wire x1="-1.5" y1="1.5" x2="-1.3" y2="1.5" width="0.13" layer="21"/>
<wire x1="-1.3" y1="1.5" x2="-0.9" y2="1.5" width="0.13" layer="21"/>
<wire x1="-0.9" y1="1.5" x2="1.5" y2="1.5" width="0.13" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.3" width="0.13" layer="21"/>
<wire x1="1.5" y1="-1.3" x2="1.5" y2="-1.5" width="0.13" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.13" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.3" width="0.13" layer="21"/>
<wire x1="-1.3" y1="1.5" x2="-0.9" y2="1.5" width="0.4" layer="21" curve="-180" cap="flat"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="1.3" width="0.13" layer="21"/>
<rectangle x1="-0.2" y1="-0.35" x2="0.2" y2="0.35" layer="29"/>
<rectangle x1="-1.6" y1="-1.2" x2="-1.1" y2="1.2" layer="29"/>
<rectangle x1="1.1" y1="-1.2" x2="1.6" y2="1.2" layer="29"/>
<rectangle x1="-1.575" y1="0.9" x2="-1.275" y2="1.1" layer="31"/>
<rectangle x1="-1.575" y1="0.4" x2="-1.275" y2="0.6" layer="31"/>
<rectangle x1="-1.575" y1="-0.1" x2="-1.275" y2="0.1" layer="31"/>
<rectangle x1="-1.575" y1="-0.6" x2="-1.275" y2="-0.4" layer="31"/>
<rectangle x1="-1.575" y1="-1.1" x2="-1.275" y2="-0.9" layer="31"/>
<rectangle x1="1.275" y1="0.9" x2="1.575" y2="1.1" layer="31"/>
<rectangle x1="1.275" y1="0.4" x2="1.575" y2="0.6" layer="31"/>
<rectangle x1="1.275" y1="-0.1" x2="1.575" y2="0.1" layer="31"/>
<rectangle x1="1.275" y1="-0.6" x2="1.575" y2="-0.4" layer="31"/>
<rectangle x1="1.275" y1="-1.1" x2="1.575" y2="-0.9" layer="31"/>
</package>
<package name="DF13-6P">
<pad name="3" x="-0.625" y="0" drill="0.45" shape="long" rot="R90" stop="no"/>
<pad name="4" x="0.625" y="0" drill="0.45" shape="long" rot="R90" stop="no"/>
<pad name="5" x="1.875" y="0" drill="0.45" shape="long" rot="R90" stop="no"/>
<pad name="6" x="3.125" y="0" drill="0.45" shape="long" rot="R90" stop="no"/>
<pad name="2" x="-1.875" y="0" drill="0.45" shape="long" rot="R90" stop="no"/>
<pad name="1" x="-3.125" y="0" drill="0.45" shape="long" rot="R90" stop="no"/>
<wire x1="-4.575" y1="-1.7" x2="-4.575" y2="1.7" width="0.127" layer="21"/>
<wire x1="-4.575" y1="1.7" x2="4.575" y2="1.7" width="0.127" layer="21"/>
<wire x1="4.575" y1="1.7" x2="4.575" y2="-1.7" width="0.127" layer="21"/>
<wire x1="4.575" y1="-1.7" x2="-4.575" y2="-1.7" width="0.127" layer="21"/>
<text x="-4.6" y="2.2" size="1" layer="25">&gt;NAME</text>
<text x="-4.6" y="-3.2" size="1" layer="27">&gt;VALUE</text>
<polygon width="0.127" layer="29">
<vertex x="-1.05" y="-0.3"/>
<vertex x="-1.05" y="0.475" curve="-90"/>
<vertex x="-0.625" y="0.9" curve="-90"/>
<vertex x="-0.225" y="0.5"/>
<vertex x="-0.225" y="-0.3"/>
<vertex x="-0.225" y="-0.5" curve="-90"/>
<vertex x="-0.625" y="-0.9" curve="-90"/>
<vertex x="-1.05" y="-0.475"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="0.2" y="-0.3"/>
<vertex x="0.2" y="0.475" curve="-90"/>
<vertex x="0.625" y="0.9" curve="-90"/>
<vertex x="1.025" y="0.5"/>
<vertex x="1.025" y="-0.3"/>
<vertex x="1.025" y="-0.5" curve="-90"/>
<vertex x="0.625" y="-0.9" curve="-90"/>
<vertex x="0.2" y="-0.475"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.45" y="-0.3"/>
<vertex x="1.45" y="0.475" curve="-90"/>
<vertex x="1.875" y="0.9" curve="-90"/>
<vertex x="2.275" y="0.5"/>
<vertex x="2.275" y="-0.3"/>
<vertex x="2.275" y="-0.5" curve="-90"/>
<vertex x="1.875" y="-0.9" curve="-90"/>
<vertex x="1.45" y="-0.475"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="2.7" y="-0.3"/>
<vertex x="2.7" y="0.475" curve="-90"/>
<vertex x="3.125" y="0.9" curve="-90"/>
<vertex x="3.525" y="0.5"/>
<vertex x="3.525" y="-0.3"/>
<vertex x="3.525" y="-0.5" curve="-90"/>
<vertex x="3.125" y="-0.9" curve="-90"/>
<vertex x="2.7" y="-0.475"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-2.3" y="-0.3"/>
<vertex x="-2.3" y="0.475" curve="-90"/>
<vertex x="-1.875" y="0.9" curve="-90"/>
<vertex x="-1.475" y="0.5"/>
<vertex x="-1.475" y="-0.3"/>
<vertex x="-1.475" y="-0.5" curve="-90"/>
<vertex x="-1.875" y="-0.9" curve="-90"/>
<vertex x="-2.3" y="-0.475"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.55" y="-0.3"/>
<vertex x="-3.55" y="0.475" curve="-90"/>
<vertex x="-3.125" y="0.9" curve="-90"/>
<vertex x="-2.725" y="0.5"/>
<vertex x="-2.725" y="-0.3"/>
<vertex x="-2.725" y="-0.5" curve="-90"/>
<vertex x="-3.125" y="-0.9" curve="-90"/>
<vertex x="-3.55" y="-0.475"/>
</polygon>
<wire x1="-3.675" y1="0.525" x2="-3.35" y2="0.975" width="0.127" layer="41" curve="-60.577826"/>
<wire x1="-2.575" y1="0.525" x2="-2.9" y2="0.975" width="0.127" layer="41" curve="60.577826"/>
<wire x1="-2.575" y1="-0.55" x2="-2.9" y2="-1" width="0.127" layer="41" curve="-60.577826"/>
<wire x1="-3.675" y1="-0.55" x2="-3.35" y2="-1" width="0.127" layer="41" curve="60.577826"/>
<wire x1="-3.675" y1="0.525" x2="-3.675" y2="-0.55" width="0.127" layer="41"/>
<wire x1="-2.575" y1="0.525" x2="-2.575" y2="-0.55" width="0.127" layer="41"/>
<wire x1="-2.425" y1="0.525" x2="-2.1" y2="0.975" width="0.127" layer="41" curve="-60.577826"/>
<wire x1="-1.325" y1="0.525" x2="-1.65" y2="0.975" width="0.127" layer="41" curve="60.577826"/>
<wire x1="-1.325" y1="-0.55" x2="-1.65" y2="-1" width="0.127" layer="41" curve="-60.577826"/>
<wire x1="-2.425" y1="-0.55" x2="-2.1" y2="-1" width="0.127" layer="41" curve="60.577826"/>
<wire x1="-2.425" y1="0.525" x2="-2.425" y2="-0.55" width="0.127" layer="41"/>
<wire x1="-1.325" y1="0.525" x2="-1.325" y2="-0.55" width="0.127" layer="41"/>
<wire x1="-1.175" y1="0.525" x2="-0.85" y2="0.975" width="0.127" layer="41" curve="-60.577826"/>
<wire x1="-0.075" y1="0.525" x2="-0.4" y2="0.975" width="0.127" layer="41" curve="60.577826"/>
<wire x1="-0.075" y1="-0.55" x2="-0.4" y2="-1" width="0.127" layer="41" curve="-60.577826"/>
<wire x1="-1.175" y1="-0.55" x2="-0.85" y2="-1" width="0.127" layer="41" curve="60.577826"/>
<wire x1="-1.175" y1="0.525" x2="-1.175" y2="-0.55" width="0.127" layer="41"/>
<wire x1="-0.075" y1="0.525" x2="-0.075" y2="-0.55" width="0.127" layer="41"/>
<wire x1="0.075" y1="0.525" x2="0.4" y2="0.975" width="0.127" layer="41" curve="-60.577826"/>
<wire x1="1.175" y1="0.525" x2="0.85" y2="0.975" width="0.127" layer="41" curve="60.577826"/>
<wire x1="1.175" y1="-0.55" x2="0.85" y2="-1" width="0.127" layer="41" curve="-60.577826"/>
<wire x1="0.075" y1="-0.55" x2="0.4" y2="-1" width="0.127" layer="41" curve="60.577826"/>
<wire x1="0.075" y1="0.525" x2="0.075" y2="-0.55" width="0.127" layer="41"/>
<wire x1="1.175" y1="0.525" x2="1.175" y2="-0.55" width="0.127" layer="41"/>
<wire x1="1.325" y1="0.525" x2="1.65" y2="0.975" width="0.127" layer="41" curve="-60.577826"/>
<wire x1="2.425" y1="0.525" x2="2.1" y2="0.975" width="0.127" layer="41" curve="60.577826"/>
<wire x1="2.425" y1="-0.55" x2="2.1" y2="-1" width="0.127" layer="41" curve="-60.577826"/>
<wire x1="1.325" y1="-0.55" x2="1.65" y2="-1" width="0.127" layer="41" curve="60.577826"/>
<wire x1="1.325" y1="0.525" x2="1.325" y2="-0.55" width="0.127" layer="41"/>
<wire x1="2.425" y1="0.525" x2="2.425" y2="-0.55" width="0.127" layer="41"/>
<wire x1="2.575" y1="0.525" x2="2.9" y2="0.975" width="0.127" layer="41" curve="-60.577826"/>
<wire x1="3.675" y1="0.525" x2="3.35" y2="0.975" width="0.127" layer="41" curve="60.577826"/>
<wire x1="3.675" y1="-0.55" x2="3.35" y2="-1" width="0.127" layer="41" curve="-60.577826"/>
<wire x1="2.575" y1="-0.55" x2="2.9" y2="-1" width="0.127" layer="41" curve="60.577826"/>
<wire x1="2.575" y1="0.525" x2="2.575" y2="-0.55" width="0.127" layer="41"/>
<wire x1="3.675" y1="0.525" x2="3.675" y2="-0.55" width="0.127" layer="41"/>
</package>
<package name="H2MM-3X2">
<text x="-2.7" y="1.5" size="0.8" layer="25" ratio="10" align="top-left">&gt;NAME</text>
<text x="-2.7" y="-1.5" size="0.8" layer="27">&gt;VALUE</text>
<rectangle x1="-2.2" y1="-1.2" x2="-1.8" y2="-0.8" layer="51"/>
<rectangle x1="-0.2" y1="-1.2" x2="0.2" y2="-0.8" layer="51"/>
<rectangle x1="1.8" y1="-1.2" x2="2.2" y2="-0.8" layer="51"/>
<rectangle x1="1.8" y1="0.8" x2="2.2" y2="1.2" layer="51"/>
<rectangle x1="-0.2" y1="0.8" x2="0.2" y2="1.2" layer="51"/>
<rectangle x1="-2.2" y1="0.8" x2="-1.8" y2="1.2" layer="51"/>
<wire x1="-3" y1="2" x2="3" y2="2" width="0.13" layer="21"/>
<wire x1="3" y1="2" x2="3" y2="-2" width="0.13" layer="21"/>
<wire x1="3" y1="-2" x2="-3" y2="-2" width="0.13" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="2" width="0.13" layer="21"/>
<pad name="2" x="-2" y="1" drill="0.8" diameter="1.25" stop="no"/>
<pad name="4" x="0" y="1" drill="0.8" diameter="1.25" stop="no"/>
<pad name="6" x="2" y="1" drill="0.8" diameter="1.25" stop="no"/>
<pad name="1" x="-2" y="-1" drill="0.8" diameter="1.25" shape="square" stop="no"/>
<pad name="3" x="0" y="-1" drill="0.8" diameter="1.25" stop="no"/>
<pad name="5" x="2" y="-1" drill="0.8" diameter="1.25" stop="no"/>
<circle x="-2" y="1" radius="0.291546875" width="0.8" layer="29"/>
<circle x="-2" y="1" radius="0.291546875" width="0.8" layer="30"/>
<circle x="0" y="1" radius="0.291546875" width="0.8" layer="30"/>
<circle x="0" y="1" radius="0.291546875" width="0.8" layer="29"/>
<circle x="2" y="1" radius="0.291546875" width="0.8" layer="30"/>
<circle x="2" y="1" radius="0.291546875" width="0.8" layer="29"/>
<circle x="2" y="-1" radius="0.291546875" width="0.8" layer="30"/>
<circle x="2" y="-1" radius="0.291546875" width="0.8" layer="29"/>
<circle x="0" y="-1" radius="0.291546875" width="0.8" layer="30"/>
<circle x="0" y="-1" radius="0.291546875" width="0.8" layer="29"/>
<rectangle x1="-2.7" y1="-1.7" x2="-1.3" y2="-0.3" layer="29"/>
<rectangle x1="-2.7" y1="-1.7" x2="-1.3" y2="-0.3" layer="30"/>
</package>
<package name="0805">
<description>IPC-SM-782 RES/CAP</description>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.5" layer="1" stop="no" cream="no"/>
<text x="-1.3" y="0.7" size="0.5" layer="25" align="top-left">&gt;NAME</text>
<text x="-1.3" y="-0.7" size="0.5" layer="27">&gt;VALUE</text>
<wire x1="-1.075" y1="0.7" x2="1.075" y2="0.7" width="0.05" layer="51"/>
<wire x1="1.075" y1="0.7" x2="1.075" y2="-0.7" width="0.05" layer="51"/>
<wire x1="1.075" y1="-0.7" x2="-1.075" y2="-0.7" width="0.05" layer="51"/>
<wire x1="-1.075" y1="-0.7" x2="-1.075" y2="0.7" width="0.05" layer="51"/>
<wire x1="-1.5" y1="0.95" x2="1.5" y2="0.95" width="0.13" layer="21"/>
<wire x1="1.5" y1="0.95" x2="1.5" y2="-0.95" width="0.13" layer="21"/>
<wire x1="1.5" y1="-0.95" x2="-1.5" y2="-0.95" width="0.13" layer="21"/>
<wire x1="-1.5" y1="-0.95" x2="-1.5" y2="0.95" width="0.13" layer="21"/>
<rectangle x1="-1.35" y1="-0.8" x2="-0.45" y2="0.8" layer="29"/>
<rectangle x1="0.45" y1="-0.8" x2="1.35" y2="0.8" layer="29"/>
<rectangle x1="-1.175" y1="-0.65" x2="-0.5" y2="0.65" layer="31"/>
<rectangle x1="0.5" y1="-0.65" x2="1.175" y2="0.65" layer="31"/>
</package>
<package name="0603">
<smd name="1" x="-0.75" y="0" dx="0.7" dy="1" layer="1" stop="no"/>
<smd name="2" x="0.75" y="0" dx="0.7" dy="1" layer="1" stop="no"/>
<text x="-1.15" y="0.5" size="0.4" layer="25" align="top-left">&gt;NAME</text>
<text x="-1.15" y="-0.5" size="0.4" layer="27">&gt;VALUE</text>
<wire x1="-0.85" y1="0.475" x2="0.85" y2="0.475" width="0.05" layer="51"/>
<wire x1="0.85" y1="0.475" x2="0.85" y2="-0.475" width="0.05" layer="51"/>
<wire x1="0.85" y1="-0.475" x2="-0.85" y2="-0.475" width="0.05" layer="51"/>
<wire x1="-0.85" y1="-0.475" x2="-0.85" y2="0.475" width="0.05" layer="51"/>
<wire x1="-1.3" y1="0.7" x2="1.3" y2="0.7" width="0.13" layer="21"/>
<wire x1="1.3" y1="0.7" x2="1.3" y2="-0.7" width="0.13" layer="21"/>
<wire x1="1.3" y1="-0.7" x2="-1.3" y2="-0.7" width="0.13" layer="21"/>
<wire x1="-1.3" y1="-0.7" x2="-1.3" y2="0.7" width="0.13" layer="21"/>
<rectangle x1="-1.15" y1="-0.55" x2="-0.35" y2="0.55" layer="29"/>
<rectangle x1="0.35" y1="-0.55" x2="1.15" y2="0.55" layer="29"/>
</package>
<package name="0603+">
<description>IPC-SM-782 RES/CAP</description>
<smd name="A" x="-0.8" y="0" dx="0.8" dy="1" layer="1" stop="no"/>
<smd name="C" x="0.8" y="0" dx="0.8" dy="1" layer="1" stop="no"/>
<text x="-1.2" y="0.5" size="0.4" layer="25" align="top-left">&gt;NAME</text>
<text x="-1.2" y="-0.5" size="0.4" layer="27">&gt;VALUE</text>
<text x="1.41" y="-0.85" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
<wire x1="-0.85" y1="0.475" x2="0.85" y2="0.475" width="0.05" layer="51"/>
<wire x1="0.85" y1="0.475" x2="0.85" y2="-0.475" width="0.05" layer="51"/>
<wire x1="0.85" y1="-0.475" x2="-0.85" y2="-0.475" width="0.05" layer="51"/>
<wire x1="-0.85" y1="-0.475" x2="-0.85" y2="0.475" width="0.05" layer="51"/>
<rectangle x1="-1.25" y1="-0.55" x2="-0.35" y2="0.55" layer="29"/>
<rectangle x1="0.35" y1="-0.55" x2="1.25" y2="0.55" layer="29"/>
<wire x1="-1.35" y1="0.65" x2="1.35" y2="0.65" width="0.13" layer="49"/>
<wire x1="1.35" y1="0.65" x2="1.35" y2="-0.65" width="0.13" layer="49"/>
<wire x1="1.35" y1="-0.65" x2="-1.35" y2="-0.65" width="0.13" layer="49"/>
<wire x1="-1.35" y1="-0.65" x2="-1.35" y2="0.65" width="0.13" layer="49"/>
</package>
<package name="DFN-16">
<smd name="12" x="0.25" y="1.95" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="11" x="0.75" y="1.95" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="10" x="1.25" y="1.95" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="9" x="1.75" y="2" dx="0.25" dy="0.6" layer="1" stop="no" cream="no"/>
<smd name="6" x="0.75" y="-2" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="7" x="1.25" y="-2" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="8" x="1.75" y="-2" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="1" x="-1.75" y="-2" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="2" x="-1.25" y="-2" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="3" x="-0.75" y="-2" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="4-5" x="0" y="-2" dx="0.7" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="16" x="-1.75" y="1.95" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="15" x="-1.25" y="1.95" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="14" x="-0.75" y="1.95" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="13" x="-0.25" y="1.95" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<text x="-2.05" y="1.5" size="0.7" layer="25" align="top-left">&gt;NAME</text>
<text x="-2.05" y="-1.5" size="0.7" layer="27">&gt;VALUE</text>
<circle x="-1.2" y="0" radius="0.45" width="0.3" layer="29"/>
<circle x="0" y="0" radius="0.45" width="0.3" layer="29"/>
<circle x="1.2" y="0" radius="0.45" width="0.3" layer="29"/>
<polygon width="0.3" layer="29">
<vertex x="1.2" y="0.7"/>
<vertex x="2" y="0.7"/>
<vertex x="2" y="-0.3"/>
</polygon>
<smd name="17" x="-0.7" y="0" dx="3.1" dy="2.44" layer="1" stop="no" cream="no"/>
<smd name="18" x="1.5" y="-0.17" dx="1.3" dy="2.1" layer="1" rot="R180" stop="no" cream="no"/>
<wire x1="2.1" y1="2" x2="2.5" y2="2" width="0.13" layer="21"/>
<wire x1="2.5" y1="2" x2="2.5" y2="-2" width="0.13" layer="21"/>
<wire x1="2.5" y1="-2" x2="2.1" y2="-2" width="0.13" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="-1.8" width="0.13" layer="21"/>
<wire x1="-2.5" y1="-1.8" x2="-2.5" y2="-1.4" width="0.13" layer="21"/>
<wire x1="-2.5" y1="-1.4" x2="-2.5" y2="2" width="0.13" layer="21"/>
<wire x1="-2.5" y1="2" x2="-2.2" y2="2" width="0.13" layer="21"/>
<wire x1="-2.5" y1="-1.8" x2="-2.5" y2="-1.4" width="0.3" layer="21" curve="-180" cap="flat"/>
<wire x1="-2.5" y1="-2" x2="-2.2" y2="-2" width="0.13" layer="21"/>
<circle x="-0.6" y="0" radius="0.14141875" width="0.3" layer="31"/>
<circle x="0.6" y="0" radius="0.14141875" width="0.3" layer="31"/>
<circle x="0" y="-0.6" radius="0.14141875" width="0.3" layer="31"/>
<circle x="-1.2" y="-0.6" radius="0.14141875" width="0.3" layer="31"/>
<circle x="-1.2" y="0.6" radius="0.14141875" width="0.3" layer="31"/>
<circle x="-1.8" y="0" radius="0.14141875" width="0.3" layer="31"/>
<circle x="0" y="0.6" radius="0.14141875" width="0.3" layer="31"/>
<circle x="1.2" y="-0.6" radius="0.14141875" width="0.3" layer="31"/>
<wire x1="-2.1" y1="-0.35" x2="-1.5" y2="-1.05" width="0.3" layer="29" curve="-180"/>
<wire x1="-1.5" y1="1.05" x2="-2.1" y2="0.35" width="0.3" layer="29" curve="-180"/>
<wire x1="-0.9" y1="-1.05" x2="-0.3" y2="-1.05" width="0.3" layer="29" curve="-278.797411"/>
<wire x1="0.3" y1="-1.05" x2="0.9" y2="-1.05" width="0.3" layer="29" curve="-278.797411"/>
<wire x1="-0.3" y1="1.05" x2="-0.9" y2="1.05" width="0.3" layer="29" curve="-278.797411"/>
<wire x1="2" y1="-0.3" x2="1.5" y2="-1.05" width="0.3" layer="29" curve="165.963757"/>
<wire x1="0.3" y1="1.05" x2="1.05" y2="0.75" width="0.3" layer="29" curve="235.738897"/>
<circle x="1.8" y="0" radius="0.14141875" width="0.3" layer="31"/>
<circle x="1.2" y="0.6" radius="0.14141875" width="0.3" layer="31"/>
<rectangle x1="-2" y1="-2.35" x2="2" y2="-1.65" layer="29"/>
<rectangle x1="-2" y1="1.6" x2="1.45" y2="2.3" layer="29"/>
<rectangle x1="1.45" y1="1.7" x2="2" y2="2.3" layer="29"/>
<rectangle x1="-1.85" y1="1.875" x2="-1.65" y2="2.275" layer="31"/>
<rectangle x1="-1.35" y1="1.875" x2="-1.15" y2="2.275" layer="31"/>
<rectangle x1="-0.85" y1="1.875" x2="-0.65" y2="2.275" layer="31"/>
<rectangle x1="-0.35" y1="1.875" x2="-0.15" y2="2.275" layer="31"/>
<rectangle x1="0.15" y1="1.875" x2="0.35" y2="2.275" layer="31"/>
<rectangle x1="0.65" y1="1.875" x2="0.85" y2="2.275" layer="31"/>
<rectangle x1="1.15" y1="1.875" x2="1.35" y2="2.275" layer="31"/>
<rectangle x1="1.65" y1="1.875" x2="1.85" y2="2.275" layer="31"/>
<rectangle x1="-1.85" y1="-2.325" x2="-1.65" y2="-1.925" layer="31"/>
<rectangle x1="-1.35" y1="-2.325" x2="-1.15" y2="-1.925" layer="31"/>
<rectangle x1="-0.85" y1="-2.325" x2="-0.65" y2="-1.925" layer="31"/>
<rectangle x1="-0.35" y1="-2.325" x2="-0.15" y2="-1.925" layer="31"/>
<rectangle x1="0.15" y1="-2.325" x2="0.35" y2="-1.925" layer="31"/>
<rectangle x1="0.65" y1="-2.325" x2="0.85" y2="-1.925" layer="31"/>
<rectangle x1="1.15" y1="-2.325" x2="1.35" y2="-1.925" layer="31"/>
<rectangle x1="1.65" y1="-2.325" x2="1.85" y2="-1.925" layer="31"/>
</package>
<package name="2020Z">
<smd name="1" x="-2.032" y="0" dx="1.9177" dy="2.794" layer="1"/>
<smd name="2" x="2.032" y="0" dx="1.9177" dy="2.794" layer="1"/>
<text x="-2.4" y="2.1" size="0.8" layer="25" align="top-left">&gt;NAME</text>
<text x="-2.4" y="-2.1" size="0.8" layer="27">&gt;VALUE</text>
<wire x1="-2.75" y1="1.7" x2="-2.75" y2="2.6" width="0.13" layer="21"/>
<wire x1="-2.75" y1="2.6" x2="2.75" y2="2.6" width="0.13" layer="21"/>
<wire x1="2.75" y1="2.6" x2="2.75" y2="1.7" width="0.13" layer="21"/>
<wire x1="-2.75" y1="-1.7" x2="-2.75" y2="-2.6" width="0.13" layer="21"/>
<wire x1="-2.75" y1="-2.6" x2="2.75" y2="-2.6" width="0.13" layer="21"/>
<wire x1="2.75" y1="-2.6" x2="2.75" y2="-1.7" width="0.13" layer="21"/>
</package>
<package name="1X03_NO_SILK">
<pad name="1" x="-3" y="0" drill="1.3" diameter="2.2" rot="R90" stop="no"/>
<pad name="2" x="0" y="0" drill="1.3" diameter="2.2" rot="R90" stop="no"/>
<pad name="3" x="3" y="0" drill="1.3" diameter="2.2" rot="R90" stop="no"/>
<text x="-3.556" y="1.8288" size="0.762" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-1.905" size="0.762" layer="27" align="top-left">&gt;VALUE</text>
<circle x="-3" y="0" radius="0.54083125" width="1.3" layer="30"/>
<circle x="0" y="0" radius="0.54083125" width="1.3" layer="30"/>
<circle x="3" y="0" radius="0.54083125" width="1.3" layer="30"/>
<circle x="-3" y="0" radius="0.54083125" width="1.3" layer="29"/>
<circle x="0" y="0" radius="0.54083125" width="1.3" layer="29"/>
<circle x="3" y="0" radius="0.54083125" width="1.3" layer="29"/>
</package>
<package name="DSN2-1006">
<smd name="C" x="-0.45" y="0" dx="0.5" dy="0.6" layer="1" stop="no"/>
<smd name="A" x="0.375" y="0" dx="0.65" dy="0.6" layer="1" stop="no"/>
<wire x1="-0.85" y1="0.45" x2="0.85" y2="0.45" width="0.13" layer="21"/>
<wire x1="0.85" y1="0.45" x2="0.85" y2="-0.45" width="0.13" layer="21"/>
<wire x1="0.85" y1="-0.45" x2="-0.85" y2="-0.45" width="0.13" layer="21"/>
<wire x1="-0.85" y1="-0.45" x2="-0.85" y2="0.45" width="0.13" layer="21"/>
<wire x1="-0.5" y1="0.3" x2="0.5" y2="0.3" width="0.05" layer="51"/>
<wire x1="0.5" y1="0.3" x2="0.5" y2="-0.3" width="0.05" layer="51"/>
<wire x1="0.5" y1="-0.3" x2="-0.5" y2="-0.3" width="0.05" layer="51"/>
<wire x1="-0.5" y1="-0.3" x2="-0.5" y2="0.3" width="0.05" layer="51"/>
<text x="-0.6" y="0.3" size="0.2" layer="25" align="top-left">&gt;NAME</text>
<text x="-0.6" y="-0.3" size="0.2" layer="27">&gt;VALUE</text>
<rectangle x1="-0.75" y1="-0.35" x2="-0.2" y2="0.35" layer="29"/>
<rectangle x1="0.05" y1="-0.35" x2="0.75" y2="0.35" layer="29"/>
</package>
<package name="WE-TPC_10MM">
<smd name="2" x="4.5" y="0" dx="1.8" dy="4.6" layer="1"/>
<smd name="1" x="-4.5" y="0" dx="1.8" dy="4.6" layer="1"/>
<wire x1="-5" y1="2.1" x2="-2.1" y2="5" width="0.3" layer="21"/>
<wire x1="-2.1" y1="5" x2="2.1" y2="5" width="0.3" layer="21"/>
<wire x1="2.1" y1="5" x2="5" y2="2.1" width="0.3" layer="21"/>
<wire x1="5" y1="2.1" x2="5" y2="-2.1" width="0.3" layer="21"/>
<wire x1="5" y1="-2.1" x2="2.1" y2="-5" width="0.3" layer="21"/>
<wire x1="2.1" y1="-5" x2="-2.1" y2="-5" width="0.3" layer="21"/>
<wire x1="-2.1" y1="-5" x2="-5" y2="-2.1" width="0.3" layer="21"/>
<wire x1="-5" y1="-2.1" x2="-5" y2="2.1" width="0.3" layer="21"/>
<text x="-4" y="2" size="1.27" layer="25" align="top-left">&gt;NAME</text>
<text x="-4" y="-2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WE-HCI_7MM">
<smd name="2" x="2.6" y="0" dx="2.7" dy="2.2" layer="1"/>
<smd name="1" x="-2.6" y="0" dx="2.7" dy="2.2" layer="1"/>
<text x="-2.8" y="2.8" size="1" layer="25" align="top-left">&gt;NAME</text>
<text x="-2.8" y="-2.8" size="1" layer="27">&gt;VALUE</text>
<wire x1="3.5" y1="-3.5" x2="-3.5" y2="-3.5" width="0.13" layer="21"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="-1.5" width="0.13" layer="21"/>
<wire x1="-3.5" y1="3.5" x2="3.5" y2="3.5" width="0.13" layer="21"/>
<wire x1="3.5" y1="3.5" x2="3.5" y2="1.5" width="0.13" layer="21"/>
<wire x1="3.5" y1="-1.5" x2="3.5" y2="-3.5" width="0.13" layer="21"/>
<wire x1="-3.5" y1="3.5" x2="-3.5" y2="1.5" width="0.13" layer="21"/>
</package>
<package name="1X02_NO_SILK">
<pad name="1" x="-1.5" y="0" drill="1.2" diameter="2" rot="R90" stop="no"/>
<pad name="2" x="1.5" y="0" drill="1.2" diameter="2" rot="R90" stop="no"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<circle x="-1.5" y="0" radius="0.559015625" width="1.1" layer="30"/>
<circle x="-1.5" y="0" radius="0.559015625" width="1.1" layer="29"/>
<circle x="1.5" y="0" radius="0.559015625" width="1.1" layer="30"/>
<circle x="1.5" y="0" radius="0.559015625" width="1.1" layer="29"/>
</package>
<package name="POWERDI_123">
<smd name="A" x="-1.55" y="0" dx="1.05" dy="1.5" layer="1"/>
<smd name="C" x="0.85" y="0" dx="2.4" dy="1.5" layer="1"/>
<text x="-2" y="0.7" size="0.4" layer="25" align="top-left">&gt;NAME</text>
<text x="-2" y="-0.7" size="0.4" layer="27">&gt;VALUE</text>
<wire x1="-1.4" y1="0.9" x2="1.4" y2="0.9" width="0.05" layer="51"/>
<wire x1="1.4" y1="0.9" x2="1.4" y2="-0.9" width="0.05" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="-1.4" y2="-0.9" width="0.05" layer="51"/>
<wire x1="-1.4" y1="-0.9" x2="-1.4" y2="0.9" width="0.05" layer="51"/>
<text x="2" y="-1" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
</package>
<package name="0201P">
<description>Resistor land pattern from Panasonic product info PDF: http://www.digikey.fi/product-detail/en/ERJ-1GEF2433C/P243KABCT-ND/1467681</description>
<smd name="1" x="-0.3" y="0" dx="0.3" dy="0.35" layer="1" stop="no"/>
<text x="-0.5" y="0.2" size="0.2" layer="25" ratio="20" align="top-left">&gt;NAME</text>
<text x="-0.5" y="-0.2" size="0.15" layer="27" ratio="20">&gt;VALUE</text>
<wire x1="-0.3" y1="0.15" x2="0.3" y2="0.15" width="0.05" layer="51"/>
<wire x1="0.3" y1="0.15" x2="0.3" y2="-0.15" width="0.05" layer="51"/>
<wire x1="0.3" y1="-0.15" x2="-0.3" y2="-0.15" width="0.05" layer="51"/>
<wire x1="-0.3" y1="-0.15" x2="-0.3" y2="0.15" width="0.05" layer="51"/>
<smd name="2" x="0.3" y="0" dx="0.3" dy="0.35" layer="1" stop="no"/>
<wire x1="-0.6" y1="0.32" x2="0.6" y2="0.32" width="0.13" layer="21"/>
<wire x1="0.6" y1="0.32" x2="0.6" y2="-0.32" width="0.13" layer="21"/>
<wire x1="0.6" y1="-0.32" x2="-0.6" y2="-0.32" width="0.13" layer="21"/>
<wire x1="-0.6" y1="-0.32" x2="-0.6" y2="0.32" width="0.13" layer="21"/>
<rectangle x1="-0.51" y1="-0.23" x2="-0.15" y2="0.23" layer="29"/>
<rectangle x1="0.15" y1="-0.23" x2="0.51" y2="0.23" layer="29"/>
</package>
<package name="SOT1061">
<smd name="A1" x="-0.65" y="0.85" dx="0.4" dy="0.4" layer="1" stop="no"/>
<smd name="A2" x="0.65" y="0.85" dx="0.4" dy="0.4" layer="1" stop="no"/>
<smd name="C1" x="0" y="-0.3" dx="1.6" dy="1" layer="1" stop="no"/>
<smd name="C2" x="0" y="-0.92" dx="0.4" dy="0.25" layer="1" stop="no"/>
<rectangle x1="-0.9" y1="0.6" x2="-0.4" y2="1.1" layer="29"/>
<rectangle x1="0.4" y1="0.6" x2="0.9" y2="1.1" layer="29"/>
<rectangle x1="-0.85" y1="-0.85" x2="0.85" y2="0.25" layer="29"/>
<rectangle x1="-0.25" y1="-1.1" x2="0.25" y2="-0.85" layer="29"/>
<wire x1="-1.05" y1="1.05" x2="-1.05" y2="-1.05" width="0.13" layer="21"/>
<wire x1="-1.05" y1="-1.05" x2="-0.45" y2="-1.05" width="0.13" layer="21"/>
<wire x1="0.45" y1="-1.05" x2="1.05" y2="-1.05" width="0.13" layer="21"/>
<wire x1="1.05" y1="-1.05" x2="1.05" y2="1.05" width="0.13" layer="21"/>
<wire x1="-0.2" y1="1.05" x2="0.2" y2="1.05" width="0.13" layer="21"/>
<text x="-0.85" y="-0.7" size="0.3" layer="27">&gt;VALUE</text>
<text x="-0.85" y="0.7" size="0.3" layer="25" align="top-left">&gt;NAME</text>
</package>
<package name="DO-220AA">
<smd name="C" x="-0.7" y="0" dx="2.6" dy="2.3" layer="1" stop="no"/>
<smd name="A" x="1.65" y="0" dx="0.8" dy="1.3" layer="1" stop="no"/>
<wire x1="-2.2" y1="1.35" x2="1.55" y2="1.35" width="0.13" layer="21"/>
<wire x1="1.55" y1="1.35" x2="1.55" y2="0.85" width="0.13" layer="21"/>
<wire x1="1.55" y1="0.85" x2="2.25" y2="0.85" width="0.13" layer="21"/>
<wire x1="2.25" y1="0.85" x2="2.25" y2="-0.85" width="0.13" layer="21"/>
<wire x1="2.25" y1="-0.85" x2="1.55" y2="-0.85" width="0.13" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="-1.35" width="0.13" layer="21"/>
<wire x1="1.55" y1="-1.35" x2="-2.2" y2="-1.35" width="0.13" layer="21"/>
<wire x1="-2.2" y1="-1.35" x2="-2.2" y2="1.35" width="0.13" layer="21"/>
<text x="-2" y="0.9" size="0.5" layer="25" align="top-left">&gt;NAME</text>
<text x="-2" y="-0.9" size="0.5" layer="27">&gt;VALUE</text>
<rectangle x1="-2.05" y1="-1.2" x2="0.65" y2="1.2" layer="29"/>
<rectangle x1="1.2" y1="-0.7" x2="2.1" y2="0.7" layer="29"/>
</package>
<package name="D2-UFDFN">
<smd name="A" x="-0.45" y="0" dx="0.6" dy="0.7" layer="1" stop="no"/>
<smd name="C" x="0.45" y="0" dx="0.6" dy="0.7" layer="1" stop="no"/>
<wire x1="-0.95" y1="0.55" x2="0.95" y2="0.55" width="0.12" layer="21"/>
<wire x1="0.95" y1="0.55" x2="0.95" y2="-0.55" width="0.12" layer="21"/>
<wire x1="0.95" y1="-0.55" x2="-0.95" y2="-0.55" width="0.12" layer="21"/>
<wire x1="-0.95" y1="-0.55" x2="-0.95" y2="0.55" width="0.12" layer="21"/>
<text x="-0.6" y="0.35" size="0.2" layer="25" align="top-left">&gt;NAME</text>
<text x="-0.6" y="-0.35" size="0.2" layer="25">&gt;VALUE</text>
<rectangle x1="-0.85" y1="-0.45" x2="-0.15" y2="0.45" layer="29"/>
<rectangle x1="0.15" y1="-0.45" x2="0.85" y2="0.45" layer="29"/>
<wire x1="-0.5" y1="0.3" x2="0.5" y2="0.3" width="0.05" layer="49"/>
<wire x1="0.5" y1="0.3" x2="0.5" y2="-0.3" width="0.05" layer="49"/>
<wire x1="0.5" y1="-0.3" x2="-0.5" y2="-0.3" width="0.05" layer="49"/>
<wire x1="-0.5" y1="-0.3" x2="-0.5" y2="0.3" width="0.05" layer="49"/>
<text x="1" y="-0.7" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
</package>
<package name="XAL5050">
<smd name="P$1" x="-1.65" y="0" dx="1.2" dy="4.4" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="P$2" x="1.65" y="0" dx="1.2" dy="4.4" layer="1" stop="no" thermals="no" cream="no"/>
<wire x1="-2.75" y1="2.85" x2="2.75" y2="2.85" width="0.13" layer="21"/>
<wire x1="2.75" y1="2.85" x2="2.75" y2="-2.85" width="0.13" layer="21"/>
<wire x1="2.75" y1="-2.85" x2="-2.75" y2="-2.85" width="0.13" layer="21"/>
<wire x1="-2.75" y1="-2.85" x2="-2.75" y2="2.85" width="0.13" layer="21"/>
<text x="-2.35" y="-2" size="0.6" layer="27">&gt;VALUE</text>
<text x="-2.35" y="2" size="0.6" layer="25" align="top-left">&gt;NAME</text>
<rectangle x1="-2.3" y1="-2.25" x2="-1" y2="2.25" layer="29"/>
<rectangle x1="1" y1="-2.25" x2="2.3" y2="2.25" layer="29"/>
<rectangle x1="-2.15" y1="0.3" x2="-1.15" y2="2" layer="31"/>
<rectangle x1="-2.15" y1="-2" x2="-1.15" y2="-0.3" layer="31"/>
<rectangle x1="1.15" y1="0.3" x2="2.15" y2="2" layer="31"/>
<rectangle x1="1.15" y1="-2" x2="2.15" y2="-0.3" layer="31"/>
</package>
<package name="POWERDI_5">
<smd name="A2" x="2.872" y="-0.92" dx="1.39" dy="1.4" layer="1" rot="R90" stop="no" thermals="no"/>
<smd name="A1" x="2.872" y="0.92" dx="1.39" dy="1.4" layer="1" rot="R90" stop="no" thermals="no"/>
<wire x1="3.82" y1="-2" x2="3.82" y2="2" width="0.13" layer="21"/>
<wire x1="3.82" y1="2" x2="-3.78" y2="2" width="0.13" layer="21"/>
<wire x1="-3.78" y1="2" x2="-3.78" y2="-2" width="0.13" layer="21"/>
<wire x1="-3.78" y1="-2" x2="3.82" y2="-2" width="0.13" layer="21"/>
<rectangle x1="2.085" y1="-1.715" x2="3.675" y2="-0.115" layer="29" rot="R90"/>
<rectangle x1="2.085" y1="0.125" x2="3.675" y2="1.725" layer="29" rot="R90"/>
<text x="-3.3" y="-1.3" size="0.8" layer="27">&gt;VALUE</text>
<text x="-3.3" y="1.3" size="0.8" layer="25" align="top-left">&gt;NAME</text>
<smd name="C" x="-1.11" y="0" dx="4.86" dy="3.36" layer="1" stop="no" thermals="no" cream="no"/>
<rectangle x1="-3.6" y1="-1.75" x2="1.4" y2="1.75" layer="29"/>
<rectangle x1="-3.15" y1="-1.35" x2="0.9" y2="1.35" layer="31"/>
</package>
<package name="QFN-18">
<smd name="21" x="-1.075" y="0" dx="0.45" dy="0.27" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="13" x="-0.25" y="-1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="14" x="0.25" y="-1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="15" x="0.75" y="-1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="16" x="1.25" y="-1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="11" x="-1.25" y="-1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="8-9" x="-2" y="0" dx="0.7" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="7" x="-2" y="0.8" dx="0.7" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="10" x="-2" y="-0.8" dx="0.7" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="4" x="-0.25" y="1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="3" x="0.25" y="1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="2" x="0.75" y="1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="1" x="1.25" y="1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="6" x="-1.25" y="1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="18" x="2" y="-0.25" dx="0.7" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="19" x="2" y="0.25" dx="0.7" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="20" x="2" y="0.75" dx="0.7" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="17" x="2" y="-0.75" dx="0.7" dy="0.25" layer="1" stop="no" cream="no"/>
<text x="-1.8" y="1.1" size="0.6" layer="25" align="top-left">&gt;NAME</text>
<text x="-1.8" y="-1.1" size="0.6" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="1.2" x2="-2" y2="1.5" width="0.13" layer="21"/>
<wire x1="-2" y1="1.5" x2="-1.65" y2="1.5" width="0.13" layer="21"/>
<wire x1="-0.85" y1="1.5" x2="-0.7" y2="1.5" width="0.13" layer="21"/>
<wire x1="1.6" y1="1.5" x2="1.9" y2="1.5" width="0.13" layer="21"/>
<wire x1="1.9" y1="1.5" x2="2" y2="1.5" width="0.13" layer="21"/>
<wire x1="2" y1="1.5" x2="2" y2="1.4" width="0.13" layer="21"/>
<wire x1="2" y1="1.4" x2="2" y2="1.1" width="0.13" layer="21"/>
<wire x1="-1.65" y1="-1.5" x2="-2" y2="-1.5" width="0.13" layer="21"/>
<wire x1="-2" y1="-1.5" x2="-2" y2="-1.2" width="0.13" layer="21"/>
<wire x1="-0.85" y1="-1.5" x2="-0.7" y2="-1.5" width="0.13" layer="21"/>
<wire x1="2" y1="-1.1" x2="2" y2="-1.5" width="0.13" layer="21"/>
<wire x1="2" y1="-1.5" x2="1.6" y2="-1.5" width="0.13" layer="21"/>
<smd name="22" x="-0.305" y="0" dx="0.406" dy="0.27" layer="1" stop="no" thermals="no" cream="no"/>
<wire x1="1.9" y1="1.5" x2="2" y2="1.4" width="0.5" layer="21" curve="-270" cap="flat"/>
<rectangle x1="-0.45" y1="1.05" x2="1.45" y2="1.75" layer="29"/>
<rectangle x1="-0.45" y1="-1.75" x2="1.45" y2="-1.05" layer="29"/>
<rectangle x1="1.65" y1="-0.95" x2="2.35" y2="0.95" layer="29"/>
<rectangle x1="-2.35" y1="-1" x2="-1.65" y2="-0.675" layer="29"/>
<rectangle x1="-2.35" y1="-0.35" x2="-1.65" y2="0.35" layer="29"/>
<rectangle x1="-1.45" y1="1.05" x2="-1.05" y2="1.75" layer="29"/>
<rectangle x1="-1.45" y1="-1.75" x2="-1.05" y2="-1.05" layer="29"/>
<rectangle x1="-1.35" y1="-0.2" x2="-0.8" y2="0.2" layer="29"/>
<rectangle x1="-0.55" y1="-0.2" x2="-0.05" y2="0.2" layer="29"/>
<rectangle x1="-2.35" y1="0.675" x2="-1.65" y2="1" layer="29"/>
<rectangle x1="-1.25" y1="-0.1" x2="-0.9" y2="0.1" layer="31"/>
<rectangle x1="-0.45" y1="-0.1" x2="-0.15" y2="0.1" layer="31"/>
<rectangle x1="-1.35" y1="1.325" x2="-1.15" y2="1.725" layer="31"/>
<rectangle x1="-0.35" y1="1.325" x2="-0.15" y2="1.725" layer="31"/>
<rectangle x1="0.15" y1="1.325" x2="0.35" y2="1.725" layer="31"/>
<rectangle x1="0.65" y1="1.325" x2="0.85" y2="1.725" layer="31"/>
<rectangle x1="1.15" y1="1.325" x2="1.35" y2="1.725" layer="31"/>
<rectangle x1="1.15" y1="-1.725" x2="1.35" y2="-1.325" layer="31" rot="R180"/>
<rectangle x1="0.65" y1="-1.725" x2="0.85" y2="-1.325" layer="31" rot="R180"/>
<rectangle x1="0.15" y1="-1.725" x2="0.35" y2="-1.325" layer="31" rot="R180"/>
<rectangle x1="-0.35" y1="-1.725" x2="-0.15" y2="-1.325" layer="31" rot="R180"/>
<rectangle x1="-1.35" y1="-1.725" x2="-1.15" y2="-1.325" layer="31" rot="R180"/>
<rectangle x1="2.025" y1="0.55" x2="2.225" y2="0.95" layer="31" rot="R270"/>
<rectangle x1="2.025" y1="0.05" x2="2.225" y2="0.45" layer="31" rot="R270"/>
<rectangle x1="2.025" y1="-0.45" x2="2.225" y2="-0.05" layer="31" rot="R270"/>
<rectangle x1="2.025" y1="-0.95" x2="2.225" y2="-0.55" layer="31" rot="R270"/>
<rectangle x1="-2.225" y1="-0.45" x2="-2.025" y2="-0.05" layer="31" rot="R90"/>
<rectangle x1="-2.225" y1="0.05" x2="-2.025" y2="0.45" layer="31" rot="R90"/>
<rectangle x1="-2.225" y1="0.6" x2="-2.025" y2="1" layer="31" rot="R90"/>
<rectangle x1="-2.225" y1="-1" x2="-2.025" y2="-0.6" layer="31" rot="R90"/>
</package>
<package name="XAL6060">
<smd name="P$1" x="-2.02" y="0" dx="1.43" dy="5.33" layer="1" stop="no" thermals="no" cream="no"/>
<wire x1="-3.25" y1="3.45" x2="3.25" y2="3.45" width="0.13" layer="21"/>
<wire x1="3.25" y1="3.45" x2="3.25" y2="-3.35" width="0.13" layer="21"/>
<wire x1="3.25" y1="-3.35" x2="-3.25" y2="-3.35" width="0.13" layer="21"/>
<wire x1="-3.25" y1="-3.35" x2="-3.25" y2="3.45" width="0.13" layer="21"/>
<text x="-2.75" y="-2" size="0.6" layer="27">&gt;VALUE</text>
<text x="-2.75" y="2" size="0.6" layer="25" align="top-left">&gt;NAME</text>
<smd name="P$2" x="2.02" y="0" dx="1.43" dy="5.33" layer="1" stop="no" thermals="no" cream="no"/>
<rectangle x1="-2.8" y1="-2.75" x2="-1.25" y2="2.75" layer="29"/>
<rectangle x1="1.25" y1="-2.75" x2="2.8" y2="2.75" layer="29"/>
<rectangle x1="-2.5" y1="-2.45" x2="-1.55" y2="2.45" layer="31"/>
<rectangle x1="1.55" y1="-2.45" x2="2.5" y2="2.45" layer="31"/>
</package>
<package name="H1.27MM-3X2">
<pad name="P$2" x="-1.27" y="0.635" drill="0.6" diameter="1" stop="no"/>
<circle x="-1.27" y="0.635" radius="0.25" width="0.6" layer="29"/>
<pad name="P$4" x="0" y="0.635" drill="0.6" diameter="1" stop="no"/>
<circle x="0" y="0.635" radius="0.25" width="0.6" layer="29"/>
<pad name="P$6" x="1.27" y="0.635" drill="0.6" diameter="1" stop="no"/>
<circle x="1.27" y="0.635" radius="0.25" width="0.6" layer="29"/>
<pad name="P$3" x="0" y="-0.635" drill="0.6" diameter="1" stop="no"/>
<circle x="0" y="-0.635" radius="0.25" width="0.6" layer="29"/>
<pad name="P$5" x="1.27" y="-0.635" drill="0.6" diameter="1" stop="no"/>
<circle x="1.27" y="-0.635" radius="0.25" width="0.6" layer="29"/>
<pad name="P$1" x="-1.27" y="-0.635" drill="0.6" diameter="1" shape="square" stop="no" first="yes"/>
<rectangle x1="-1.82" y1="-1.185" x2="-0.72" y2="-0.085" layer="29"/>
<wire x1="-2" y1="1.715" x2="2" y2="1.715" width="0.13" layer="21"/>
<wire x1="2" y1="1.715" x2="2" y2="-1.715" width="0.13" layer="21"/>
<wire x1="2" y1="-1.715" x2="-2" y2="-1.715" width="0.13" layer="21"/>
<wire x1="-2" y1="-1.715" x2="-2" y2="1.715" width="0.13" layer="21"/>
</package>
<package name="DO-214AA+">
<smd name="A" x="-2" y="0" dx="2.16" dy="2.26" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="C" x="2" y="0" dx="2.16" dy="2.26" layer="1" stop="no" thermals="no" cream="no"/>
<wire x1="-2.3" y1="2" x2="2.3" y2="2" width="0.13" layer="21"/>
<wire x1="2.3" y1="2" x2="2.3" y2="1.5" width="0.13" layer="21"/>
<wire x1="2.3" y1="-1.5" x2="2.3" y2="-2" width="0.13" layer="21"/>
<wire x1="2.3" y1="-2" x2="-2.3" y2="-2" width="0.13" layer="21"/>
<wire x1="-2.3" y1="-2" x2="-2.3" y2="-1.5" width="0.13" layer="21"/>
<wire x1="-2.3" y1="2" x2="-2.3" y2="1.5" width="0.13" layer="21"/>
<text x="-2" y="-1.2" size="0.7" layer="27">&gt;VALUE</text>
<text x="-2" y="1.2" size="0.7" layer="25" align="top-left">&gt;NAME</text>
<text x="3.2" y="-1.6" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
<rectangle x1="-3.15" y1="-1.2" x2="-0.85" y2="1.2" layer="29"/>
<rectangle x1="0.85" y1="-1.2" x2="3.15" y2="1.2" layer="29"/>
<rectangle x1="-2.9" y1="-1" x2="-1.1" y2="1" layer="31"/>
<rectangle x1="1.1" y1="-1" x2="2.9" y2="1" layer="31"/>
</package>
<package name="0402_BARE">
<smd name="1" x="-0.55" y="0" dx="0.5" dy="0.7" layer="1"/>
<smd name="2" x="0.55" y="0" dx="0.5" dy="0.7" layer="1"/>
<text x="-0.85" y="0.35" size="0.254" layer="25" align="top-left">&gt;NAME</text>
<text x="-0.85" y="-0.35" size="0.254" layer="27">&gt;VALUE</text>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.05" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.05" layer="51"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.05" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.05" layer="51"/>
</package>
<package name="0503+">
<description>Suggested land pattern from Zener diode document: http://www.digikey.fi/product-detail/en/CZRER52C5V1/641-1259-1-ND/1963937</description>
<smd name="A" x="-0.525" y="0" dx="0.75" dy="0.95" layer="1"/>
<text x="-0.95" y="0.5" size="0.254" layer="25" align="top-left">&gt;NAME</text>
<text x="-0.95" y="-0.5" size="0.254" layer="27">&gt;VALUE</text>
<text x="1.1" y="-0.9" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
<smd name="C" x="0.525" y="0" dx="0.75" dy="0.95" layer="1"/>
<wire x1="-0.675" y1="0.425" x2="0.675" y2="0.425" width="0.05" layer="51"/>
<wire x1="0.675" y1="0.425" x2="0.675" y2="-0.425" width="0.05" layer="51"/>
<wire x1="0.675" y1="-0.425" x2="-0.675" y2="-0.425" width="0.05" layer="51"/>
<wire x1="-0.675" y1="-0.425" x2="-0.675" y2="0.425" width="0.05" layer="51"/>
<wire x1="-1.1" y1="0.7" x2="1.1" y2="0.7" width="0.13" layer="21"/>
<wire x1="1.1" y1="0.7" x2="1.1" y2="-0.7" width="0.13" layer="21"/>
<wire x1="1.1" y1="-0.7" x2="-1.1" y2="-0.7" width="0.13" layer="21"/>
<wire x1="-1.1" y1="-0.7" x2="-1.1" y2="0.7" width="0.13" layer="21"/>
</package>
<package name="SOD123F+">
<smd name="A" x="-1.4" y="0" dx="1.34" dy="1.8" layer="1" stop="no"/>
<smd name="C" x="1.4" y="0" dx="1.34" dy="1.8" layer="1" stop="no"/>
<rectangle x1="-2.12" y1="-0.95" x2="-0.68" y2="0.95" layer="29"/>
<rectangle x1="0.68" y1="-0.95" x2="2.12" y2="0.95" layer="29"/>
<wire x1="-2.3" y1="1.1" x2="2.3" y2="1.1" width="0.13" layer="21"/>
<wire x1="2.3" y1="1.1" x2="2.3" y2="-1.1" width="0.13" layer="21"/>
<wire x1="2.3" y1="-1.1" x2="-2.3" y2="-1.1" width="0.13" layer="21"/>
<wire x1="-2.3" y1="-1.1" x2="-2.3" y2="1.1" width="0.13" layer="21"/>
<text x="-2.05" y="-0.85" size="0.6" layer="27">&gt;VALUE</text>
<text x="-2.05" y="0.85" size="0.6" layer="25" align="top-left">&gt;NAME</text>
<wire x1="-1.4" y1="0.95" x2="1.4" y2="0.95" width="0.05" layer="20"/>
<wire x1="1.4" y1="0.95" x2="1.4" y2="-0.95" width="0.05" layer="20"/>
<wire x1="1.4" y1="-0.95" x2="-1.4" y2="-0.95" width="0.05" layer="20"/>
<wire x1="-1.4" y1="-0.95" x2="-1.4" y2="0.95" width="0.05" layer="20"/>
<text x="2.23" y="-1.33" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
</package>
<package name="SOD523+">
<smd name="A" x="-0.7" y="0" dx="0.6" dy="0.7" layer="1" stop="no"/>
<smd name="C" x="0.7" y="0" dx="0.6" dy="0.7" layer="1" stop="no"/>
<wire x1="-1.2" y1="0.6" x2="1.2" y2="0.6" width="0.13" layer="21"/>
<wire x1="1.2" y1="0.6" x2="1.2" y2="-0.6" width="0.13" layer="21"/>
<wire x1="1.2" y1="-0.6" x2="-1.2" y2="-0.6" width="0.13" layer="21"/>
<wire x1="-1.2" y1="-0.6" x2="-1.2" y2="0.6" width="0.13" layer="21"/>
<text x="-1.05" y="-0.45" size="0.4" layer="27">&gt;VALUE</text>
<text x="-1.05" y="0.45" size="0.4" layer="25" align="top-left">&gt;NAME</text>
<text x="1.23" y="-0.73" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
<rectangle x1="-1.05" y1="-0.4" x2="-0.35" y2="0.4" layer="29"/>
<rectangle x1="0.35" y1="-0.4" x2="1.05" y2="0.4" layer="29"/>
<wire x1="-0.65" y1="0.45" x2="0.65" y2="0.45" width="0.05" layer="51"/>
<wire x1="0.65" y1="0.45" x2="0.65" y2="0.15" width="0.05" layer="51"/>
<wire x1="0.65" y1="0.15" x2="0.85" y2="0.15" width="0.05" layer="51"/>
<wire x1="0.85" y1="0.15" x2="0.85" y2="-0.15" width="0.05" layer="51"/>
<wire x1="0.85" y1="-0.15" x2="0.65" y2="-0.15" width="0.05" layer="51"/>
<wire x1="0.65" y1="-0.15" x2="0.65" y2="-0.45" width="0.05" layer="51"/>
<wire x1="0.65" y1="-0.45" x2="-0.65" y2="-0.45" width="0.05" layer="51"/>
<wire x1="-0.65" y1="-0.45" x2="-0.65" y2="-0.15" width="0.05" layer="51"/>
<wire x1="-0.65" y1="-0.15" x2="-0.85" y2="-0.15" width="0.05" layer="51"/>
<wire x1="-0.85" y1="-0.15" x2="-0.85" y2="0.15" width="0.05" layer="51"/>
<wire x1="-0.85" y1="0.15" x2="-0.65" y2="0.15" width="0.05" layer="51"/>
<wire x1="-0.65" y1="0.15" x2="-0.65" y2="0.45" width="0.05" layer="51"/>
</package>
<package name="0603+DUAL">
<text x="-0.8" y="0.5" size="0.3" layer="25" align="top-left">&gt;NAME</text>
<text x="-0.8" y="-0.5" size="0.3" layer="27">&gt;VALUE</text>
<text x="-1.06" y="0.95" size="0.52" layer="49" font="vector" ratio="20" rot="SR90" align="top-left">C</text>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.05" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.05" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.05" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.05" layer="51"/>
<smd name="3" x="0.6" y="0.4" dx="0.5" dy="0.4" layer="1" stop="no"/>
<smd name="1" x="0.6" y="-0.4" dx="0.5" dy="0.4" layer="1" stop="no"/>
<smd name="2" x="-0.6" y="-0.4" dx="0.5" dy="0.4" layer="1" stop="no"/>
<smd name="4" x="-0.6" y="0.4" dx="0.5" dy="0.4" layer="1" stop="no"/>
<rectangle x1="-0.9" y1="0.15" x2="-0.3" y2="0.65" layer="29"/>
<rectangle x1="-0.9" y1="-0.65" x2="-0.3" y2="-0.15" layer="29"/>
<rectangle x1="0.3" y1="-0.65" x2="0.9" y2="-0.15" layer="29"/>
<rectangle x1="0.3" y1="0.15" x2="0.9" y2="0.65" layer="29"/>
<wire x1="-1" y1="0.75" x2="1" y2="0.75" width="0.13" layer="49"/>
<wire x1="1" y1="0.75" x2="1" y2="-0.75" width="0.13" layer="49"/>
<wire x1="1" y1="-0.75" x2="-1" y2="-0.75" width="0.13" layer="49"/>
<wire x1="-1" y1="-0.75" x2="-1" y2="0.75" width="0.13" layer="49"/>
</package>
<package name="2-UQFN">
<smd name="C" x="0" y="0" dx="1.4" dy="1.05" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="A2" x="1.5" y="0" dx="0.5" dy="1.05" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="A1" x="-1.5" y="0" dx="0.5" dy="1.07" layer="1" stop="no" thermals="no" cream="no"/>
<rectangle x1="-1.75" y1="-0.55" x2="-1.25" y2="0.55" layer="29"/>
<rectangle x1="1.25" y1="-0.525" x2="1.75" y2="0.525" layer="29"/>
<rectangle x1="-0.7" y1="-0.525" x2="0.7" y2="0.525" layer="29"/>
<rectangle x1="-0.6" y1="-0.4" x2="-0.15" y2="0.4" layer="31"/>
<rectangle x1="0.15" y1="-0.4" x2="0.6" y2="0.4" layer="31"/>
<rectangle x1="-1.65" y1="-0.45" x2="-1.35" y2="0.45" layer="31"/>
<rectangle x1="1.35" y1="-0.45" x2="1.65" y2="0.45" layer="31"/>
<wire x1="-1.65" y1="-0.75" x2="1.65" y2="-0.75" width="0.13" layer="21"/>
<wire x1="-1.65" y1="0.75" x2="1.65" y2="0.75" width="0.13" layer="21"/>
<text x="-1.6" y="-0.5" size="0.4" layer="27">&gt;VALUE</text>
<text x="-1.6" y="0.5" size="0.4" layer="25" align="top-left">&gt;NAME</text>
<polygon width="0.127" layer="21">
<vertex x="-1.65" y="0.75"/>
<vertex x="-1.65" y="0.85" curve="-90"/>
<vertex x="-1.5" y="1" curve="-90"/>
<vertex x="-1.35" y="0.85"/>
<vertex x="-1.35" y="0.75"/>
</polygon>
</package>
<package name="SO-8">
<smd name="6" x="0.635" y="2.7" dx="0.6" dy="1.55" layer="1" thermals="no"/>
<smd name="5" x="1.905" y="2.7" dx="0.6" dy="1.55" layer="1" thermals="no"/>
<smd name="8" x="-1.905" y="2.7" dx="0.6" dy="1.55" layer="1" thermals="no"/>
<smd name="7" x="-0.635" y="2.7" dx="0.6" dy="1.55" layer="1" thermals="no"/>
<smd name="3" x="0.635" y="-2.7" dx="0.6" dy="1.55" layer="1" thermals="no"/>
<smd name="4" x="1.905" y="-2.7" dx="0.6" dy="1.55" layer="1" thermals="no"/>
<smd name="1" x="-1.905" y="-2.7" dx="0.6" dy="1.55" layer="1" thermals="no"/>
<smd name="2" x="-0.635" y="-2.7" dx="0.6" dy="1.55" layer="1" thermals="no"/>
<wire x1="-2.5" y1="2" x2="-2.5" y2="-2" width="0.13" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-2.4" y2="-2" width="0.13" layer="21"/>
<wire x1="-2.5" y1="2" x2="-2.4" y2="2" width="0.13" layer="21"/>
<wire x1="2.4" y1="2" x2="2.5" y2="2" width="0.13" layer="21"/>
<wire x1="2.5" y1="2" x2="2.5" y2="-2" width="0.13" layer="21"/>
<wire x1="2.5" y1="-2" x2="2.4" y2="-2" width="0.13" layer="21"/>
<polygon width="0.01" layer="21">
<vertex x="-2.5" y="-1.6" curve="180"/>
<vertex x="-2.5" y="-0.6"/>
</polygon>
<text x="-2.1" y="-1.4" size="0.8" layer="27">&gt;VALUE</text>
<text x="-2.1" y="1.4" size="0.8" layer="25" align="top-left">&gt;NAME</text>
</package>
<package name="SOD323+">
<smd name="C" x="1.35" y="0" dx="1" dy="0.8" layer="1"/>
<smd name="A" x="-1.35" y="0" dx="1" dy="0.8" layer="1"/>
<wire x1="-0.85" y1="0.65" x2="-0.85" y2="-0.65" width="0.05" layer="51"/>
<wire x1="-0.85" y1="-0.65" x2="0.85" y2="-0.65" width="0.05" layer="51"/>
<wire x1="0.85" y1="-0.65" x2="0.85" y2="0.65" width="0.05" layer="51"/>
<wire x1="0.85" y1="0.65" x2="-0.85" y2="0.65" width="0.05" layer="51"/>
<wire x1="-2.05" y1="0.75" x2="-2.05" y2="-0.75" width="0.13" layer="21"/>
<wire x1="-2.05" y1="-0.75" x2="2.05" y2="-0.75" width="0.13" layer="21"/>
<wire x1="2.05" y1="-0.75" x2="2.05" y2="0.75" width="0.13" layer="21"/>
<wire x1="2.05" y1="0.75" x2="-2.05" y2="0.75" width="0.13" layer="21"/>
<text x="-1.8" y="0.5" size="0.4" layer="21" align="top-left">&gt;NAME</text>
<text x="-1.8" y="-0.5" size="0.4" layer="27">&gt;VALUE</text>
<text x="2.08" y="-0.93" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
</package>
<package name="SOT-666">
<smd name="2" x="0" y="-0.7" dx="0.25" dy="0.4" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="3" x="0.5" y="-0.7" dx="0.25" dy="0.4" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="1" x="-0.5" y="-0.7" dx="0.25" dy="0.4" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="5" x="0" y="0.7" dx="0.25" dy="0.4" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="4" x="0.5" y="0.7" dx="0.25" dy="0.4" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="6" x="-0.5" y="0.7" dx="0.25" dy="0.4" layer="1" stop="no" thermals="no" cream="no"/>
<wire x1="-0.85" y1="0.6" x2="-0.85" y2="-0.6" width="0.13" layer="21"/>
<wire x1="0.85" y1="0.6" x2="0.85" y2="-0.6" width="0.13" layer="21"/>
<text x="-0.4" y="-0.3" size="0.2" layer="27">&gt;VALUE</text>
<text x="-0.4" y="0.3" size="0.2" layer="25" align="top-left">&gt;NAME</text>
<polygon width="0.01" layer="21">
<vertex x="-0.8" y="-0.25"/>
<vertex x="-0.4" y="-0.25"/>
<vertex x="-0.8" y="0.15"/>
</polygon>
<rectangle x1="-0.7" y1="0.5" x2="0.7" y2="0.9" layer="29"/>
<rectangle x1="-0.7" y1="-0.9" x2="0.7" y2="-0.5" layer="29"/>
<rectangle x1="-0.6" y1="0.55" x2="-0.4" y2="0.85" layer="31"/>
<rectangle x1="-0.1" y1="0.55" x2="0.1" y2="0.85" layer="31"/>
<rectangle x1="0.4" y1="0.55" x2="0.6" y2="0.85" layer="31"/>
<rectangle x1="0.4" y1="-0.85" x2="0.6" y2="-0.55" layer="31"/>
<rectangle x1="-0.1" y1="-0.85" x2="0.1" y2="-0.55" layer="31"/>
<rectangle x1="-0.6" y1="-0.85" x2="-0.4" y2="-0.55" layer="31"/>
</package>
<package name="TO-277">
<smd name="C" x="-1.06" y="0" dx="4.8" dy="4.72" layer="1" rot="R90" stop="no" thermals="no" cream="no"/>
<smd name="A1" x="2.745" y="-1.04" dx="1.4" dy="1.27" layer="1" rot="R90" stop="no" thermals="no" cream="no"/>
<smd name="A2" x="2.745" y="1.04" dx="1.4" dy="1.27" layer="1" rot="R90" stop="no" thermals="no" cream="no"/>
<wire x1="-3.7" y1="2.6" x2="3.6" y2="2.6" width="0.13" layer="21"/>
<wire x1="3.6" y1="2.6" x2="3.6" y2="-2.6" width="0.13" layer="21"/>
<wire x1="3.6" y1="-2.6" x2="-3.7" y2="-2.6" width="0.13" layer="21"/>
<wire x1="-3.7" y1="-2.6" x2="-3.7" y2="2.6" width="0.13" layer="21"/>
<rectangle x1="-3.5" y1="-2.45" x2="1.35" y2="2.45" layer="29"/>
<rectangle x1="2.05" y1="0.25" x2="3.45" y2="1.8" layer="29"/>
<rectangle x1="2.05" y1="-1.8" x2="3.45" y2="-0.25" layer="29"/>
<rectangle x1="2.2" y1="0.45" x2="3.3" y2="1.65" layer="31"/>
<rectangle x1="2.2" y1="-1.65" x2="3.3" y2="-0.45" layer="31"/>
<text x="-3.45" y="-2" size="0.8" layer="27">&gt;VALUE</text>
<text x="-3.45" y="2" size="0.8" layer="25" align="top-left">&gt;NAME</text>
<rectangle x1="-3.2" y1="0.5" x2="1" y2="2.1" layer="31"/>
<rectangle x1="-3.2" y1="-2.1" x2="1" y2="-0.5" layer="31"/>
</package>
<package name="DO-201AD">
<pad name="A" x="-6.5" y="0" drill="1.4" diameter="2.4"/>
<pad name="C" x="6.5" y="0" drill="1.4" diameter="2.4"/>
<wire x1="-4.7" y1="2.6" x2="4.7" y2="2.6" width="0.13" layer="51"/>
<wire x1="4.7" y1="2.6" x2="4.7" y2="-2.6" width="0.13" layer="51"/>
<wire x1="4.7" y1="-2.6" x2="-4.7" y2="-2.6" width="0.13" layer="51"/>
<wire x1="-4.7" y1="-2.6" x2="-4.7" y2="2.6" width="0.13" layer="51"/>
<text x="-4.1" y="2" size="1.27" layer="25" align="top-left">&gt;NAME</text>
<text x="-4.1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="3.5" y1="0.5" x2="3.5" y2="0" width="0.13" layer="51"/>
<wire x1="3.5" y1="0" x2="3.5" y2="-0.5" width="0.13" layer="51"/>
<wire x1="3.5" y1="0" x2="3" y2="0.5" width="0.13" layer="51"/>
<wire x1="3.5" y1="0" x2="3" y2="-0.5" width="0.13" layer="51"/>
<wire x1="3" y1="-0.5" x2="3" y2="0" width="0.13" layer="51"/>
<wire x1="3" y1="0" x2="3" y2="0.5" width="0.13" layer="51"/>
<wire x1="3" y1="0" x2="2.5" y2="0" width="0.13" layer="51"/>
<wire x1="3.5" y1="0" x2="4" y2="0" width="0.13" layer="51"/>
</package>
<package name="1X01_NO_SILK">
<pad name="1" x="0" y="0" drill="1.4" diameter="2.4" rot="R90"/>
<circle x="0" y="0" radius="0.508" width="0" layer="29"/>
<circle x="0" y="0" radius="0.9398" width="0" layer="30"/>
</package>
<package name="SOT-343">
<smd name="1" x="0.65" y="-0.75" dx="0.6" dy="0.8" layer="1" stop="no" thermals="no"/>
<wire x1="1.125" y1="0.625" x2="1.125" y2="-0.625" width="0.13" layer="21"/>
<text x="-1" y="-0.4" size="0.35" layer="27">&gt;VALUE</text>
<text x="-1" y="0.4" size="0.35" layer="25" align="top-left">&gt;NAME</text>
<smd name="4" x="-0.65" y="-0.75" dx="0.6" dy="0.8" layer="1" stop="no" thermals="no"/>
<smd name="3" x="-0.65" y="0.75" dx="0.6" dy="0.8" layer="1" stop="no" thermals="no"/>
<smd name="2" x="0.65" y="0.75" dx="0.6" dy="0.8" layer="1" stop="no" thermals="no"/>
<wire x1="-1.125" y1="-0.625" x2="-1.125" y2="0.625" width="0.13" layer="21"/>
<wire x1="-0.125" y1="0.625" x2="0.125" y2="0.625" width="0.13" layer="21"/>
<wire x1="-0.125" y1="-0.625" x2="0.125" y2="-0.625" width="0.13" layer="21"/>
<polygon width="0.01" layer="21">
<vertex x="1.125" y="-0.075" curve="-180"/>
<vertex x="1.125" y="-0.675"/>
</polygon>
<rectangle x1="-0.95" y1="-1.15" x2="-0.35" y2="-0.35" layer="29"/>
<rectangle x1="0.35" y1="-1.15" x2="0.95" y2="-0.35" layer="29"/>
<rectangle x1="0.35" y1="0.35" x2="0.95" y2="1.15" layer="29"/>
<rectangle x1="-0.95" y1="0.35" x2="-0.35" y2="1.15" layer="29"/>
</package>
<package name="UMD2">
<smd name="C" x="1.05" y="0" dx="0.9" dy="0.8" layer="1"/>
<smd name="A" x="-1.05" y="0" dx="0.9" dy="0.8" layer="1" thermals="no"/>
<wire x1="-0.8" y1="0.65" x2="-0.8" y2="-0.65" width="0.05" layer="51"/>
<wire x1="-0.8" y1="-0.65" x2="0.75" y2="-0.65" width="0.05" layer="51"/>
<wire x1="0.75" y1="-0.65" x2="0.75" y2="0.65" width="0.05" layer="51"/>
<wire x1="0.75" y1="0.65" x2="-0.8" y2="0.65" width="0.05" layer="51"/>
<text x="-1.1" y="0.5" size="0.4" layer="21" align="top-left">&gt;NAME</text>
<text x="-1.1" y="-0.5" size="0.4" layer="27">&gt;VALUE</text>
<text x="1.58" y="-0.63" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
</package>
<package name="SOD323F+">
<smd name="C" x="1" y="0" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="A" x="-1" y="0" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<wire x1="-0.85" y1="0.65" x2="-0.85" y2="-0.65" width="0.05" layer="51"/>
<wire x1="-0.85" y1="-0.65" x2="0.85" y2="-0.65" width="0.05" layer="51"/>
<wire x1="0.85" y1="-0.65" x2="0.85" y2="0.65" width="0.05" layer="51"/>
<wire x1="0.85" y1="0.65" x2="-0.85" y2="0.65" width="0.05" layer="51"/>
<wire x1="-1.55" y1="0.65" x2="-1.55" y2="-0.65" width="0.13" layer="21"/>
<wire x1="-1.55" y1="-0.65" x2="1.55" y2="-0.65" width="0.13" layer="21"/>
<wire x1="1.55" y1="-0.65" x2="1.55" y2="0.65" width="0.13" layer="21"/>
<wire x1="1.55" y1="0.65" x2="-1.55" y2="0.65" width="0.13" layer="21"/>
<text x="-1.3" y="0.5" size="0.35" layer="25" align="top-left">&gt;NAME</text>
<text x="-1.3" y="-0.5" size="0.35" layer="27">&gt;VALUE</text>
<text x="1.58" y="-0.83" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
</package>
<package name="SOT-363">
<smd name="2" x="0" y="-0.9" dx="0.3" dy="0.5" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="3" x="0.65" y="-0.9" dx="0.3" dy="0.5" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="1" x="-0.65" y="-0.9" dx="0.3" dy="0.5" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="5" x="0" y="0.9" dx="0.3" dy="0.5" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="4" x="0.65" y="0.9" dx="0.3" dy="0.5" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="6" x="-0.65" y="0.9" dx="0.3" dy="0.5" layer="1" stop="no" thermals="no" cream="no"/>
<wire x1="-1.1" y1="0.7" x2="-1.1" y2="-0.7" width="0.13" layer="21"/>
<wire x1="-1.1" y1="-0.7" x2="-1.2" y2="-0.7" width="0.13" layer="21"/>
<wire x1="-1.1" y1="0.7" x2="-1.2" y2="0.7" width="0.13" layer="21"/>
<wire x1="1.1" y1="0.7" x2="1.1" y2="-0.7" width="0.13" layer="21"/>
<wire x1="1.1" y1="-0.7" x2="1.2" y2="-0.7" width="0.13" layer="21"/>
<wire x1="1.1" y1="0.7" x2="1.2" y2="0.7" width="0.13" layer="21"/>
<text x="-0.9" y="-0.4" size="0.3" layer="27">&gt;VALUE</text>
<text x="-0.9" y="0.4" size="0.3" layer="25" align="top-left">&gt;NAME</text>
<rectangle x1="-0.9" y1="0.65" x2="0.9" y2="1.15" layer="29"/>
<rectangle x1="-0.9" y1="-1.15" x2="0.9" y2="-0.65" layer="29"/>
<rectangle x1="-0.75" y1="0.7" x2="-0.55" y2="1.1" layer="31"/>
<rectangle x1="-0.1" y1="0.7" x2="0.1" y2="1.1" layer="31"/>
<rectangle x1="0.55" y1="0.7" x2="0.75" y2="1.1" layer="31"/>
<rectangle x1="0.55" y1="-1.1" x2="0.75" y2="-0.7" layer="31"/>
<rectangle x1="-0.1" y1="-1.1" x2="0.1" y2="-0.7" layer="31"/>
<rectangle x1="-0.75" y1="-1.1" x2="-0.55" y2="-0.7" layer="31"/>
</package>
<package name="SMINI4-F3-B">
<wire x1="1.125" y1="0.625" x2="1.125" y2="-0.625" width="0.13" layer="21"/>
<text x="-1" y="-0.4" size="0.35" layer="27">&gt;VALUE</text>
<text x="-1" y="0.4" size="0.35" layer="25" align="top-left">&gt;NAME</text>
<smd name="4" x="-0.65" y="0.95" dx="0.5" dy="0.5" layer="1" thermals="no"/>
<wire x1="-1.125" y1="-0.625" x2="-1.125" y2="0.625" width="0.13" layer="21"/>
<wire x1="-0.125" y1="0.625" x2="0.125" y2="0.625" width="0.13" layer="21"/>
<wire x1="-0.125" y1="-0.625" x2="0.125" y2="-0.625" width="0.13" layer="21"/>
<polygon width="0.01" layer="21">
<vertex x="-1.15" y="-0.65" curve="-180"/>
<vertex x="-1.15" y="-0.05"/>
</polygon>
<smd name="3" x="0.65" y="0.95" dx="0.5" dy="0.5" layer="1" thermals="no"/>
<smd name="1" x="-0.65" y="-0.95" dx="0.5" dy="0.5" layer="1" thermals="no"/>
<smd name="2" x="0.65" y="-0.95" dx="0.5" dy="0.5" layer="1" thermals="no"/>
</package>
<package name="H2_0.7_3.5MM">
<pad name="POS" x="-1.75" y="0" drill="0.7" diameter="1.4"/>
<pad name="NEG" x="1.75" y="0" drill="0.7" diameter="1.4" shape="square"/>
</package>
<package name="JUMPER-PAD-ROUND">
<smd name="1" x="-0.3" y="0" dx="0.4" dy="0.3" layer="1" roundness="20" rot="R270" stop="no" thermals="no" cream="no"/>
<smd name="2" x="0.3" y="0" dx="0.4" dy="0.3" layer="1" roundness="20" rot="R90" stop="no" thermals="no" cream="no"/>
<text x="-1.1575" y="2.0225" size="0.5" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.175" y="1.3075" size="0.5" layer="27">&gt;VALUE</text>
<polygon width="0.2032" layer="1">
<vertex x="-0.225" y="0.6" curve="140"/>
<vertex x="-0.225" y="-0.6"/>
</polygon>
<polygon width="0.2032" layer="1">
<vertex x="0.225" y="-0.6" curve="140"/>
<vertex x="0.225" y="0.6"/>
</polygon>
<circle x="0" y="0" radius="0.360553125" width="1" layer="29"/>
<circle x="0" y="0" radius="1.1" width="0.254" layer="21"/>
</package>
<package name="1210+">
<smd name="A" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="C" x="1.4" y="0" dx="1.6" dy="2.7" layer="1" stop="no" thermals="no" cream="no"/>
<text x="-2.1" y="1.05" size="0.6" layer="25" align="top-left">&gt;NAME</text>
<text x="-2.1" y="-1.05" size="0.6" layer="27">&gt;VALUE</text>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.127" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.127" layer="51"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.127" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.127" layer="51"/>
<text x="2.2" y="-1.6" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
<rectangle x1="-2.25" y1="-1.4" x2="-0.55" y2="1.4" layer="29"/>
<rectangle x1="0.55" y1="-1.4" x2="2.25" y2="1.4" layer="29"/>
<rectangle x1="-1.9" y1="-1.2" x2="-0.7" y2="1.2" layer="31"/>
<rectangle x1="0.7" y1="-1.2" x2="1.9" y2="1.2" layer="31"/>
</package>
<package name="0805+">
<smd name="A" x="-1" y="0" dx="1" dy="1.5" layer="1"/>
<smd name="C" x="1" y="0" dx="1" dy="1.5" layer="1"/>
<text x="-1.5" y="0.7" size="0.5" layer="25" align="top-left">&gt;NAME</text>
<text x="-1.5" y="-0.7" size="0.5" layer="27">&gt;VALUE</text>
<wire x1="-1.075" y1="0.7" x2="1.075" y2="0.7" width="0.05" layer="51"/>
<wire x1="1.075" y1="0.7" x2="1.075" y2="-0.7" width="0.05" layer="51"/>
<wire x1="1.075" y1="-0.7" x2="-1.075" y2="-0.7" width="0.05" layer="51"/>
<wire x1="-1.075" y1="-0.7" x2="-1.075" y2="0.7" width="0.05" layer="51"/>
<wire x1="-1.7" y1="0.95" x2="1.7" y2="0.95" width="0.13" layer="21"/>
<wire x1="1.7" y1="0.95" x2="1.7" y2="-0.95" width="0.13" layer="21"/>
<wire x1="1.7" y1="-0.95" x2="-1.7" y2="-0.95" width="0.13" layer="21"/>
<wire x1="-1.7" y1="-0.95" x2="-1.7" y2="0.95" width="0.13" layer="21"/>
<text x="1.53" y="-1.13" size="0.52" layer="49" font="vector" ratio="20" rot="SR270" align="top-left">C</text>
</package>
<package name="AVX9258-6P">
<text x="-3" y="-1" size="0.5" layer="25" ratio="10" align="top-left">&gt;NAME</text>
<text x="-3" y="-2.3" size="0.5" layer="27">&gt;VALUE</text>
<smd name="1" x="-2.5" y="0" dx="0.7" dy="1.1" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="2" x="-1.5" y="0.1" dx="0.7" dy="1.3" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="3" x="-0.5" y="0" dx="0.7" dy="1.1" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="4" x="0.5" y="0" dx="0.7" dy="1.1" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="5" x="1.5" y="0" dx="0.7" dy="1.1" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="6" x="2.5" y="0.1" dx="0.7" dy="1.3" layer="1" stop="no" thermals="no" cream="no"/>
<rectangle x1="-1.8" y1="-0.5" x2="-1.2" y2="0.7" layer="29"/>
<rectangle x1="-0.8" y1="-0.5" x2="-0.2" y2="0.5" layer="29"/>
<rectangle x1="0.2" y1="-0.5" x2="0.8" y2="0.5" layer="29"/>
<rectangle x1="1.2" y1="-0.5" x2="1.8" y2="0.5" layer="29"/>
<rectangle x1="2.2" y1="-0.5" x2="2.8" y2="0.7" layer="29"/>
<rectangle x1="-2.8" y1="-0.5" x2="-2.2" y2="0.5" layer="29"/>
</package>
<package name="AVX9258-8P">
<text x="-5" y="5" size="0.6" layer="25" ratio="10">&gt;NAME</text>
<text x="-5" y="4" size="0.6" layer="27">&gt;VALUE</text>
<smd name="4" x="0.5" y="-2.85" dx="0.7" dy="0.7" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="5" x="-0.5" y="-2.85" dx="0.7" dy="0.7" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="6" x="-1.5" y="-2.85" dx="0.7" dy="0.7" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="7" x="-2.5" y="-2.85" dx="0.7" dy="0.7" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="8" x="-3.5" y="-2.85" dx="0.7" dy="0.7" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="3" x="1.5" y="-2.85" dx="0.7" dy="0.7" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="2" x="2.5" y="-2.85" dx="0.7" dy="0.7" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="1" x="3.5" y="-2.85" dx="0.7" dy="0.7" layer="1" stop="no" thermals="no" cream="no"/>
<hole x="-4.675" y="0" drill="1.2"/>
<hole x="4.675" y="0" drill="1.2"/>
<rectangle x1="-3.85" y1="-3.2" x2="-3.15" y2="-2.5" layer="29"/>
<rectangle x1="-2.85" y1="-3.2" x2="-2.15" y2="-2.5" layer="29"/>
<rectangle x1="-1.85" y1="-3.2" x2="-1.15" y2="-2.5" layer="29"/>
<rectangle x1="-0.85" y1="-3.2" x2="-0.15" y2="-2.5" layer="29"/>
<rectangle x1="0.15" y1="-3.2" x2="0.85" y2="-2.5" layer="29"/>
<rectangle x1="1.15" y1="-3.2" x2="1.85" y2="-2.5" layer="29"/>
<rectangle x1="2.15" y1="-3.2" x2="2.85" y2="-2.5" layer="29"/>
<rectangle x1="3.15" y1="-3.2" x2="3.85" y2="-2.5" layer="29"/>
<rectangle x1="-3.75" y1="-3.1" x2="-3.25" y2="-2.6" layer="31"/>
<rectangle x1="-2.75" y1="-3.1" x2="-2.25" y2="-2.6" layer="31"/>
<rectangle x1="-1.75" y1="-3.1" x2="-1.25" y2="-2.6" layer="31"/>
<rectangle x1="-0.75" y1="-3.1" x2="-0.25" y2="-2.6" layer="31"/>
<rectangle x1="0.25" y1="-3.1" x2="0.75" y2="-2.6" layer="31"/>
<rectangle x1="1.25" y1="-3.1" x2="1.75" y2="-2.6" layer="31"/>
<rectangle x1="2.25" y1="-3.1" x2="2.75" y2="-2.6" layer="31"/>
<rectangle x1="3.25" y1="-3.1" x2="3.75" y2="-2.6" layer="31"/>
<rectangle x1="-3.75" y1="2.6" x2="-3.25" y2="3.1" layer="31"/>
<rectangle x1="-3.85" y1="2.5" x2="-3.15" y2="3.2" layer="29"/>
<rectangle x1="-2.75" y1="2.6" x2="-2.25" y2="3.1" layer="31"/>
<rectangle x1="-2.85" y1="2.5" x2="-2.15" y2="3.2" layer="29"/>
<rectangle x1="-1.75" y1="2.6" x2="-1.25" y2="3.1" layer="31"/>
<rectangle x1="-1.85" y1="2.5" x2="-1.15" y2="3.2" layer="29"/>
<rectangle x1="-0.75" y1="2.6" x2="-0.25" y2="3.1" layer="31"/>
<rectangle x1="-0.85" y1="2.5" x2="-0.15" y2="3.2" layer="29"/>
<rectangle x1="0.25" y1="2.6" x2="0.75" y2="3.1" layer="31"/>
<rectangle x1="0.15" y1="2.5" x2="0.85" y2="3.2" layer="29"/>
<rectangle x1="1.25" y1="2.6" x2="1.75" y2="3.1" layer="31"/>
<rectangle x1="1.15" y1="2.5" x2="1.85" y2="3.2" layer="29"/>
<rectangle x1="2.25" y1="2.6" x2="2.75" y2="3.1" layer="31"/>
<rectangle x1="2.15" y1="2.5" x2="2.85" y2="3.2" layer="29"/>
<rectangle x1="3.25" y1="2.6" x2="3.75" y2="3.1" layer="31"/>
<rectangle x1="3.15" y1="2.5" x2="3.85" y2="3.2" layer="29"/>
<wire x1="-5.175" y1="2.75" x2="-4.125" y2="2.75" width="0.2032" layer="21"/>
<wire x1="4.125" y1="2.75" x2="5.175" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-5.175" y1="-2.75" x2="-4.125" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="4.125" y1="-2.75" x2="5.175" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="5.175" y1="-1.05" x2="5.175" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="5.175" y1="2.75" x2="5.175" y2="1.05" width="0.2032" layer="21"/>
<wire x1="-5.175" y1="-2.75" x2="-5.175" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="-5.175" y1="1.05" x2="-5.175" y2="2.75" width="0.2032" layer="21"/>
<text x="0" y="0" size="0.8128" layer="21" font="vector" ratio="15" align="center">00925
80080
04063</text>
</package>
</packages>
<symbols>
<symbol name="AVRISP">
<pin name="SCK" x="-12.7" y="0" visible="pin" length="middle" direction="in"/>
<pin name="MISO" x="-12.7" y="7.62" visible="pin" length="middle"/>
<pin name="RESET" x="-12.7" y="-7.62" visible="pin" length="middle" direction="in"/>
<pin name="VCC" x="12.7" y="7.62" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="12.7" y="-7.62" visible="pin" length="middle" direction="pwr" rot="R180"/>
<wire x1="-10.16" y1="12.7" x2="10.16" y2="12.7" width="0.508" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="-12.7" width="0.508" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="-10.16" y2="-12.7" width="0.508" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="-10.16" y2="12.7" width="0.508" layer="94"/>
<text x="-10.16" y="15.24" size="3.81" layer="95">&gt;NAME</text>
<text x="-10.16" y="-19.05" size="3.81" layer="96">&gt;VALUE</text>
<circle x="-5.08" y="0" radius="2.54" width="0.508" layer="94"/>
<circle x="-5.08" y="-7.62" radius="2.54" width="0.508" layer="94"/>
<circle x="5.08" y="7.62" radius="2.54" width="0.508" layer="94"/>
<circle x="5.08" y="0" radius="2.54" width="0.508" layer="94"/>
<circle x="5.08" y="-7.62" radius="2.54" width="0.508" layer="94"/>
<pin name="MOSI" x="12.7" y="0" visible="pin" length="middle" rot="R180"/>
<circle x="-5.08" y="7.62" radius="2.54" width="0.508" layer="94"/>
</symbol>
<symbol name="Z-DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.397" y1="1.27" x2="1.397" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.54" y="4.318" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="1.397" y1="1.27" x2="1.905" y2="1.778" width="0.254" layer="94"/>
<wire x1="1.397" y1="-1.27" x2="0.889" y2="-1.778" width="0.254" layer="94"/>
</symbol>
<symbol name="SCH_DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.286" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="CAP">
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<text x="-1.778" y="3.302" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.032" y="-4.572" size="1.27" layer="96">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="2.54" y2="0.254" layer="94" rot="R90"/>
<rectangle x1="0" y1="-0.254" x2="4.064" y2="0.254" layer="94" rot="R90"/>
<wire x1="2.032" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0.508" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="IND">
<wire x1="-5.08" y1="0" x2="-2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94" curve="-180"/>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-5.08" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="ATTINY13">
<pin name="PB4" x="-12.7" y="-2.54" visible="pin" length="middle"/>
<pin name="PB3" x="-12.7" y="2.54" visible="pin" length="middle"/>
<pin name="PB5" x="-12.7" y="7.62" visible="pin" length="middle"/>
<pin name="GND" x="-12.7" y="-7.62" visible="pin" length="middle" direction="pwr"/>
<pin name="VCC" x="12.7" y="7.62" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="PB2" x="12.7" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="PB1" x="12.7" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="PB0" x="12.7" y="-7.62" visible="pin" length="middle" rot="R180"/>
<wire x1="-7.62" y1="12.7" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="-7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-12.7" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
<text x="-7.62" y="15.24" size="2.54" layer="95">&gt;NAME</text>
<text x="-7.62" y="-17.78" size="2.54" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="RES">
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<wire x1="-1.905" y1="-1.27" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-3.81" y="1.905" size="1.27" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="1.27" layer="96">&gt;VALUE</text>
<wire x1="0" y1="1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="-1.27" x2="2.286" y2="0" width="0.254" layer="94"/>
<wire x1="-2.286" y1="0" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.286" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="LTC3115-1">
<pin name="GND" x="-15.24" y="0" visible="pin" length="middle" direction="pas"/>
<pin name="PVOUT" x="-15.24" y="7.62" visible="pin" length="middle" direction="pwr"/>
<pin name="SW2" x="-15.24" y="12.7" visible="pin" length="middle" direction="pas"/>
<pin name="RUN" x="-15.24" y="17.78" visible="pin" length="middle" direction="in"/>
<pin name="VC" x="-15.24" y="-7.62" visible="pin" length="middle" direction="pas"/>
<pin name="FB" x="-15.24" y="-12.7" visible="pin" length="middle" direction="in"/>
<pin name="RT" x="-15.24" y="-17.78" visible="pin" length="middle" direction="in"/>
<pin name="PWM" x="15.24" y="17.78" visible="pin" length="middle" direction="in" rot="R180"/>
<pin name="SW1" x="15.24" y="12.7" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="PVIN" x="15.24" y="7.62" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="BST1" x="15.24" y="2.54" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="BST2" x="15.24" y="-2.54" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="PVCC" x="15.24" y="-7.62" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="VIN" x="15.24" y="-12.7" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="VCC" x="15.24" y="-17.78" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="PGND" x="0" y="-27.94" length="middle" direction="pas" rot="R90"/>
<wire x1="-10.16" y1="22.86" x2="10.16" y2="22.86" width="0.254" layer="94"/>
<wire x1="10.16" y1="22.86" x2="10.16" y2="-22.86" width="0.254" layer="94"/>
<wire x1="10.16" y1="-22.86" x2="-10.16" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-22.86" x2="-10.16" y2="22.86" width="0.254" layer="94"/>
<text x="-10.16" y="27.94" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="25.4" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LED">
<wire x1="0" y1="-1.27" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.032" x2="2.159" y2="3.429" width="0.1524" layer="94"/>
<wire x1="1.905" y1="1.905" x2="3.302" y2="3.302" width="0.1524" layer="94"/>
<text x="-2.54" y="3.81" size="1.524" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.524" layer="96" align="top-left">&gt;VALUE</text>
<pin name="C" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<polygon width="0.1524" layer="94">
<vertex x="2.159" y="3.429"/>
<vertex x="1.27" y="3.048"/>
<vertex x="1.778" y="2.54"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.302" y="3.302"/>
<vertex x="2.413" y="2.921"/>
<vertex x="2.921" y="2.413"/>
</polygon>
</symbol>
<symbol name="M03">
<wire x1="-1.27" y1="-5.08" x2="-7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-1.27" y2="5.08" width="0.4064" layer="94"/>
<text x="-7.62" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-7.62" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="2.54" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="2.54" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="2.54" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M02">
<wire x1="1.27" y1="-2.54" x2="-5.08" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="N-MOSFET">
<pin name="S" x="0" y="-7.62" visible="off" direction="pas" rot="R90"/>
<pin name="D" x="0" y="7.62" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="G" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-3.81" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-2.54" y="0"/>
<vertex x="-1.27" y="0.635"/>
<vertex x="-1.27" y="-0.635"/>
</polygon>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="0" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.81" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="2.54" y="0.635"/>
<vertex x="1.905" y="-0.635"/>
<vertex x="3.175" y="-0.635"/>
</polygon>
<wire x1="1.905" y1="0.635" x2="3.175" y2="0.635" width="0.1524" layer="94"/>
<text x="-8.255" y="1.27" size="1.27" layer="95" rot="R90" align="top-left">&gt;NAME</text>
<text x="-6.35" y="1.27" size="1.27" layer="96" rot="R90" align="top-left">&gt;VALUE</text>
</symbol>
<symbol name="LT8614">
<pin name="BST" x="12.7" y="0" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="INTVCC" x="-12.7" y="-15.24" visible="pin" length="middle" direction="pas"/>
<pin name="BIAS" x="12.7" y="-10.16" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="VIN1" x="-12.7" y="10.16" visible="pin" length="middle" direction="pas"/>
<pin name="GND1" x="-12.7" y="5.08" visible="pin" length="middle" direction="pas"/>
<pin name="SW" x="12.7" y="-5.08" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="GND2" x="12.7" y="5.08" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="VIN2" x="12.7" y="10.16" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="EN/UV" x="0" y="20.32" visible="pin" length="middle" direction="pas" rot="R270"/>
<pin name="RT" x="-12.7" y="-20.32" visible="pin" length="middle" direction="pas"/>
<pin name="TR/SS" x="-12.7" y="-10.16" visible="pin" length="middle" direction="pas"/>
<pin name="SYNC/MODE" x="-12.7" y="-5.08" visible="pin" length="middle" direction="pas"/>
<pin name="GND" x="12.7" y="-20.32" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="PG" x="-12.7" y="0" visible="pin" length="middle" direction="pas"/>
<pin name="FB" x="12.7" y="-15.24" visible="pin" length="middle" direction="pas" rot="R180"/>
<wire x1="-7.62" y1="15.24" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="15.24" x2="7.62" y2="-22.86" width="0.254" layer="94"/>
<wire x1="7.62" y1="-22.86" x2="-7.62" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-22.86" x2="-7.62" y2="15.24" width="0.254" layer="94"/>
<text x="-7.62" y="-27.94" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-30.48" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="LED_DUAL">
<wire x1="0" y1="1.27" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="4.572" x2="2.159" y2="5.969" width="0.1524" layer="94"/>
<wire x1="1.905" y1="4.445" x2="3.302" y2="5.842" width="0.1524" layer="94"/>
<text x="-2.54" y="6.858" size="1.524" layer="95">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.524" layer="96" align="top-left">&gt;VALUE</text>
<pin name="C1" x="5.08" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="A1" x="-2.54" y="2.54" visible="off" length="short" direction="pas"/>
<polygon width="0.1524" layer="94">
<vertex x="2.159" y="5.969"/>
<vertex x="1.27" y="5.588"/>
<vertex x="1.778" y="5.08"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.302" y="5.842"/>
<vertex x="2.413" y="5.461"/>
<vertex x="2.921" y="4.953"/>
</polygon>
<wire x1="0" y1="-3.81" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-0.508" x2="2.159" y2="0.889" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="3.302" y2="0.762" width="0.1524" layer="94"/>
<pin name="C2" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="A2" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<polygon width="0.1524" layer="94">
<vertex x="2.159" y="0.889"/>
<vertex x="1.27" y="0.508"/>
<vertex x="1.778" y="0"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.302" y="0.762"/>
<vertex x="2.413" y="0.381"/>
<vertex x="2.921" y="-0.127"/>
</polygon>
</symbol>
<symbol name="SPT">
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="1.27" width="0.254" layer="94"/>
<pin name="A1" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="-1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<pin name="A2" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="P-MOSFET">
<pin name="S" x="0" y="-7.62" visible="off" direction="pas" rot="R90"/>
<pin name="D" x="0" y="7.62" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="G" x="-7.62" y="-2.54" visible="off" length="short" direction="pas"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-3.81" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="0" y="0"/>
<vertex x="-1.27" y="-0.635"/>
<vertex x="-1.27" y="0.635"/>
</polygon>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="0" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.81" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="2.54" y="-0.635"/>
<vertex x="3.175" y="0.635"/>
<vertex x="1.905" y="0.635"/>
</polygon>
<wire x1="3.175" y1="-0.635" x2="1.905" y2="-0.635" width="0.1524" layer="94"/>
<text x="-8.255" y="-0.635" size="1.27" layer="95" rot="R90" align="top-left">&gt;NAME</text>
<text x="-6.35" y="-0.635" size="1.27" layer="96" rot="R90" align="top-left">&gt;VALUE</text>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.397" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.397" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.397" x2="-1.27" y2="-1.397" width="0.254" layer="94"/>
<wire x1="1.397" y1="1.651" x2="1.397" y2="-1.651" width="0.254" layer="94"/>
<text x="-2.54" y="4.318" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="M01">
<wire x1="1.27" y1="-2.54" x2="-5.08" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="0" visible="off" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CPOL">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341"/>
<text x="1.016" y="3.175" size="1.27" layer="95">&gt;NAME</text>
<text x="1.016" y="0.889" size="1.27" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="JUMPER-PAD">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="TCAP">
<pin name="A" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="C" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<text x="-4.318" y="3.302" size="1.27" layer="95">&gt;NAME</text>
<text x="-4.572" y="-4.572" size="1.27" layer="96">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-0.254" x2="1.524" y2="0.254" layer="94" rot="R90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.778" x2="2.032" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="1.778" x2="-2.54" y2="-1.778" width="0.508" layer="94" curve="-83.267079"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AVRISP">
<description>AVR ISP header</description>
<gates>
<gate name="G$1" symbol="AVRISP" x="0" y="0"/>
</gates>
<devices>
<device name="JST-SH" package="JST-SH_6-TOP">
<connects>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="MISO" pad="1"/>
<connect gate="G$1" pin="MOSI" pad="4"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HIROSE" package="DF13-6P">
<connects>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="MISO" pad="1"/>
<connect gate="G$1" pin="MOSI" pad="4"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="H3X2" package="H2MM-3X2">
<connects>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="MISO" pad="1"/>
<connect gate="G$1" pin="MOSI" pad="4"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="H1809-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Hirose Electric Co Ltd" constant="no"/>
<attribute name="MFG_PART_#" value="A3C-6P-2DSA" constant="no"/>
<attribute name="RELATED" value="H2002-ND, H9999-ND" constant="no"/>
<attribute name="TYPE" value="thru-hole" constant="no"/>
</technology>
</technologies>
</device>
<device name="1.27MM" package="H1.27MM-3X2">
<connects>
<connect gate="G$1" pin="GND" pad="P$6"/>
<connect gate="G$1" pin="MISO" pad="P$1"/>
<connect gate="G$1" pin="MOSI" pad="P$4"/>
<connect gate="G$1" pin="RESET" pad="P$5"/>
<connect gate="G$1" pin="SCK" pad="P$3"/>
<connect gate="G$1" pin="VCC" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="609-3710-ND" constant="no"/>
<attribute name="MANUFACTURER" value="FCI" constant="no"/>
<attribute name="MFG_PART_#" value="20021111-00006T4LF" constant="no"/>
</technology>
</technologies>
</device>
<device name="AVX" package="AVX9258-6P">
<connects>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="MISO" pad="1"/>
<connect gate="G$1" pin="MOSI" pad="4"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="478-5495-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="AVX Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="009258008004063" constant="no"/>
</technology>
</technologies>
</device>
<device name="AVXC" package="AVX9258-8P">
<connects>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="MISO" pad="1"/>
<connect gate="G$1" pin="MOSI" pad="4"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="D_ZENER">
<description>Zener diode</description>
<gates>
<gate name="G$1" symbol="Z-DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="5.1V" package="0503+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="641-1259-1-ND" constant="no"/>
<attribute name="I_RLEAK" value="100nA @ 800mV" constant="no"/>
<attribute name="MANUFACTURER" value="Comchip Technology" constant="no"/>
<attribute name="MFG_PART_#" value="CZRER52C5V1" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.40" constant="no"/>
<attribute name="V_FWD" value="900mV @ 10mA" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.3V" package="D2-UFDFN">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="BZT52C4V3LPDICT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Diodes Incorporated" constant="no"/>
<attribute name="MFG_PART_#" value="BZT52C4V3LP-7" constant="no"/>
<attribute name="TEMP" value="-65°C ~ 150°C" constant="no"/>
<attribute name="TOLERANCE" value="±7%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.45" constant="no"/>
</technology>
</technologies>
</device>
<device name="12V_0402" package="0402+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="UCLAMP1201PCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Semtech" constant="no"/>
<attribute name="MFG_PART_#" value="UCLAMP1201P.TCT" constant="no"/>
<attribute name="USD" value="0.35" constant="no"/>
</technology>
</technologies>
</device>
<device name="5V_0402" package="0402+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="641-1295-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Comchip Technology" constant="no"/>
<attribute name="MFG_PART_#" value="CPDQR5V0U" constant="no"/>
<attribute name="USD" value="0.40" constant="no"/>
</technology>
</technologies>
</device>
<device name="12V_SPHV" package="0402+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="F6506CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Littelfuse Inc" constant="no"/>
<attribute name="MFG_PART_#" value="SPHV12-01ETG" constant="no"/>
<attribute name="USD" value="0.44" constant="no"/>
</technology>
</technologies>
</device>
<device name="33.3V" package="DO-214AA+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="P6SMB39ALFCT-ND" constant="no"/>
<attribute name="EURO" value="0.20/100" constant="no"/>
<attribute name="MANUFACTURER" value="Littelfuse Inc" constant="no"/>
<attribute name="MFG_PART_#" value="P6SMB39A" constant="no"/>
</technology>
</technologies>
</device>
<device name="6V_123F" package="SOD123F+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="F5749CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Littelfuse Inc" constant="no"/>
<attribute name="MFG_PART_#" value="SMF6.0A" constant="no"/>
</technology>
</technologies>
</device>
<device name="12V" package="SOD523+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="UCLAMP1201HCT-ND" constant="no"/>
<attribute name="EURO" value="6.22/25" constant="no"/>
<attribute name="MANUFACTURER" value="Semtech Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="UCLAMP1201H.TCT" constant="no"/>
</technology>
</technologies>
</device>
<device name="5V" package="SOD523+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="VESD05A1-02V-G-08CT-ND" constant="no"/>
<attribute name="EURO" value="3.00/10" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Semiconductor Diodes Division" constant="no"/>
<attribute name="MFG_PART_#" value="VESD05A1-02V-G-08" constant="no"/>
</technology>
</technologies>
</device>
<device name="6V" package="SOD523+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="568-7365-1-ND" constant="no"/>
<attribute name="EURO" value="3.44/10" constant="no"/>
<attribute name="MANUFACTURER" value="NXP Semiconductors" constant="no"/>
<attribute name="MFG_PART_#" value="PESD5Z6.0,115" constant="no"/>
</technology>
</technologies>
</device>
<device name="15V" package="SOD523+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="497-10767-1-ND" constant="no"/>
<attribute name="EURO" value="4.06/10" constant="no"/>
<attribute name="MANUFACTURER" value="STMicroelectronics" constant="no"/>
<attribute name="MFG_PART_#" value="ESDA18-1K" constant="no"/>
</technology>
</technologies>
</device>
<device name="5.6V" package="SOD523+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="BZT585B5V6T-7DICT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Diodes Incorporated" constant="no"/>
<attribute name="MFG_PART_#" value="BZT585B5V6T-7" constant="no"/>
</technology>
</technologies>
</device>
<device name="15V_0.2W_C" package="SOD323+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="MM3Z15VT1GOSCT-ND" constant="no"/>
<attribute name="EURO" value="0.15" constant="no"/>
<attribute name="MANUFACTURER" value="ON Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="MM3Z15VT1G" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.81V_0.5W" package="SOD323F+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="DDZ4V7CSF-7DICT-ND" constant="no"/>
<attribute name="EURO" value="0.16" constant="no"/>
<attribute name="MANUFACTURER" value="Diodes Incorporated" constant="no"/>
<attribute name="MFG_PART_#" value="DDZ4V7CSF-7" constant="no"/>
<attribute name="TOLERANCE" value="±3%" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.7V_0.2W" package="SOD323+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="MM3Z4V7T1GOSCT-ND" constant="no"/>
<attribute name="EURO" value="0.15" constant="no"/>
<attribute name="MANUFACTURER" value="ON Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="MM3Z4V7T1G" constant="no"/>
</technology>
</technologies>
</device>
<device name="12V_0.2W" package="SOD323+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="MM3Z12VT1GOSCT-ND" constant="no"/>
<attribute name="EURO" value="0.15" constant="no"/>
<attribute name="MANUFACTURER" value="ON Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="MM3Z12VT1G" constant="no"/>
</technology>
</technologies>
</device>
<device name="14.7V_0.2W" package="UMD2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="UDZSTE-1715BCT-ND" constant="no"/>
<attribute name="EURO" value="5.01/25" constant="no"/>
<attribute name="MANUFACTURER" value="Rohm Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="UDZSTE-1715B" constant="no"/>
</technology>
</technologies>
</device>
<device name="15V_0.2W_B" package="SOD323F+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="MM3Z15VBCT-ND" constant="no"/>
<attribute name="EURO" value="4.62/25" constant="no"/>
<attribute name="MANUFACTURER" value="Fairchild Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="MM3Z15VB" constant="no"/>
</technology>
</technologies>
</device>
<device name="15V_0.2W_A" package="SOD523+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="MM5Z15VT1GOSCT-ND" constant="no"/>
<attribute name="EURO" value="4.23/25" constant="no"/>
<attribute name="MANUFACTURER" value="ON Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="MM5Z15VT1G" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="D_SCH" prefix="D">
<description>Schottky diode</description>
<gates>
<gate name="G$1" symbol="SCH_DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="30V_0.2A" package="0402+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER" value="Comchip Technology" constant="no"/>
<attribute name="MFG_PART_#" value="CDBQR40" constant="no"/>
<attribute name="TEMP" value="125°C (Max)" constant="no"/>
</technology>
</technologies>
</device>
<device name="10V_3A" package="SOD323+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="568-6521-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="NXP Semiconductors" constant="no"/>
<attribute name="MFG_PART_#" value="PMEG4005AEA,115" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.49" constant="no"/>
</technology>
</technologies>
</device>
<device name="40V_0.5A" package="DSN2-1006">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="NSR05F40NXT5GOSCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="ON Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="NSR05F40NXT5G" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.38" constant="no"/>
</technology>
</technologies>
</device>
<device name="30V_0.1A" package="0402+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="641-1272-1-ND" constant="no"/>
<attribute name="I_RLEAK" value="500nA @ 10V" constant="no"/>
<attribute name="MANUFACTURER" value="Comchip Technology" constant="no"/>
<attribute name="MFG_PART_#" value="CDBQR0130R" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.43" constant="no"/>
<attribute name="V_FWD" value="450mV @ 10mA" constant="no"/>
</technology>
</technologies>
</device>
<device name="100V_1A" package="POWERDI_123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="DFLS1100DICT-ND" constant="no"/>
<attribute name="I_RLEAK" value="1µA @ 100V" constant="no"/>
<attribute name="MANUFACTURER" value="Diodes Incorporated" constant="no"/>
<attribute name="MFG_PART_#" value="DFLS1100-7" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 175°C" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.51" constant="no"/>
<attribute name="V_FWD" value="770mV @ 1A" constant="no"/>
</technology>
</technologies>
</device>
<device name="40V_0.02A" package="0402+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="641-1270-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Comchip Technology" constant="no"/>
<attribute name="MFG_PART_#" value="CDBQR00340" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.43" constant="no"/>
</technology>
</technologies>
</device>
<device name="60V_2A" package="SOT1061">
<connects>
<connect gate="G$1" pin="A" pad="A1 A2"/>
<connect gate="G$1" pin="C" pad="C1 C2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="568-6781-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="NXP Semiconductors" constant="no"/>
<attribute name="MFG_PART_#" value="PMEG6020EPA,115" constant="no"/>
<attribute name="USD" value="0.48" constant="no"/>
</technology>
</technologies>
</device>
<device name="60V_3A" package="DO-220AA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="SS3P6-M3/84AGICT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Semiconductor Diodes Division" constant="no"/>
<attribute name="MFG_PART_#" value="SS3P6-M3/84A" constant="no"/>
<attribute name="USD" value="1.20" constant="no"/>
</technology>
</technologies>
</device>
<device name="60V_7A" package="POWERDI_5">
<connects>
<connect gate="G$1" pin="A" pad="A1 A2"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="PDS760DICT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Diodes Incorporated" constant="no"/>
<attribute name="MFG_PART_#" value="PDS760-13" constant="no"/>
<attribute name="USD" value="1.47" constant="no"/>
</technology>
</technologies>
</device>
<device name="60V_7A_SS" package="TO-277">
<connects>
<connect gate="G$1" pin="A" pad="A1 A2"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="SS10P6-M3/86AGICT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Semiconductor Diodes Division" constant="no"/>
<attribute name="MFG_PART_#" value="SS10P6-M3/86A" constant="no"/>
<attribute name="USD" value="1.18" constant="no"/>
</technology>
</technologies>
</device>
<device name="40V_0.2A" package="SOD523+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="BAS40-02V-V-G-08GICT-ND" constant="no"/>
<attribute name="EURO" value="2.98/10" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Semiconductor Diodes Division" constant="no"/>
<attribute name="MFG_PART_#" value="BAS40-02V-V-G-08" constant="no"/>
<attribute name="V_FWD" value="0.38V@1mA" constant="no"/>
</technology>
</technologies>
</device>
<device name="40V_0.25MA" package="SOD523+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="NSR0340V2T1GOSCT-ND" constant="no"/>
<attribute name="EURO" value="3.70/10" constant="no"/>
<attribute name="MANUFACTURER" value="ON Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="NSR0340V2T1G" constant="no"/>
<attribute name="V_FWD" value="0.5V@0.2A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" uservalue="yes">
<description>Ceramic capacitor</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="22UF_16V" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-6797-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="C2012X5R1C226K125AC" constant="no"/>
<attribute name="TEMP" value="X5R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="0.67" constant="no"/>
</technology>
</technologies>
</device>
<device name="22UF_35V" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-14428-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="C2012X5R1V226M125AC" constant="no"/>
<attribute name="TEMP" value="X5R" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
<attribute name="USD" value="1.09" constant="no"/>
</technology>
</technologies>
</device>
<device name="47UF_6.3V-12" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="490-5311-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics North America" constant="no"/>
<attribute name="MFG_PART_#" value="GRM32ER70J476KE20L" constant="no"/>
<attribute name="TEMP" value="X7R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="1.95" constant="no"/>
</technology>
</technologies>
</device>
<device name="10UF_35V" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-11516-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="C2012JB1V106K085AC" constant="no"/>
<attribute name="TEMP" value="-25°C ~ 85°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.61" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.7UF_35V" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-11301-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="C1608JB1V475K080AC" constant="no"/>
<attribute name="TEMP" value="-25°C ~ 85°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.41" constant="no"/>
</technology>
</technologies>
</device>
<device name="1UF_35V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-11014-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="C1005JB1V105K050BC" constant="no"/>
<attribute name="TEMP" value="-25°C ~ 85°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.16" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.7PF_25V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="490-8178-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics North America" constant="no"/>
<attribute name="MFG_PART_#" value="GRM1555C1E4R7BA01D" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±0.1pF" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.09" constant="no"/>
</technology>
</technologies>
</device>
<device name="10UF_6.3V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="1276-1451-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc" constant="no"/>
<attribute name="MFG_PART_#" value="CL05A106MQ5NUNC" constant="no"/>
<attribute name="TEMP" value="X5R" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
<attribute name="USD" value="0.53" constant="no"/>
</technology>
</technologies>
</device>
<device name="10NF_35V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-6900-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="CGA2B3X7R1V103K050BB" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.09" constant="no"/>
</technology>
</technologies>
</device>
<device name="0.1UF_35V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-6901-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="CGA2B3X7R1V104K050BB" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.13" constant="no"/>
</technology>
</technologies>
</device>
<device name="1UF_4V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-8013-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="C1005X7S0G105K050BC" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.12" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.7UF_6.3V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-10890-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="C1005JB0J475K050BC" constant="no"/>
<attribute name="TEMP" value="JB" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="0.36" constant="no"/>
</technology>
</technologies>
</device>
<device name="820PF_50V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="720-1209-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Vitramon" constant="no"/>
<attribute name="MFG_PART_#" value="VJ0402Y821KNAAJ" constant="no"/>
<attribute name="TEMP" value="X7R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="0.43" constant="no"/>
</technology>
</technologies>
</device>
<device name="33PF_50V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="399-11121-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Kemet" constant="no"/>
<attribute name="MFG_PART_#" value="C0402C330F5GACTU" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="1.22" constant="no"/>
</technology>
</technologies>
</device>
<device name="1UF_6.3V_02" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="1276-1323-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc" constant="no"/>
<attribute name="MFG_PART_#" value="CL03A105KQ3CSNC" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 85°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.31" constant="no"/>
</technology>
</technologies>
</device>
<device name="0.1UF_10V_02" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-9051-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="C0603X7S1A104K030BC" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.13" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.7PF_50V_02" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="587-3191-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Taiyo Yuden" constant="no"/>
<attribute name="MFG_PART_#" value="UMK063CG4R7CT-F" constant="no"/>
<attribute name="TEMP" value="C0G, NP0" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
<attribute name="USD" value="1.10/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="10NF_10V_02" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="490-3194-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics North America" constant="no"/>
<attribute name="MFG_PART_#" value="GRM033R71A103KA01D" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.09" constant="no"/>
</technology>
</technologies>
</device>
<device name="33PF_25V_02" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="478-7391-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="AVX Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="02013A330GAT2A" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±2%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.34" constant="no"/>
</technology>
</technologies>
</device>
<device name="820PF_16V" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="478-1038-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="AVX Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="0201YC821KAT2A" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.33" constant="no"/>
</technology>
</technologies>
</device>
<device name="1UF_50V" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="587-3247-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Taiyo Yuden" constant="no"/>
<attribute name="MFG_PART_#" value="UMK107AB7105KA-T" constant="no"/>
<attribute name="TEMP" value="X7R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="1.71/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="0.1UF_25V" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="1276-1318-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc" constant="no"/>
<attribute name="MFG_PART_#" value="CL03A104KA3NNNC" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 85°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.30" constant="no"/>
</technology>
</technologies>
</device>
<device name="0.1UF_50V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="587-3498-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Taiyo Yuden" constant="no"/>
<attribute name="MFG_PART_#" value="UMK105B7104KV-FR" constant="no"/>
<attribute name="TEMP" value="X7R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="2.29/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="47UF_6.3V-08" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="587-1779-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Taiyo Yuden" constant="no"/>
<attribute name="MFG_PART_#" value="JMK212BJ476MG-T" constant="no"/>
<attribute name="TEMP" value="X5R" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
<attribute name="USD" value="1.17" constant="no"/>
</technology>
</technologies>
</device>
<device name="10NF_10V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="478-7890-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="AVX Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="0402ZC103KAT2A" constant="no"/>
<attribute name="TEMP" value="X7R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="0.58/100" constant="no"/>
</technology>
</technologies>
</device>
<device name="1UF_6.3V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="1276-1513-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc" constant="no"/>
<attribute name="MFG_PART_#" value="CL05B105KQ5NQNC" constant="no"/>
<attribute name="TEMP" value="X7R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="0.1UF_10V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="587-3769-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Taiyo Yuden" constant="no"/>
<attribute name="MFG_PART_#" value="LMK105B7104KVHF" constant="no"/>
<attribute name="TEMP" value="X7R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="1.90/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="33PF_25V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="399-11122-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Kemet" constant="no"/>
<attribute name="MFG_PART_#" value="C0402C330J3GACTU" constant="no"/>
<attribute name="TEMP" value="C0G, NP0" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
<attribute name="USD" value="0.30" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.7PF_50V_AVX" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="478-1066-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="AVX Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="04025A4R7CAT2A" constant="no"/>
<attribute name="TEMP" value="C0G, NP0" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
<attribute name="USD" value="0.26" constant="no"/>
</technology>
</technologies>
</device>
<device name="10NF_50V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="399-7758-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Kemet" constant="no"/>
<attribute name="MFG_PART_#" value="C0402C103J5RACTU" constant="no"/>
<attribute name="TEMP" value="X7R" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
<attribute name="USD" value="0.34" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.2UF_35V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-9028-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="C1005X5R1V225K050BC" constant="no"/>
<attribute name="TEMP" value="X5R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="0.28" constant="no"/>
</technology>
</technologies>
</device>
<device name="47UF_10V" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-8239-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="C2012X5R1A476M125AC" constant="no"/>
<attribute name="TEMP" value="X5R" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
<attribute name="USD" value="0.93" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.7UF_10V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-13820-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="C1005X5R1A475K050BC" constant="no"/>
<attribute name="TEMP" value="X5R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="0.43" constant="no"/>
</technology>
</technologies>
</device>
<device name="0.1UF_100V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="490-10458-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics North America" constant="no"/>
<attribute name="MFG_PART_#" value="GRM155R62A104KE14D" constant="no"/>
<attribute name="TEMP" value="X5R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="0.65/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="1NF_100V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-15935-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="CGA2B2X8R2A102K050BA" constant="no"/>
<attribute name="TEMP" value="X8R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="0.26" constant="no"/>
</technology>
</technologies>
</device>
<device name="10UF_10V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="1276-1450-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc" constant="no"/>
<attribute name="MFG_PART_#" value="CL05A106MP5NUNC" constant="no"/>
<attribute name="TEMP" value="X5R" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
<attribute name="USD" value="0.59" constant="no"/>
</technology>
</technologies>
</device>
<device name="33PF_100V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="490-7305-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics North America" constant="no"/>
<attribute name="MFG_PART_#" value="GRM1555C2A330JA01D" constant="no"/>
<attribute name="TEMP" value="C0G, NP0" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
<attribute name="USD" value="1.50/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="820PF_100V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="445-172681-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TDK Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="CGA2B1C0G2A821J050BC" constant="no"/>
<attribute name="TEMP" value="C0G, NP0" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
<attribute name="USD" value="1.27/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="1UF_10V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="490-3890-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics North America" constant="no"/>
<attribute name="MFG_PART_#" value="GRM155R61A105KE15D" constant="no"/>
<attribute name="TEMP" value="X5R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="1.86/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="1.5NF_50V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="490-3245-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics North America" constant="no"/>
<attribute name="MFG_PART_#" value="GRM155R71H152KA01D" constant="no"/>
<attribute name="TEMP" value="X7R" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
<attribute name="USD" value="0.97/100" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.7PF_50V" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="1276-1702-1-ND" constant="no"/>
<attribute name="EURO" value="1.35/100" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc." constant="no"/>
<attribute name="MFG_PART_#" value="CL05C4R7BB5NNNC" constant="no"/>
<attribute name="TEMP" value="C0G, NP0" constant="no"/>
<attribute name="TOLERANCE" value="±0.1pF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATTINY13A">
<description>IC MCU 8BIT 1KB FLASH 10MLF</description>
<gates>
<gate name="G$1" symbol="ATTINY13" x="0" y="0"/>
</gates>
<devices>
<device name="10M1" package="10-MLF">
<connects>
<connect gate="G$1" pin="GND" pad="5 11"/>
<connect gate="G$1" pin="PB0" pad="6"/>
<connect gate="G$1" pin="PB1" pad="7"/>
<connect gate="G$1" pin="PB2" pad="9"/>
<connect gate="G$1" pin="PB3" pad="2"/>
<connect gate="G$1" pin="PB4" pad="4"/>
<connect gate="G$1" pin="PB5" pad="1"/>
<connect gate="G$1" pin="VCC" pad="10"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="ATTINY13A-MMF-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Atmel" constant="no"/>
<attribute name="MFG_PART_#" value="ATTINY13A-MMF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES" prefix="R">
<description>Resistor</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="1M_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-1.00MLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04021M00FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.11/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="243K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-243KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402243KFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="0.78/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="18.2K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-18.2KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040218K2FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="0.78/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="90.9K_16_PREC" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-2170-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="TNPW040290K9BEED" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="40.2K_16_PREC" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="TNP40.2KDACT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="TNPW040240K2BEED" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="10K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-10.0KLCT-ND" constant="no"/>
<attribute name="EURO" value="2.10/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040210K0FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="35.7K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040235K7FKED" constant="no"/>
<attribute name="TEMP" value="100 PPM / K" constant="no"/>
<attribute name="TOLERANCE" value="1 %" constant="no"/>
</technology>
</technologies>
</device>
<device name="220_16_PREC" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="A102714CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TE Connectivity" constant="no"/>
<attribute name="MFG_PART_#" value="3-1879208-3" constant="no"/>
<attribute name="TEMP" value="±25ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±0.1%" constant="no"/>
<attribute name="USD" value="0.63" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.2K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-2.20KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04022K20FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.11/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="324_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="A102794CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="TE Connectivity" constant="no"/>
<attribute name="MFG_PART_#" value="1879214-1" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="243K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="P243KABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components" constant="no"/>
<attribute name="MFG_PART_#" value="ERJ-1GEF2433C" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="40.2K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-40.2KAABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW020140K2FKED" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="324_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="P324ABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components" constant="no"/>
<attribute name="MFG_PART_#" value="ERJ-1GEF3240C" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="220_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="P220ABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components" constant="no"/>
<attribute name="MFG_PART_#" value="ERJ-1GEF2200C" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="1M_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-1.00MAABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW02011M00FKED" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="1K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-1.00KAABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW02011K00FKED" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="0" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="470_8" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-470YACT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402470RJNEDHP" constant="no"/>
<attribute name="TEMP" value="±200ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="5%" constant="no"/>
<attribute name="USD" value="1.44/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="18.2K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="P18.2KABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components" constant="no"/>
<attribute name="MFG_PART_#" value="ERJ-1GEF1822C" constant="no"/>
<attribute name="TEMP" value="±200ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="1.82/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="10K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-10.0KAABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW020110K0FKED" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 155°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.23" constant="no"/>
</technology>
</technologies>
</device>
<device name="90.9K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-90.9KAABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW020190K9FKED" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="35.7K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="P35.7KABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components" constant="no"/>
<attribute name="MFG_PART_#" value="ERJ-1GEF3572C" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="330_8" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="MCT0603-330-CFCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Beyschlag" constant="no"/>
<attribute name="MFG_PART_#" value="MCT06030C3300FP500" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="324_8" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="CRCW0402324RFKEDHPCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402324RFKEDHP" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.2K_8" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-2.2KYACT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04022K20JNEDHP" constant="no"/>
<attribute name="TEMP" value="±200ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
<attribute name="USD" value="1.44/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="220_8" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-220YACT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402220RJNEDHP" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="41.2K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="P41.2KABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components" constant="no"/>
<attribute name="MFG_PART_#" value="ERJ-1GEF4122C" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
</technology>
</technologies>
</device>
<device name="324_10" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="P324LCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components" constant="no"/>
<attribute name="MFG_PART_#" value="ERJ-2RKF3240X" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.09" constant="no"/>
</technology>
</technologies>
</device>
<device name="330_5" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="RHM1022CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Rohm Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="ESR01MZPJ331" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.20" constant="no"/>
</technology>
</technologies>
</device>
<device name="470_5" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="RHM1025CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Rohm Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="ESR01MZPJ471" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.20" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.2K_5" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="RHM1014CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Rohm Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="ESR01MZPJ222" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.20" constant="no"/>
</technology>
</technologies>
</device>
<device name="48.7K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="P48.7ABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components" constant="no"/>
<attribute name="MFG_PART_#" value="ERJ-1GEF48R7C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.10" constant="no"/>
</technology>
</technologies>
</device>
<device name="6.8K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-6.80KAABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW02016K80FKED" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.23" constant="no"/>
</technology>
</technologies>
</device>
<device name="26.1K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-26.1KAABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW020126K1FKED" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.23" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.42K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-4.42KAABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW02014K42FKED" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.23" constant="no"/>
</technology>
</technologies>
</device>
<device name="1.6K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-1.60KAABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW02011K60FKED" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.23" constant="no"/>
</technology>
</technologies>
</device>
<device name="680_5" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="RHM1033CT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Rohm Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="ESR01MZPJ681" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.20" constant="no"/>
</technology>
</technologies>
</device>
<device name="2K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="P2.00KABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components" constant="no"/>
<attribute name="MFG_PART_#" value="ERJ-1GEF2001C" constant="no"/>
<attribute name="TEMP" value="±200ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="29.4K_20" package="0201P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="P29.4KABCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components" constant="no"/>
<attribute name="MFG_PART_#" value="ERJ-1GEF2942C" constant="no"/>
<attribute name="TEMP" value="±200ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.10" constant="no"/>
</technology>
</technologies>
</device>
<device name="2K_8" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-2.00KYCT-ND" constant="no"/>
<attribute name="EURO" value="4.49/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04022K00FKEDHP" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.22K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-4.22KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04024K22FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="0.78/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="220_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-220LCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402220RFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="0.78/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="26.1K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-26.1KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040226K1FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="0.78/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="90.9K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-90.9KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040290K9FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="0.78/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="29.4K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-29.4KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040229K4FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="0.78/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="10K_8" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-10.0KYCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040210K0FKEDHP" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="1.55/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="1M_8" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-1.00MYCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04021M00FKEDHP" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="1.55/10" constant="no"/>
</technology>
</technologies>
</device>
<device name="470K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-470KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402470KFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.11/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="26.7K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-26.7KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040226K7FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.11/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="1K_8" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-1.00KYCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04021K00FKEDHP" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="4.17/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="15.4K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-15.4KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040215K4FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.11/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="40.2K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-40.2KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040240K2FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.11/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="1.6K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-1.60KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04021K60FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="1.82/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="1K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-1.00KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04021K00FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="1.82/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="34.8K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-34.8KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040234K8FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.01/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="41.2K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-41.2KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040241K2FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.01/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="22K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-22.0KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040222K0FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.01/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="21.5K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-21.5KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040221K5FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.01/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="9.53K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-9.53KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04029K53FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.01/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="9.76K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-9.76KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04029K76FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.01/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.37K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-2.37KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04022K37FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.01/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="13.7K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-13.7KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040213K7FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.01/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="1.2K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-1.20KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04021K20FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
<attribute name="USD" value="2.10/50" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402" package="0402_BARE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.9K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-5.90KLCT-ND" constant="no"/>
<attribute name="EURO" value="2.10/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04025K90FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="33_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-33.0LCT-ND" constant="no"/>
<attribute name="EURO" value="2.10/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040233R0FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="3K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-3.00KLCT-ND" constant="no"/>
<attribute name="EURO" value="2.10/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04023K00FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="165_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-165LCT-ND" constant="no"/>
<attribute name="EURO" value="2.10/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402165RFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="232_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-232LCT-ND" constant="no"/>
<attribute name="EURO" value="2.10/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402232RFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="442_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-442LCT-ND" constant="no"/>
<attribute name="EURO" value="2.10/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402442RFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="13.3K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-13.3KLCT-ND" constant="no"/>
<attribute name="EURO" value="2.10/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040213K3FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="33.2_8" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-33.2YCT-ND" constant="no"/>
<attribute name="EURO" value="4.49/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040233R2FKEDHP" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="100_8" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-100YACT-ND" constant="no"/>
<attribute name="EURO" value="1.68/10" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402100RJNEDHP" constant="no"/>
<attribute name="TEMP" value="±200ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
</technology>
</technologies>
</device>
<device name="200K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-200KLCT-ND" constant="no"/>
<attribute name="EURO" value="2.27/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402200KFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="590_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-590LCT-ND" constant="no"/>
<attribute name="EURO" value="2.27/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402590RFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.32K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-2.32KLCT-ND" constant="no"/>
<attribute name="EURO" value="2.27/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04022K32FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="2M_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-2.00MLCT-ND" constant="no"/>
<attribute name="EURO" value="2.01/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04022M00FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.94K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-2.94KLCT-ND" constant="no"/>
<attribute name="EURO" value="2.01/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04022K94FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="162K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-162KLCT-ND" constant="no"/>
<attribute name="EURO" value="2.01/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402162KFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.7K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-4.70KLCT-ND" constant="no"/>
<attribute name="EURO" value="2.01/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04024K70FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="470_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-470LCT-ND" constant="no"/>
<attribute name="EURO" value="2.01/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402470RFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="20K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-20.0KLCT-ND" constant="no"/>
<attribute name="EURO" value="2.01/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040220K0FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.16K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-3.16KLCT-ND" constant="no"/>
<attribute name="EURO" value="2.01/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04023K16FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="698_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-698LCT-ND" constant="no"/>
<attribute name="EURO" value="2.01/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402698RFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="17.8K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-17.8KLCT-ND" constant="no"/>
<attribute name="EURO" value="2.01/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW040217K8FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.74K_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-2.74KLCT-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW04022K74FKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="140_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-140LCT-ND" constant="no"/>
<attribute name="EURO" value="2.01/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402140RFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="301_8" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-301YCT-ND" constant="no"/>
<attribute name="EURO" value="3.69/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402301RFKEDHP" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="620_4" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="P620BYCT-ND" constant="no"/>
<attribute name="EURO" value="3.41/50" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components" constant="no"/>
<attribute name="MFG_PART_#" value="ERJ-PA3F6200V" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="100_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-100LCT-ND" constant="no"/>
<attribute name="EURO" value="2.01/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402100RFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="210_16" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-210LCT-ND" constant="no"/>
<attribute name="EURO" value="2.01/50" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="CRCW0402210RFKED" constant="no"/>
<attribute name="TEMP" value="±100ppm/°C" constant="no"/>
<attribute name="TOLERANCE" value="±1%" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="I" uservalue="yes">
<description>Inductor</description>
<gates>
<gate name="G$1" symbol="IND" x="0" y="0"/>
</gates>
<devices>
<device name="6.8UH" package="2525CZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-1011-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="IHLP2525CZER6R8M01" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="2.38" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.2UH" package="2525CZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-1008-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="IHLP2525CZER2R2M01" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
<attribute name="USD" value="1.99" constant="no"/>
</technology>
</technologies>
</device>
<device name="10UH" package="2020Z">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="541-1272-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Dale" constant="no"/>
<attribute name="MFG_PART_#" value="IHLP2020CZER100M01" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="2.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="WE-TPC_10UH" package="WE-TPC_10MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="732-1076-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Wurth Electronics Inc" constant="no"/>
<attribute name="MFG_PART_#" value="744066100" constant="no"/>
<attribute name="TEMP" value="-40°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±30%" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="2.20" constant="no"/>
</technology>
</technologies>
</device>
<device name="WE-HCI_10UH" package="WE-HCI_7MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="732-2180-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Wurth Electronics Inc" constant="no"/>
<attribute name="MFG_PART_#" value="744314101" constant="no"/>
<attribute name="TEMP" value="-40°C ~ 150°C" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
<attribute name="USD" value="4.04" constant="no"/>
</technology>
</technologies>
</device>
<device name="10UH_4.9A" package="XAL5050">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="EURO" value="2.97" constant="no"/>
<attribute name="MANUFACTURER" value="Coilcraft" constant="no"/>
<attribute name="MFG_PART_#" value="XAL5050-103MEB" constant="no"/>
<attribute name="MOUSER_PART_#" value="994-XAL5050-103MEB" constant="no"/>
</technology>
</technologies>
</device>
<device name="8.2UH_6.1A" package="XAL5050">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="EURO" value="2.51" constant="no"/>
<attribute name="MANUFACTURER" value="Coilcraft" constant="no"/>
<attribute name="MFG_PART_#" value="XAL5050-822MEB" constant="no"/>
<attribute name="MOUSER_PART_#" value="994-XAL5050-822MEB" constant="no"/>
</technology>
</technologies>
</device>
<device name="5.6UH_10A" package="XAL6060">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="EURO" value="2.57" constant="no"/>
<attribute name="MANUFACTURER" value="Coilcraft" constant="no"/>
<attribute name="MFG_PART_#" value="XAL6060-562MEB" constant="no"/>
</technology>
</technologies>
</device>
<device name="5.6UH_7.2A" package="XAL5050">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="EURO" value="2.51" constant="no"/>
<attribute name="MANUFACTURER" value="Coilcraft" constant="no"/>
<attribute name="MFG_PART_#" value="XAL5050-562MEB" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTC3115-1">
<description>40V, 2A Synchronous Buck-Boost DC/DC Converter</description>
<gates>
<gate name="G$1" symbol="LTC3115-1" x="0" y="0"/>
</gates>
<devices>
<device name="IDHD" package="DFN-16">
<connects>
<connect gate="G$1" pin="BST1" pad="13"/>
<connect gate="G$1" pin="BST2" pad="12"/>
<connect gate="G$1" pin="FB" pad="7"/>
<connect gate="G$1" pin="GND" pad="4-5"/>
<connect gate="G$1" pin="PGND" pad="17 18"/>
<connect gate="G$1" pin="PVCC" pad="11"/>
<connect gate="G$1" pin="PVIN" pad="14"/>
<connect gate="G$1" pin="PVOUT" pad="3"/>
<connect gate="G$1" pin="PWM" pad="16"/>
<connect gate="G$1" pin="RT" pad="8"/>
<connect gate="G$1" pin="RUN" pad="1"/>
<connect gate="G$1" pin="SW1" pad="15"/>
<connect gate="G$1" pin="SW2" pad="2"/>
<connect gate="G$1" pin="VC" pad="6"/>
<connect gate="G$1" pin="VCC" pad="9"/>
<connect gate="G$1" pin="VIN" pad="10"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="LTC3115IDHD-2#PBF-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Linear Technology" constant="no"/>
<attribute name="MFG_PART_#" value="LTC3115IDHD-1#PBF" constant="no"/>
<attribute name="TEMP" value="-40°C ~ 125°C" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="GREEN_2.8V_5MA" package="0603+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="160-1832-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Lite-On Inc" constant="no"/>
<attribute name="MFG_PART_#" value="LTST-C193TGKT-5A" constant="no"/>
<attribute name="USD" value="0.44" constant="no"/>
<attribute name="V_FWD" value="3V (15mA)" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M03">
<gates>
<gate name="G$1" symbol="M03" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X03_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M02">
<gates>
<gate name="G$1" symbol="M02" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LT8614">
<description>IC REG BUCK SYNC ADJ 4A 18QFN</description>
<gates>
<gate name="G$1" symbol="LT8614" x="0" y="0"/>
</gates>
<devices>
<device name="IUDC" package="QFN-18">
<connects>
<connect gate="G$1" pin="BIAS" pad="1"/>
<connect gate="G$1" pin="BST" pad="3"/>
<connect gate="G$1" pin="EN/UV" pad="14"/>
<connect gate="G$1" pin="FB" pad="20"/>
<connect gate="G$1" pin="GND" pad="18"/>
<connect gate="G$1" pin="GND1" pad="6 7"/>
<connect gate="G$1" pin="GND2" pad="10 11"/>
<connect gate="G$1" pin="INTVCC" pad="2"/>
<connect gate="G$1" pin="PG" pad="19"/>
<connect gate="G$1" pin="RT" pad="15"/>
<connect gate="G$1" pin="SW" pad="8-9 21 22"/>
<connect gate="G$1" pin="SYNC/MODE" pad="17"/>
<connect gate="G$1" pin="TR/SS" pad="16"/>
<connect gate="G$1" pin="VIN1" pad="4"/>
<connect gate="G$1" pin="VIN2" pad="13"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="LT8614IUDC#PBF-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Linear Technology" constant="no"/>
<attribute name="MFG_PART_#" value="LT8614IUDC#PBF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED_DUAL">
<gates>
<gate name="G$1" symbol="LED_DUAL" x="0" y="0"/>
</gates>
<devices>
<device name="RG" package="0603+DUAL">
<connects>
<connect gate="G$1" pin="A1" pad="1"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="C1" pad="2"/>
<connect gate="G$1" pin="C2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="754-1650-1-ND" constant="no"/>
<attribute name="EURO" value="15.39/25" constant="no"/>
<attribute name="MANUFACTURER" value="Kingbright" constant="no"/>
<attribute name="MFG_PART_#" value="APHB1608ZGSURKC" constant="no"/>
<attribute name="V_FWD" value="green 3.4V, red 1.95V (15mA)" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TVS-SPT">
<description>Double TVS diode</description>
<gates>
<gate name="G$1" symbol="SPT" x="0" y="0"/>
</gates>
<devices>
<device name="36V" package="2-UQFN">
<connects>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="497-13827-1-ND" constant="no"/>
<attribute name="EURO" value="1.15" constant="no"/>
<attribute name="MANUFACTURER" value="STMicroelectronics" constant="no"/>
<attribute name="MFG_PART_#" value="SPT02-236DDB" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2PMOS">
<description>Dual P-channel enhancement mode MOSFET</description>
<gates>
<gate name="G$1" symbol="P-MOSFET" x="10.16" y="0"/>
<gate name="G$2" symbol="P-MOSFET" x="-5.08" y="0"/>
</gates>
<devices>
<device name="40V_5.1A" package="SO-8">
<connects>
<connect gate="G$1" pin="D" pad="7 8"/>
<connect gate="G$1" pin="G" pad="2"/>
<connect gate="G$1" pin="S" pad="1"/>
<connect gate="G$2" pin="D" pad="5 6"/>
<connect gate="G$2" pin="G" pad="4"/>
<connect gate="G$2" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="DMP4047SSD-13DICT-ND" constant="no"/>
<attribute name="EURO" value="0.74" constant="no"/>
<attribute name="MANUFACTURER" value="Diodes Incorporated" constant="no"/>
<attribute name="MFG_PART_#" value="DMP4047SSD" constant="no"/>
</technology>
</technologies>
</device>
<device name="40V_8A" package="SO-8">
<connects>
<connect gate="G$1" pin="D" pad="7 8"/>
<connect gate="G$1" pin="G" pad="2"/>
<connect gate="G$1" pin="S" pad="1"/>
<connect gate="G$2" pin="D" pad="5 6"/>
<connect gate="G$2" pin="G" pad="4"/>
<connect gate="G$2" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="SI4909DY-T1-GE3CT-ND" constant="no"/>
<attribute name="EURO" value="1.11" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Siliconix" constant="no"/>
<attribute name="MFG_PART_#" value="SI4909DY-T1-GE3" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2ZD">
<description>Dual Zener diode</description>
<gates>
<gate name="G$1" symbol="Z-DIODE" x="0" y="2.54"/>
<gate name="G$2" symbol="Z-DIODE" x="0" y="-2.54"/>
</gates>
<devices>
<device name="18V_0.2W" package="SOT-363">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="6"/>
<connect gate="G$2" pin="A" pad="4"/>
<connect gate="G$2" pin="C" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="MMBZ5248BS-FDICT-ND" constant="no"/>
<attribute name="EURO" value="3.42/10" constant="no"/>
<attribute name="MANUFACTURER" value="Diodes Incorporated" constant="no"/>
<attribute name="MFG_PART_#" value="MMBZ5248BS-7-F" constant="no"/>
<attribute name="TOLERANCE" value="±5%" constant="no"/>
</technology>
</technologies>
</device>
<device name="12V_0.2W" package="SOT-343">
<connects>
<connect gate="G$1" pin="A" pad="3"/>
<connect gate="G$1" pin="C" pad="4"/>
<connect gate="G$2" pin="A" pad="2"/>
<connect gate="G$2" pin="C" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="UMZ12KTLCT-ND" constant="no"/>
<attribute name="EURO" value="3.38/10" constant="no"/>
<attribute name="MANUFACTURER" value="Rohm Semiconductor" constant="no"/>
<attribute name="MFG_PART_#" value="UMZ12KTL" constant="no"/>
</technology>
</technologies>
</device>
<device name="18V_0.2W_B" package="SMINI4-F3-B">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="4"/>
<connect gate="G$2" pin="A" pad="2"/>
<connect gate="G$2" pin="C" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="DZ4J180K0RCT-ND" constant="no"/>
<attribute name="EURO" value="7.08/25" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components" constant="no"/>
<attribute name="MFG_PART_#" value="DZ4J180K0R" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2NMOS">
<description>Dual N-channel MOSFET</description>
<gates>
<gate name="G$1" symbol="N-MOSFET" x="-5.08" y="0"/>
<gate name="G$2" symbol="N-MOSFET" x="10.16" y="0"/>
</gates>
<devices>
<device name="60V_0.2A" package="SOT-666">
<connects>
<connect gate="G$1" pin="D" pad="6"/>
<connect gate="G$1" pin="G" pad="2"/>
<connect gate="G$1" pin="S" pad="1"/>
<connect gate="G$2" pin="D" pad="3"/>
<connect gate="G$2" pin="G" pad="5"/>
<connect gate="G$2" pin="S" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="SSM6N7002BFELMCT-ND" constant="no"/>
<attribute name="EURO" value="3.16/10" constant="no"/>
<attribute name="MANUFACTURER" value="Toshiba Semiconductor and Storage" constant="no"/>
<attribute name="MFG_PART_#" value="SSM6N7002BFE,LM" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE">
<description>Diode</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="325V_0.4A" package="D2-UFDFN">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="BAS521LP-7DICT-ND" constant="no"/>
<attribute name="I_RLEAK" value="150nA @ 250V" constant="no"/>
<attribute name="MANUFACTURER" value="Diodes Incorporated" constant="no"/>
<attribute name="MFG_PART_#" value="BAS521LP-7" constant="no"/>
<attribute name="TEMP" value="-65°C ~ 150°C" constant="no"/>
<attribute name="TYPE" value="smt" constant="no"/>
<attribute name="USD" value="0.34" constant="no"/>
<attribute name="V_FWD" value="1.1V @ 100mA" constant="no"/>
</technology>
</technologies>
</device>
<device name="40V_3A" package="DO-201AD">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="SB340-E3/54GICT-ND" constant="no"/>
<attribute name="EURO" value="7.11/10" constant="no"/>
<attribute name="MANUFACTURER" value="Vishay Semiconductor Diodes Division" constant="no"/>
<attribute name="MFG_PART_#" value="SB340-E3/54" constant="no"/>
</technology>
</technologies>
</device>
<device name="45V_12A" package="DO-201AD">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="SBR12A45SD1-TDICT-ND" constant="no"/>
<attribute name="EURO" value="7.74/10" constant="no"/>
<attribute name="MANUFACTURER" value="Diodes Incorporated" constant="no"/>
<attribute name="MFG_PART_#" value="SBR12A45SD1-T" constant="no"/>
<attribute name="V_FWD" value="480mV @ 110A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M01">
<gates>
<gate name="G$1" symbol="M01" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ECAP">
<description>Electrolytic capacitor</description>
<gates>
<gate name="G$1" symbol="CPOL" x="0" y="0"/>
</gates>
<devices>
<device name="100UF_50V" package="H2_0.7_3.5MM">
<connects>
<connect gate="G$1" pin="+" pad="POS"/>
<connect gate="G$1" pin="-" pad="NEG"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIA" value="8mm" constant="no"/>
<attribute name="DIGIKEY_PART_#" value="495-6007-ND" constant="no"/>
<attribute name="ESR" value="615 mOhm" constant="no"/>
<attribute name="HEIGHT" value="13mm" constant="no"/>
<attribute name="IMPEDANCE" value="540 mOhm" constant="no"/>
<attribute name="LIFE" value="5000 Hrs @ 105°C" constant="no"/>
<attribute name="MANUFACTURER" value="EPCOS (TDK)" constant="no"/>
<attribute name="MFG_PART_#" value="B41888C6107M" constant="no"/>
<attribute name="RIPPLE" value="0.5A" constant="no"/>
</technology>
</technologies>
</device>
<device name="330UF_16V" package="H2_0.7_3.5MM">
<connects>
<connect gate="G$1" pin="+" pad="POS"/>
<connect gate="G$1" pin="-" pad="NEG"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="493-1521-ND" constant="no"/>
<attribute name="EURO" value="2.62/10" constant="no"/>
<attribute name="LIFE" value="7000 Hrs @ 105°C" constant="no"/>
<attribute name="MANUFACTURER" value="Nichicon" constant="no"/>
<attribute name="MFG_PART_#" value="UHE1C331MPD" constant="no"/>
<attribute name="RIPPLE" value="448mA" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JUMPER-PAD">
<gates>
<gate name="G$1" symbol="JUMPER-PAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JUMPER-PAD-ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TCAP">
<gates>
<gate name="G$1" symbol="TCAP" x="2.54" y="0"/>
</gates>
<devices>
<device name="220UF_10V" package="1210+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="478-6612-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="AVX Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="TLNT227M010R1300" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
<attribute name="USD" value="2.67" constant="no"/>
</technology>
</technologies>
</device>
<device name="47UF_10V" package="0805+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="399-10536-1-ND" constant="no"/>
<attribute name="MANUFACTURER" value="Kemet" constant="no"/>
<attribute name="MFG_PART_#" value="T529P476M010AAE200" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 105°C" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
<attribute name="USD" value="1.43" constant="no"/>
</technology>
</technologies>
</device>
<device name="220UF_6.3V" package="1210+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="478-6608-1-ND" constant="no"/>
<attribute name="ESR" value="70mOhm" constant="no"/>
<attribute name="EURO" value="1.15" constant="no"/>
<attribute name="MANUFACTURER" value="AVX Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="TCJB227M006R0070" constant="no"/>
<attribute name="TOLERANCE" value="±20%" constant="no"/>
</technology>
</technologies>
</device>
<device name="330UF_6.3V" package="1210+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="478-8407-1-ND" constant="no"/>
<attribute name="ESR" value="600mOhm" constant="no"/>
<attribute name="EURO" value="16.56/10" constant="no"/>
<attribute name="MANUFACTURER" value="AVX Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="F950J337KBAAQ2" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
</technology>
</technologies>
</device>
<device name="47UF_16V" package="1210+">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY_PART_#" value="478-8477-1-ND" constant="no"/>
<attribute name="ESR" value="600mOhm" constant="no"/>
<attribute name="EURO" value="14.50/10" constant="no"/>
<attribute name="MANUFACTURER" value="AVX Corporation" constant="no"/>
<attribute name="MFG_PART_#" value="F951C476KBAAQ2" constant="no"/>
<attribute name="TEMP" value="-55°C ~ 125°C" constant="no"/>
<attribute name="TOLERANCE" value="±10%" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Passives">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="PAD.02X.02">
<smd name="P$1" x="0" y="0" dx="0.508" dy="0.508" layer="1"/>
</package>
<package name="PAD.03X.03">
<smd name="P$1" x="0" y="0" dx="0.762" dy="0.762" layer="1" roundness="100" cream="no"/>
</package>
<package name="PAD.03X.05">
<smd name="P$1" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" cream="no"/>
</package>
<package name="PAD.03X.04">
<smd name="P$1" x="0" y="0" dx="1.016" dy="1.016" layer="1" roundness="100" cream="no"/>
</package>
<package name="TP_15TH">
<pad name="P$1" x="0" y="0" drill="0.381" diameter="0.6096" stop="no"/>
<circle x="0" y="0" radius="0.381" width="0" layer="30"/>
</package>
</packages>
<symbols>
<symbol name="TEST-POINT">
<wire x1="2.54" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.762" x2="3.302" y2="-0.762" width="0.1524" layer="94" curve="180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;Name</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;Value</text>
<pin name="1" x="0" y="0" visible="off" length="point" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TEST-POINT" prefix="TP">
<description>Bare copper test points for troubleshooting or ICT</description>
<gates>
<gate name="G$1" symbol="TEST-POINT" x="0" y="0"/>
</gates>
<devices>
<device name="2" package="PAD.02X.02">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3" package="PAD.03X.03">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3X5" package="PAD.03X.05">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3X4" package="PAD.03X.04">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP_15TH_THRU" package="TP_15TH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="AGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="AGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AGND">
<gates>
<gate name="G$1" symbol="AGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.2032" drill="0">
<clearance class="0" value="0.2032"/>
</class>
<class number="1" name="ground" width="0.2032" drill="0">
<clearance class="1" value="0.2032"/>
</class>
<class number="2" name="HV" width="0.2032" drill="0">
<clearance class="2" value="0.3048"/>
</class>
</classes>
<parts>
<part name="MCU" library="dubec" deviceset="ATTINY13A" device="10M1"/>
<part name="12REG" library="dubec" deviceset="LTC3115-1" device="IDHD" value="LTC3115-1IDHD"/>
<part name="C1" library="dubec" deviceset="CAP" device="22UF_35V"/>
<part name="12CB1" library="dubec" deviceset="CAP" device="0.1UF_100V"/>
<part name="12CB2" library="dubec" deviceset="CAP" device="0.1UF_100V"/>
<part name="12L" library="dubec" deviceset="I" device="10UH_4.9A" value="10uH"/>
<part name="12CVCC" library="dubec" deviceset="CAP" device="10UF_10V"/>
<part name="12RT" library="dubec" deviceset="RES" device="40.2K_16" value="RES40.2K_16"/>
<part name="12RBOT" library="dubec" deviceset="RES" device="90.9K_16" value="RES90.9K_16"/>
<part name="12RFF" library="dubec" deviceset="RES" device="10K_16" value="RES10K_16"/>
<part name="12CFF" library="dubec" deviceset="CAP" device="33PF_100V" value="33pF"/>
<part name="12RFB" library="dubec" deviceset="RES" device="40.2K_16" value="RES40.2K_16"/>
<part name="12CFB" library="dubec" deviceset="CAP" device="820PF_100V" value="820pF"/>
<part name="12RTOP" library="dubec" deviceset="RES" device="1M_16" value="RES1M_16"/>
<part name="5D" library="dubec" deviceset="LED" device="GREEN_2.8V_5MA"/>
<part name="12D" library="dubec" deviceset="LED" device="GREEN_2.8V_5MA"/>
<part name="RDAUX" library="dubec" deviceset="RES" device="100_16" value="RES100_16"/>
<part name="12RLED" library="dubec" deviceset="RES" device="620_4" value="RES620_4"/>
<part name="5RLED" library="dubec" deviceset="RES" device="140_16" value="RES140_16"/>
<part name="AVRISP" library="dubec" deviceset="AVRISP" device="AVX" value="AVRISPAVX"/>
<part name="UD" library="dubec" deviceset="D_SCH" device="40V_0.25MA" value="D_SCH40V_0.25MA"/>
<part name="12TVS" library="dubec" deviceset="D_ZENER" device="15V" value="D_ZENER15V"/>
<part name="RAUXTOP" library="dubec" deviceset="RES" device="17.8K_16" value="RES17.8K_16"/>
<part name="RAUXBOT" library="dubec" deviceset="RES" device="2.74K_16" value="RES2.74K_16"/>
<part name="CMCU" library="dubec" deviceset="CAP" device="4.7UF_10V"/>
<part name="5TVS" library="dubec" deviceset="D_ZENER" device="6V" value="D_ZENER6V"/>
<part name="RS1" library="dubec" deviceset="RES" device="442_16" value="RES442_16"/>
<part name="5V_OUT" library="dubec" deviceset="M03" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="RIN1PU" library="dubec" deviceset="RES" device="40.2K_16" value="RES40.2K_16"/>
<part name="C2" library="dubec" deviceset="CAP" device="22UF_35V"/>
<part name="12V_OUT" library="dubec" deviceset="M02" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="RPD" library="dubec" deviceset="RES" device="10K_16" value="RES10K_16"/>
<part name="5L" library="dubec" deviceset="I" device="8.2UH_6.1A"/>
<part name="STVS" library="dubec" deviceset="D_ZENER" device="5.6V" value="D_ZENER5.6V"/>
<part name="5REG" library="dubec" deviceset="LT8614" device="IUDC"/>
<part name="5C1" library="dubec" deviceset="CAP" device="1UF_50V"/>
<part name="5C2" library="dubec" deviceset="CAP" device="1UF_50V"/>
<part name="5CBST" library="dubec" deviceset="CAP" device="0.1UF_100V"/>
<part name="5RTOP" library="dubec" deviceset="RES" device="3K_16" value="RES3K_16"/>
<part name="5RBOT" library="dubec" deviceset="RES" device="698_16" value="RES698_16"/>
<part name="5CTR" library="dubec" deviceset="CAP" device="1.5NF_50V"/>
<part name="5CVCC" library="dubec" deviceset="CAP" device="4.7UF_10V"/>
<part name="5RRT" library="dubec" deviceset="RES" device="40.2K_16" value="RES40.2K_16"/>
<part name="5V_PG" library="SparkFun-Passives" deviceset="TEST-POINT" device="3X4" value="TEST-POINT3X4"/>
<part name="AUX_OUT" library="SparkFun-Passives" deviceset="TEST-POINT" device="3X4" value="TEST-POINT3X4"/>
<part name="RTPAUX" library="dubec" deviceset="RES" device="442_16" value="RES442_16"/>
<part name="DAUX" library="dubec" deviceset="LED_DUAL" device="RG"/>
<part name="RDERR" library="dubec" deviceset="RES" device="210_16" value="RES210_16"/>
<part name="DTVS" library="dubec" deviceset="TVS-SPT" device="36V"/>
<part name="QR" library="dubec" deviceset="2PMOS" device="40V_8A" value="2PMOS40V_8A"/>
<part name="D2" library="dubec" deviceset="2ZD" device="18V_0.2W" value="2ZD18V_0.2W"/>
<part name="RA2" library="dubec" deviceset="RES" device="3K_16" value="RES3K_16"/>
<part name="RB2" library="dubec" deviceset="RES" device="3K_16" value="RES3K_16"/>
<part name="QA1" library="dubec" deviceset="2NMOS" device="60V_0.2A"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="DA1" library="dubec" deviceset="DIODE" device="45V_12A" value="DIODE45V_12A"/>
<part name="DB1" library="dubec" deviceset="DIODE" device="45V_12A" value="DIODE45V_12A"/>
<part name="GND_MAIN" library="dubec" deviceset="M01" device=""/>
<part name="GND_AUX" library="dubec" deviceset="M01" device=""/>
<part name="RA1" library="dubec" deviceset="RES" device="90.9K_16" value="RES90.9K_16"/>
<part name="RB1" library="dubec" deviceset="RES" device="90.9K_16" value="RES90.9K_16"/>
<part name="RA3" library="dubec" deviceset="RES" device="2M_16" value="RES2M_16"/>
<part name="QA2" library="dubec" deviceset="2NMOS" device="60V_0.2A"/>
<part name="D3" library="dubec" deviceset="D_ZENER" device="15V_0.2W_A"/>
<part name="CSNUB" library="dubec" deviceset="ECAP" device="100UF_50V"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="R12VPU" library="dubec" deviceset="RES" device="10K_16" value="RES10K_16"/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="SJ1" library="dubec" deviceset="JUMPER-PAD" device=""/>
<part name="5COUT1" library="dubec" deviceset="TCAP" device="330UF_6.3V" value="TCAP330UF_6.3V"/>
<part name="12COUT1" library="dubec" deviceset="TCAP" device="47UF_16V"/>
<part name="12COUT2" library="dubec" deviceset="CAP" device="22UF_35V"/>
<part name="5COUT2" library="dubec" deviceset="CAP" device="47UF_10V"/>
<part name="RRSPU" library="dubec" deviceset="RES" device="10K_16" value="RES10K_16"/>
<part name="U$2" library="SparkFun-Aesthetics" deviceset="AGND" device=""/>
<part name="U$3" library="SparkFun-Aesthetics" deviceset="AGND" device=""/>
<part name="U$4" library="SparkFun-Aesthetics" deviceset="AGND" device=""/>
<part name="U$5" library="SparkFun-Aesthetics" deviceset="AGND" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="U$6" library="SparkFun-Aesthetics" deviceset="AGND" device=""/>
<part name="U$7" library="SparkFun-Aesthetics" deviceset="AGND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-27.94" y="-91.44" size="1.778" layer="97" rot="R180" align="bottom-center">(UNUSED)</text>
<wire x1="-38.1" y1="-96.52" x2="-38.1" y2="-73.66" width="0.1524" layer="97"/>
<wire x1="-38.1" y1="-73.66" x2="-10.16" y2="-73.66" width="0.1524" layer="97"/>
<wire x1="10.16" y1="-10.16" x2="119.38" y2="-10.16" width="0.254" layer="94"/>
<wire x1="119.38" y1="-10.16" x2="119.38" y2="-99.06" width="0.254" layer="94"/>
<wire x1="119.38" y1="-99.06" x2="10.16" y2="-99.06" width="0.254" layer="94"/>
<wire x1="10.16" y1="-99.06" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<text x="15.24" y="-20.32" size="3.81" layer="94">12V regulator</text>
<wire x1="10.16" y1="93.98" x2="101.6" y2="93.98" width="0.254" layer="94"/>
<wire x1="101.6" y1="93.98" x2="101.6" y2="7.62" width="0.254" layer="94"/>
<wire x1="101.6" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="93.98" width="0.254" layer="94"/>
<text x="15.24" y="83.82" size="3.81" layer="94">5V regulator</text>
<wire x1="-111.76" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="-96.52" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-96.52" x2="-111.76" y2="-96.52" width="0.254" layer="94"/>
<wire x1="-111.76" y1="-96.52" x2="-111.76" y2="-10.16" width="0.254" layer="94"/>
<text x="-106.68" y="-20.32" size="3.81" layer="94">Battery switch</text>
<wire x1="-96.52" y1="93.98" x2="-10.16" y2="93.98" width="0.254" layer="94"/>
<wire x1="-10.16" y1="93.98" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="-96.52" y2="7.62" width="0.254" layer="94"/>
<wire x1="-96.52" y1="7.62" x2="-96.52" y2="93.98" width="0.254" layer="94"/>
<text x="-91.44" y="83.82" size="3.81" layer="94">Logic</text>
</plain>
<instances>
<instance part="MCU" gate="G$1" x="-38.1" y="63.5" rot="R180"/>
<instance part="12REG" gate="G$1" x="86.36" y="-58.42"/>
<instance part="C1" gate="G$1" x="-30.48" y="-60.96" rot="R90"/>
<instance part="12CB1" gate="G$1" x="104.14" y="-27.94" rot="R90"/>
<instance part="12CB2" gate="G$1" x="68.58" y="-27.94" rot="R90"/>
<instance part="12L" gate="G$1" x="86.36" y="-22.86"/>
<instance part="12CVCC" gate="G$1" x="106.68" y="-81.28" rot="R270"/>
<instance part="12RT" gate="G$1" x="68.58" y="-81.28" rot="R90"/>
<instance part="12RBOT" gate="G$1" x="48.26" y="-71.12" rot="R270"/>
<instance part="12RFF" gate="G$1" x="43.18" y="-60.96"/>
<instance part="12CFF" gate="G$1" x="38.1" y="-55.88" rot="R90"/>
<instance part="12RFB" gate="G$1" x="63.5" y="-66.04"/>
<instance part="12CFB" gate="G$1" x="58.42" y="-60.96" rot="R180"/>
<instance part="12RTOP" gate="G$1" x="50.8" y="-53.34" rot="R90"/>
<instance part="5D" gate="G$1" x="83.82" y="30.48" rot="R270"/>
<instance part="12D" gate="G$1" x="33.02" y="-66.04"/>
<instance part="RDAUX" gate="G$1" x="-76.2" y="25.4" rot="R180"/>
<instance part="12RLED" gate="G$1" x="25.4" y="-55.88" rot="R90"/>
<instance part="5RLED" gate="G$1" x="83.82" y="38.1" rot="R90"/>
<instance part="AVRISP" gate="G$1" x="-137.16" y="76.2"/>
<instance part="UD" gate="G$1" x="-60.96" y="55.88"/>
<instance part="12TVS" gate="G$1" x="30.48" y="-73.66" rot="R180"/>
<instance part="RAUXTOP" gate="G$1" x="-17.78" y="20.32" rot="R90"/>
<instance part="RAUXBOT" gate="G$1" x="-25.4" y="27.94"/>
<instance part="CMCU" gate="G$1" x="-53.34" y="45.72" rot="R90"/>
<instance part="5TVS" gate="G$1" x="76.2" y="35.56" rot="R90"/>
<instance part="RS1" gate="G$1" x="-58.42" y="66.04"/>
<instance part="5V_OUT" gate="G$1" x="114.3" y="50.8" rot="R180"/>
<instance part="GND5" gate="1" x="106.68" y="17.78"/>
<instance part="RIN1PU" gate="G$1" x="-76.2" y="71.12"/>
<instance part="C2" gate="G$1" x="-25.4" y="-60.96" rot="R90"/>
<instance part="12V_OUT" gate="G$1" x="20.32" y="-106.68" rot="MR90"/>
<instance part="GND1" gate="1" x="-116.84" y="-83.82"/>
<instance part="RPD" gate="G$1" x="-45.72" y="-17.78" rot="R180"/>
<instance part="5L" gate="G$1" x="81.28" y="50.8"/>
<instance part="STVS" gate="G$1" x="106.68" y="58.42" rot="R90"/>
<instance part="5REG" gate="G$1" x="45.72" y="55.88"/>
<instance part="5C1" gate="G$1" x="27.94" y="63.5" rot="R90"/>
<instance part="5C2" gate="G$1" x="63.5" y="63.5" rot="R90"/>
<instance part="5CBST" gate="G$1" x="66.04" y="55.88" rot="R180"/>
<instance part="5RTOP" gate="G$1" x="68.58" y="40.64" rot="R90"/>
<instance part="5RBOT" gate="G$1" x="68.58" y="30.48" rot="R90"/>
<instance part="5CTR" gate="G$1" x="25.4" y="45.72"/>
<instance part="5CVCC" gate="G$1" x="25.4" y="40.64"/>
<instance part="5RRT" gate="G$1" x="30.48" y="30.48" rot="R90"/>
<instance part="5V_PG" gate="G$1" x="27.94" y="55.88" rot="R180"/>
<instance part="AUX_OUT" gate="G$1" x="-68.58" y="30.48"/>
<instance part="RTPAUX" gate="G$1" x="-76.2" y="30.48"/>
<instance part="DAUX" gate="G$1" x="-66.04" y="22.86"/>
<instance part="RDERR" gate="G$1" x="-66.04" y="15.24"/>
<instance part="DTVS" gate="G$1" x="-43.18" y="-60.96"/>
<instance part="QR" gate="G$1" x="-86.36" y="-53.34" rot="R270"/>
<instance part="QR" gate="G$2" x="-86.36" y="-68.58" rot="MR90"/>
<instance part="D2" gate="G$1" x="-93.98" y="-40.64" rot="R180"/>
<instance part="D2" gate="G$2" x="-93.98" y="-81.28" rot="R180"/>
<instance part="RA2" gate="G$1" x="-81.28" y="-40.64"/>
<instance part="RB2" gate="G$1" x="-81.28" y="-81.28"/>
<instance part="QA1" gate="G$1" x="-66.04" y="-40.64" rot="MR270"/>
<instance part="QA1" gate="G$2" x="-53.34" y="-27.94" rot="MR270"/>
<instance part="GND6" gate="1" x="-50.8" y="-73.66"/>
<instance part="DA1" gate="G$1" x="-106.68" y="-53.34"/>
<instance part="DB1" gate="G$1" x="-106.68" y="-68.58"/>
<instance part="GND_MAIN" gate="G$1" x="-124.46" y="-60.96"/>
<instance part="GND_AUX" gate="G$1" x="-124.46" y="-76.2"/>
<instance part="RA1" gate="G$1" x="-93.98" y="-33.02"/>
<instance part="RB1" gate="G$1" x="-93.98" y="-88.9"/>
<instance part="RA3" gate="G$1" x="-81.28" y="-33.02"/>
<instance part="QA2" gate="G$1" x="-22.86" y="-86.36" rot="R270"/>
<instance part="QA2" gate="G$2" x="-66.04" y="-81.28" rot="R90"/>
<instance part="D3" gate="G$1" x="-53.34" y="-35.56" rot="R180"/>
<instance part="CSNUB" gate="G$1" x="-20.32" y="-58.42"/>
<instance part="GND7" gate="1" x="58.42" y="-96.52"/>
<instance part="R12VPU" gate="G$1" x="-76.2" y="60.96"/>
<instance part="GND9" gate="1" x="-121.92" y="63.5"/>
<instance part="SJ1" gate="G$1" x="-58.42" y="60.96"/>
<instance part="5COUT1" gate="G$1" x="93.98" y="38.1" rot="R180"/>
<instance part="12COUT1" gate="G$1" x="30.48" y="-78.74" rot="R180"/>
<instance part="12COUT2" gate="G$1" x="30.48" y="-83.82"/>
<instance part="5COUT2" gate="G$1" x="93.98" y="45.72"/>
<instance part="RRSPU" gate="G$1" x="-66.04" y="45.72"/>
<instance part="U$2" gate="G$1" x="106.68" y="-93.98"/>
<instance part="U$3" gate="G$1" x="-43.18" y="15.24"/>
<instance part="U$4" gate="G$1" x="-15.24" y="66.04"/>
<instance part="U$5" gate="G$1" x="15.24" y="12.7"/>
<instance part="GND2" gate="1" x="-53.34" y="15.24"/>
<instance part="U$6" gate="G$1" x="-55.88" y="-88.9"/>
<instance part="U$7" gate="G$1" x="-15.24" y="-91.44"/>
<instance part="GND3" gate="1" x="-30.48" y="-43.18"/>
</instances>
<busses>
</busses>
<nets>
<net name="35V" class="2">
<segment>
<pinref part="12REG" gate="G$1" pin="PVIN"/>
<wire x1="86.36" y1="-48.26" x2="101.6" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-48.26" x2="101.6" y2="-50.8" width="0.1524" layer="91"/>
<junction x="86.36" y="-48.26"/>
<pinref part="12REG" gate="G$1" pin="VIN"/>
<wire x1="101.6" y1="-71.12" x2="101.6" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-68.58" x2="86.36" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-68.58" x2="86.36" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-33.02" x2="86.36" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-33.02" x2="86.36" y2="-48.26" width="0.1524" layer="91"/>
<label x="7.62" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DTVS" gate="G$1" pin="C"/>
<wire x1="-43.18" y1="-58.42" x2="-43.18" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="QR" gate="G$2" pin="D"/>
<wire x1="-78.74" y1="-68.58" x2="-68.58" y2="-68.58" width="0.1524" layer="91"/>
<pinref part="QR" gate="G$1" pin="D"/>
<wire x1="-78.74" y1="-53.34" x2="-68.58" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="-53.34" x2="-68.58" y2="-68.58" width="0.1524" layer="91"/>
<junction x="-68.58" y="-53.34"/>
<wire x1="-68.58" y1="-53.34" x2="-43.18" y2="-53.34" width="0.1524" layer="91"/>
<junction x="-43.18" y="-53.34"/>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="CSNUB" gate="G$1" pin="+"/>
<wire x1="-43.18" y1="-53.34" x2="-30.48" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-53.34" x2="-25.4" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-53.34" x2="-20.32" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-53.34" x2="-20.32" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-53.34" x2="-25.4" y2="-55.88" width="0.1524" layer="91"/>
<junction x="-25.4" y="-53.34"/>
<wire x1="-30.48" y1="-53.34" x2="-30.48" y2="-55.88" width="0.1524" layer="91"/>
<junction x="-30.48" y="-53.34"/>
<junction x="-20.32" y="-53.34"/>
<label x="-7.62" y="-53.34" size="1.778" layer="95" xref="yes"/>
<wire x1="-20.32" y1="-53.34" x2="-7.62" y2="-53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="5REG" gate="G$1" pin="EN/UV"/>
<junction x="45.72" y="76.2"/>
<pinref part="5C2" gate="G$1" pin="2"/>
<pinref part="5REG" gate="G$1" pin="VIN2"/>
<wire x1="58.42" y1="66.04" x2="58.42" y2="68.58" width="0.1524" layer="91"/>
<wire x1="58.42" y1="68.58" x2="63.5" y2="68.58" width="0.1524" layer="91"/>
<junction x="58.42" y="68.58"/>
<wire x1="58.42" y1="68.58" x2="58.42" y2="73.66" width="0.1524" layer="91"/>
<wire x1="58.42" y1="73.66" x2="55.88" y2="76.2" width="0.1524" layer="91"/>
<wire x1="55.88" y1="76.2" x2="45.72" y2="76.2" width="0.1524" layer="91"/>
<pinref part="5C1" gate="G$1" pin="2"/>
<pinref part="5REG" gate="G$1" pin="VIN1"/>
<wire x1="33.02" y1="66.04" x2="33.02" y2="68.58" width="0.1524" layer="91"/>
<wire x1="33.02" y1="68.58" x2="27.94" y2="68.58" width="0.1524" layer="91"/>
<junction x="33.02" y="68.58"/>
<wire x1="33.02" y1="76.2" x2="33.02" y2="68.58" width="0.1524" layer="91"/>
<wire x1="33.02" y1="76.2" x2="45.72" y2="76.2" width="0.1524" layer="91"/>
<wire x1="33.02" y1="76.2" x2="7.62" y2="76.2" width="0.1524" layer="91"/>
<junction x="33.02" y="76.2"/>
<label x="7.62" y="76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="5RLED" gate="G$1" pin="2"/>
<pinref part="5L" gate="G$1" pin="2"/>
<pinref part="5TVS" gate="G$1" pin="C"/>
<pinref part="5V_OUT" gate="G$1" pin="2"/>
<wire x1="88.9" y1="50.8" x2="91.44" y2="50.8" width="0.1524" layer="91"/>
<pinref part="5REG" gate="G$1" pin="BIAS"/>
<pinref part="5RTOP" gate="G$1" pin="2"/>
<wire x1="91.44" y1="50.8" x2="111.76" y2="50.8" width="0.1524" layer="91"/>
<wire x1="58.42" y1="45.72" x2="68.58" y2="45.72" width="0.1524" layer="91"/>
<junction x="68.58" y="45.72"/>
<wire x1="68.58" y1="45.72" x2="76.2" y2="45.72" width="0.1524" layer="91"/>
<wire x1="76.2" y1="45.72" x2="83.82" y2="45.72" width="0.1524" layer="91"/>
<wire x1="76.2" y1="45.72" x2="76.2" y2="38.1" width="0.1524" layer="91"/>
<junction x="76.2" y="45.72"/>
<wire x1="83.82" y1="45.72" x2="83.82" y2="43.18" width="0.1524" layer="91"/>
<junction x="83.82" y="45.72"/>
<pinref part="5COUT2" gate="G$1" pin="1"/>
<junction x="91.44" y="45.72"/>
<wire x1="83.82" y1="45.72" x2="91.44" y2="45.72" width="0.1524" layer="91"/>
<wire x1="91.44" y1="50.8" x2="91.44" y2="45.72" width="0.1524" layer="91"/>
<junction x="91.44" y="50.8"/>
<pinref part="5COUT1" gate="G$1" pin="A"/>
<wire x1="91.44" y1="45.72" x2="91.44" y2="38.1" width="0.1524" layer="91"/>
<label x="96.52" y="50.8" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="UD" gate="G$1" pin="A"/>
<pinref part="RIN1PU" gate="G$1" pin="1"/>
<wire x1="-81.28" y1="71.12" x2="-83.82" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="71.12" x2="-83.82" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="60.96" x2="-83.82" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="55.88" x2="-63.5" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="55.88" x2="-88.9" y2="55.88" width="0.1524" layer="91"/>
<junction x="-83.82" y="55.88"/>
<label x="-99.06" y="55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R12VPU" gate="G$1" pin="1"/>
<wire x1="-88.9" y1="55.88" x2="-99.06" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="60.96" x2="-83.82" y2="60.96" width="0.1524" layer="91"/>
<junction x="-83.82" y="60.96"/>
<wire x1="-88.9" y1="55.88" x2="-88.9" y2="20.32" width="0.1524" layer="91"/>
<junction x="-88.9" y="55.88"/>
<pinref part="DAUX" gate="G$1" pin="A2"/>
<wire x1="-68.58" y1="20.32" x2="-88.9" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="12V_BST1" class="0">
<segment>
<pinref part="12REG" gate="G$1" pin="BST1"/>
<pinref part="12CB1" gate="G$1" pin="1"/>
<wire x1="101.6" y1="-55.88" x2="104.14" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-55.88" x2="104.14" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="12V" class="0">
<segment>
<pinref part="12REG" gate="G$1" pin="PVOUT"/>
<pinref part="12CFF" gate="G$1" pin="2"/>
<pinref part="12RTOP" gate="G$1" pin="2"/>
<junction x="50.8" y="-48.26"/>
<pinref part="12RLED" gate="G$1" pin="2"/>
<pinref part="12TVS" gate="G$1" pin="C"/>
<wire x1="20.32" y1="-48.26" x2="20.32" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-73.66" x2="20.32" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-48.26" x2="25.4" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-48.26" x2="38.1" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-50.8" x2="38.1" y2="-48.26" width="0.1524" layer="91"/>
<junction x="38.1" y="-48.26"/>
<wire x1="50.8" y1="-48.26" x2="58.42" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-48.26" x2="60.96" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-50.8" x2="71.12" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="12V_OUT" gate="G$1" pin="2"/>
<wire x1="38.1" y1="-48.26" x2="50.8" y2="-48.26" width="0.1524" layer="91"/>
<label x="60.96" y="-48.26" size="1.778" layer="95"/>
<wire x1="20.32" y1="-78.74" x2="20.32" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-83.82" x2="20.32" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-73.66" x2="20.32" y2="-73.66" width="0.1524" layer="91"/>
<junction x="20.32" y="-73.66"/>
<wire x1="27.94" y1="-78.74" x2="20.32" y2="-78.74" width="0.1524" layer="91"/>
<junction x="20.32" y="-78.74"/>
<label x="20.32" y="-91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="25.4" y1="-50.8" x2="25.4" y2="-48.26" width="0.1524" layer="91"/>
<junction x="25.4" y="-48.26"/>
<pinref part="12COUT1" gate="G$1" pin="A"/>
<pinref part="12COUT2" gate="G$1" pin="1"/>
<wire x1="27.94" y1="-83.82" x2="20.32" y2="-83.82" width="0.1524" layer="91"/>
<junction x="20.32" y="-83.82"/>
</segment>
</net>
<net name="12V_FB" class="0">
<segment>
<pinref part="12REG" gate="G$1" pin="FB"/>
<junction x="50.8" y="-60.96"/>
<wire x1="50.8" y1="-60.96" x2="53.34" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-63.5" x2="53.34" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-68.58" x2="55.88" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-71.12" x2="71.12" y2="-71.12" width="0.1524" layer="91"/>
<pinref part="12CFB" gate="G$1" pin="2"/>
<wire x1="53.34" y1="-60.96" x2="50.8" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="12RBOT" gate="G$1" pin="1"/>
<wire x1="48.26" y1="-66.04" x2="48.26" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-63.5" x2="50.8" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="12RTOP" gate="G$1" pin="1"/>
<wire x1="50.8" y1="-58.42" x2="50.8" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="12RFF" gate="G$1" pin="2"/>
<wire x1="48.26" y1="-60.96" x2="50.8" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="12VCC" class="0">
<segment>
<pinref part="12CVCC" gate="G$1" pin="1"/>
<pinref part="12REG" gate="G$1" pin="VCC"/>
<wire x1="106.68" y1="-78.74" x2="106.68" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-76.2" x2="101.6" y2="-76.2" width="0.1524" layer="91"/>
<pinref part="12REG" gate="G$1" pin="PVCC"/>
<pinref part="12REG" gate="G$1" pin="PWM"/>
<wire x1="106.68" y1="-40.64" x2="101.6" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-66.04" x2="101.6" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-66.04" x2="106.68" y2="-40.64" width="0.1524" layer="91"/>
<junction x="106.68" y="-66.04"/>
<wire x1="106.68" y1="-76.2" x2="106.68" y2="-66.04" width="0.1524" layer="91"/>
<junction x="106.68" y="-76.2"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="AVRISP" gate="G$1" pin="GND"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="-124.46" y1="68.58" x2="-121.92" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="68.58" x2="-121.92" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="12D" gate="G$1" pin="C"/>
<wire x1="38.1" y1="-66.04" x2="40.64" y2="-68.58" width="0.1524" layer="91"/>
<pinref part="12V_OUT" gate="G$1" pin="1"/>
<wire x1="22.86" y1="-101.6" x2="22.86" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-93.98" x2="25.4" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-91.44" x2="40.64" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="12REG" gate="G$1" pin="GND"/>
<wire x1="71.12" y1="-58.42" x2="71.12" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-60.96" x2="81.28" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-60.96" x2="81.28" y2="-91.44" width="0.1524" layer="91"/>
<junction x="81.28" y="-91.44"/>
<pinref part="12REG" gate="G$1" pin="PGND"/>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="12TVS" gate="G$1" pin="A"/>
<junction x="40.64" y="-91.44"/>
<wire x1="40.64" y1="-73.66" x2="33.02" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-91.44" x2="40.64" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-83.82" x2="40.64" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-78.74" x2="35.56" y2="-78.74" width="0.1524" layer="91"/>
<junction x="40.64" y="-78.74"/>
<wire x1="40.64" y1="-78.74" x2="40.64" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-91.44" x2="58.42" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-91.44" x2="81.28" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-91.44" x2="86.36" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-86.36" x2="86.36" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-68.58" x2="40.64" y2="-73.66" width="0.1524" layer="91"/>
<junction x="40.64" y="-73.66"/>
<wire x1="58.42" y1="-91.44" x2="58.42" y2="-93.98" width="0.1524" layer="91"/>
<junction x="58.42" y="-91.44"/>
<pinref part="12COUT1" gate="G$1" pin="C"/>
<pinref part="12COUT2" gate="G$1" pin="2"/>
<wire x1="35.56" y1="-83.82" x2="40.64" y2="-83.82" width="0.1524" layer="91"/>
<junction x="40.64" y="-83.82"/>
</segment>
<segment>
<wire x1="-119.38" y1="-60.96" x2="-116.84" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="-60.96" x2="-116.84" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="-76.2" x2="-119.38" y2="-76.2" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-116.84" y1="-76.2" x2="-116.84" y2="-81.28" width="0.1524" layer="91"/>
<junction x="-116.84" y="-76.2"/>
<pinref part="GND_MAIN" gate="G$1" pin="1"/>
<pinref part="GND_AUX" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="5TVS" gate="G$1" pin="A"/>
<pinref part="5D" gate="G$1" pin="C"/>
<wire x1="20.32" y1="22.86" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<wire x1="76.2" y1="22.86" x2="76.2" y2="33.02" width="0.1524" layer="91"/>
<pinref part="5C1" gate="G$1" pin="1"/>
<pinref part="5REG" gate="G$1" pin="GND1"/>
<wire x1="27.94" y1="60.96" x2="33.02" y2="60.96" width="0.1524" layer="91"/>
<pinref part="5REG" gate="G$1" pin="GND2"/>
<pinref part="5C2" gate="G$1" pin="1"/>
<wire x1="58.42" y1="60.96" x2="63.5" y2="60.96" width="0.1524" layer="91"/>
<wire x1="58.42" y1="58.42" x2="58.42" y2="60.96" width="0.1524" layer="91"/>
<junction x="58.42" y="60.96"/>
<pinref part="5REG" gate="G$1" pin="GND"/>
<wire x1="58.42" y1="35.56" x2="58.42" y2="22.86" width="0.1524" layer="91"/>
<wire x1="27.94" y1="60.96" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<wire x1="20.32" y1="60.96" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<junction x="27.94" y="60.96"/>
<wire x1="58.42" y1="58.42" x2="33.02" y2="58.42" width="0.1524" layer="91"/>
<wire x1="33.02" y1="60.96" x2="33.02" y2="58.42" width="0.1524" layer="91"/>
<junction x="33.02" y="60.96"/>
<pinref part="5REG" gate="G$1" pin="SYNC/MODE"/>
<wire x1="33.02" y1="50.8" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<junction x="20.32" y="50.8"/>
<pinref part="5COUT1" gate="G$1" pin="C"/>
<pinref part="5COUT2" gate="G$1" pin="2"/>
<wire x1="99.06" y1="38.1" x2="99.06" y2="45.72" width="0.1524" layer="91"/>
<wire x1="99.06" y1="38.1" x2="99.06" y2="22.86" width="0.1524" layer="91"/>
<junction x="99.06" y="38.1"/>
<wire x1="106.68" y1="22.86" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
<pinref part="5V_OUT" gate="G$1" pin="3"/>
<wire x1="106.68" y1="48.26" x2="111.76" y2="48.26" width="0.1524" layer="91"/>
<pinref part="STVS" gate="G$1" pin="A"/>
<wire x1="106.68" y1="55.88" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
<junction x="106.68" y="48.26"/>
<wire x1="106.68" y1="20.32" x2="106.68" y2="22.86" width="0.1524" layer="91"/>
<junction x="106.68" y="22.86"/>
<wire x1="20.32" y1="22.86" x2="58.42" y2="22.86" width="0.1524" layer="91"/>
<wire x1="58.42" y1="22.86" x2="76.2" y2="22.86" width="0.1524" layer="91"/>
<wire x1="76.2" y1="22.86" x2="83.82" y2="22.86" width="0.1524" layer="91"/>
<wire x1="83.82" y1="22.86" x2="99.06" y2="22.86" width="0.1524" layer="91"/>
<wire x1="99.06" y1="22.86" x2="106.68" y2="22.86" width="0.1524" layer="91"/>
<junction x="58.42" y="22.86"/>
<junction x="76.2" y="22.86"/>
<wire x1="83.82" y1="25.4" x2="83.82" y2="22.86" width="0.1524" layer="91"/>
<junction x="83.82" y="22.86"/>
<junction x="99.06" y="22.86"/>
</segment>
<segment>
<pinref part="CSNUB" gate="G$1" pin="-"/>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="DTVS" gate="G$1" pin="A1"/>
<wire x1="-50.8" y1="-60.96" x2="-50.8" y2="-66.04" width="0.1524" layer="91"/>
<junction x="-50.8" y="-66.04"/>
<junction x="-50.8" y="-66.04"/>
<wire x1="-50.8" y1="-66.04" x2="-50.8" y2="-71.12" width="0.1524" layer="91"/>
<pinref part="DTVS" gate="G$1" pin="A2"/>
<wire x1="-35.56" y1="-60.96" x2="-35.56" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="-66.04" x2="-50.8" y2="-66.04" width="0.1524" layer="91"/>
<junction x="-35.56" y="-66.04"/>
<wire x1="-35.56" y1="-66.04" x2="-30.48" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-30.48" y1="-63.5" x2="-30.48" y2="-66.04" width="0.1524" layer="91"/>
<junction x="-30.48" y="-66.04"/>
<wire x1="-30.48" y1="-66.04" x2="-25.4" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="-63.5" x2="-25.4" y2="-66.04" width="0.1524" layer="91"/>
<junction x="-25.4" y="-66.04"/>
<wire x1="-25.4" y1="-66.04" x2="-20.32" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-66.04" x2="-20.32" y2="-63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-53.34" y1="17.78" x2="-53.34" y2="25.4" width="0.1524" layer="91"/>
<pinref part="DAUX" gate="G$1" pin="C1"/>
<wire x1="-60.96" y1="25.4" x2="-53.34" y2="25.4" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="A"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="-50.8" y1="-35.56" x2="-30.48" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-35.56" x2="-30.48" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="RPD" gate="G$1" pin="1"/>
<wire x1="-40.64" y1="-17.78" x2="-30.48" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-17.78" x2="-30.48" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-30.48" y="-35.56"/>
</segment>
</net>
<net name="5V_PG" class="0">
<segment>
<pinref part="5V_PG" gate="G$1" pin="1"/>
<pinref part="5REG" gate="G$1" pin="PG"/>
<wire x1="27.94" y1="55.88" x2="33.02" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IN_1" class="0">
<segment>
<pinref part="STVS" gate="G$1" pin="C"/>
<wire x1="106.68" y1="60.96" x2="106.68" y2="63.5" width="0.1524" layer="91"/>
<pinref part="5V_OUT" gate="G$1" pin="1"/>
<wire x1="111.76" y1="68.58" x2="111.76" y2="63.5" width="0.1524" layer="91"/>
<wire x1="111.76" y1="63.5" x2="111.76" y2="53.34" width="0.1524" layer="91"/>
<label x="111.76" y="68.58" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="106.68" y1="63.5" x2="111.76" y2="63.5" width="0.1524" layer="91"/>
<junction x="111.76" y="63.5"/>
</segment>
<segment>
<pinref part="RS1" gate="G$1" pin="1"/>
<label x="-99.06" y="76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="RIN1PU" gate="G$1" pin="2"/>
<wire x1="-71.12" y1="71.12" x2="-66.04" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="66.04" x2="-63.5" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="71.12" x2="-71.12" y2="76.2" width="0.1524" layer="91"/>
<junction x="-71.12" y="71.12"/>
<wire x1="-71.12" y1="76.2" x2="-99.06" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V_MCU" class="0">
<segment>
<pinref part="MCU" gate="G$1" pin="VCC"/>
<pinref part="CMCU" gate="G$1" pin="2"/>
<pinref part="UD" gate="G$1" pin="C"/>
<wire x1="-58.42" y1="55.88" x2="-53.34" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="55.88" x2="-50.8" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="50.8" x2="-53.34" y2="55.88" width="0.1524" layer="91"/>
<junction x="-53.34" y="55.88"/>
<label x="-53.34" y="55.88" size="1.778" layer="95"/>
<pinref part="RRSPU" gate="G$1" pin="2"/>
<wire x1="-60.96" y1="45.72" x2="-58.42" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="45.72" x2="-58.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="50.8" x2="-53.34" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="AVRISP" gate="G$1" pin="VCC"/>
<wire x1="-124.46" y1="83.82" x2="-121.92" y2="83.82" width="0.1524" layer="91"/>
<label x="-121.92" y="83.82" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="ADC2" class="0">
<segment>
<pinref part="RAUXTOP" gate="G$1" pin="2"/>
<pinref part="RAUXBOT" gate="G$1" pin="2"/>
<wire x1="-17.78" y1="27.94" x2="-17.78" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="27.94" x2="-17.78" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="27.94" x2="-17.78" y2="27.94" width="0.1524" layer="91"/>
<junction x="-17.78" y="27.94"/>
<pinref part="MCU" gate="G$1" pin="PB4"/>
<wire x1="-25.4" y1="66.04" x2="-17.78" y2="58.42" width="0.1524" layer="91"/>
<label x="-25.4" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="12CFB" gate="G$1" pin="1"/>
<pinref part="12RFB" gate="G$1" pin="1"/>
<wire x1="60.96" y1="-60.96" x2="60.96" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-63.5" x2="58.42" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-63.5" x2="58.42" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="12V_COMP" class="0">
<segment>
<pinref part="12RFB" gate="G$1" pin="2"/>
<pinref part="12REG" gate="G$1" pin="VC"/>
<wire x1="68.58" y1="-66.04" x2="71.12" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="12CFF" gate="G$1" pin="1"/>
<pinref part="12RFF" gate="G$1" pin="1"/>
<wire x1="38.1" y1="-58.42" x2="38.1" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="12V_RT" class="0">
<segment>
<pinref part="12RT" gate="G$1" pin="2"/>
<pinref part="12REG" gate="G$1" pin="RT"/>
<wire x1="68.58" y1="-76.2" x2="71.12" y2="-76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="12D" gate="G$1" pin="A"/>
<pinref part="12RLED" gate="G$1" pin="1"/>
<wire x1="30.48" y1="-66.04" x2="25.4" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-66.04" x2="25.4" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="12V_RUN" class="0">
<segment>
<label x="-99.06" y="66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R12VPU" gate="G$1" pin="2"/>
<wire x1="-66.04" y1="60.96" x2="-71.12" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="66.04" x2="-99.06" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="60.96" x2="-66.04" y2="60.96" width="0.1524" layer="91"/>
<junction x="-66.04" y="60.96"/>
<pinref part="SJ1" gate="G$1" pin="1"/>
<wire x1="-63.5" y1="60.96" x2="-66.04" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="12REG" gate="G$1" pin="RUN"/>
<wire x1="71.12" y1="-40.64" x2="7.62" y2="-40.64" width="0.1524" layer="91"/>
<label x="7.62" y="-40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="RS1" gate="G$1" pin="2"/>
<pinref part="MCU" gate="G$1" pin="PB1"/>
<wire x1="-53.34" y1="66.04" x2="-50.8" y2="66.04" width="0.1524" layer="91"/>
<label x="-53.34" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="AVRISP" gate="G$1" pin="MISO"/>
<wire x1="-152.4" y1="83.82" x2="-149.86" y2="83.82" width="0.1524" layer="91"/>
<label x="-152.4" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="MCU" gate="G$1" pin="PB0"/>
<wire x1="-50.8" y1="71.12" x2="-55.88" y2="71.12" width="0.1524" layer="91"/>
<label x="-55.88" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="AVRISP" gate="G$1" pin="MOSI"/>
<wire x1="-124.46" y1="76.2" x2="-121.92" y2="76.2" width="0.1524" layer="91"/>
<label x="-121.92" y="76.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RDERR" gate="G$1" pin="1"/>
<wire x1="-71.12" y1="15.24" x2="-76.2" y2="15.24" width="0.1524" layer="91"/>
<label x="-76.2" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="5V_SW" class="0">
<segment>
<wire x1="68.58" y1="55.88" x2="71.12" y2="55.88" width="0.1524" layer="91"/>
<wire x1="71.12" y1="55.88" x2="71.12" y2="50.8" width="0.1524" layer="91"/>
<pinref part="5L" gate="G$1" pin="1"/>
<pinref part="5REG" gate="G$1" pin="SW"/>
<pinref part="5CBST" gate="G$1" pin="1"/>
<wire x1="71.12" y1="50.8" x2="73.66" y2="50.8" width="0.1524" layer="91"/>
<wire x1="58.42" y1="50.8" x2="71.12" y2="50.8" width="0.1524" layer="91"/>
<junction x="71.12" y="50.8"/>
</segment>
</net>
<net name="12V_SW1" class="0">
<segment>
<pinref part="12CB1" gate="G$1" pin="2"/>
<pinref part="12L" gate="G$1" pin="2"/>
<wire x1="104.14" y1="-22.86" x2="99.06" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="12REG" gate="G$1" pin="SW1"/>
<wire x1="99.06" y1="-22.86" x2="93.98" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-22.86" x2="99.06" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-43.18" x2="101.6" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-43.18" x2="101.6" y2="-45.72" width="0.1524" layer="91"/>
<junction x="99.06" y="-22.86"/>
</segment>
</net>
<net name="12V_SW2" class="0">
<segment>
<pinref part="12REG" gate="G$1" pin="SW2"/>
<pinref part="12CB2" gate="G$1" pin="2"/>
<pinref part="12L" gate="G$1" pin="1"/>
<wire x1="68.58" y1="-22.86" x2="73.66" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-22.86" x2="78.74" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-22.86" x2="73.66" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-43.18" x2="71.12" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-43.18" x2="71.12" y2="-45.72" width="0.1524" layer="91"/>
<junction x="73.66" y="-22.86"/>
</segment>
</net>
<net name="12V_BST2" class="0">
<segment>
<pinref part="12REG" gate="G$1" pin="BST2"/>
<pinref part="12CB2" gate="G$1" pin="1"/>
<wire x1="101.6" y1="-60.96" x2="101.6" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-58.42" x2="91.44" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-58.42" x2="91.44" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-30.48" x2="68.58" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V_BST" class="0">
<segment>
<pinref part="5REG" gate="G$1" pin="BST"/>
<pinref part="5CBST" gate="G$1" pin="2"/>
<wire x1="58.42" y1="55.88" x2="60.96" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V_FB" class="0">
<segment>
<pinref part="5REG" gate="G$1" pin="FB"/>
<pinref part="5RTOP" gate="G$1" pin="1"/>
<wire x1="63.5" y1="35.56" x2="68.58" y2="35.56" width="0.1524" layer="91"/>
<pinref part="5RBOT" gate="G$1" pin="2"/>
<junction x="68.58" y="35.56"/>
<wire x1="58.42" y1="40.64" x2="63.5" y2="40.64" width="0.1524" layer="91"/>
<wire x1="63.5" y1="40.64" x2="63.5" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V_TR" class="0">
<segment>
<pinref part="5CTR" gate="G$1" pin="2"/>
<pinref part="5REG" gate="G$1" pin="TR/SS"/>
<wire x1="30.48" y1="45.72" x2="33.02" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V_VCC" class="0">
<segment>
<pinref part="5CVCC" gate="G$1" pin="2"/>
<pinref part="5REG" gate="G$1" pin="INTVCC"/>
<wire x1="30.48" y1="40.64" x2="33.02" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V_RT" class="0">
<segment>
<pinref part="5RRT" gate="G$1" pin="2"/>
<pinref part="5REG" gate="G$1" pin="RT"/>
<wire x1="30.48" y1="35.56" x2="33.02" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="5D" gate="G$1" pin="A"/>
<pinref part="5RLED" gate="G$1" pin="1"/>
</segment>
</net>
<net name="TP_AUX" class="0">
<segment>
<pinref part="RTPAUX" gate="G$1" pin="2"/>
<wire x1="-68.58" y1="30.48" x2="-71.12" y2="30.48" width="0.1524" layer="91"/>
<pinref part="AUX_OUT" gate="G$1" pin="1"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="MCU" gate="G$1" pin="PB5"/>
<wire x1="-25.4" y1="55.88" x2="-22.86" y2="53.34" width="0.1524" layer="91"/>
<label x="-25.4" y="55.88" size="1.778" layer="95"/>
<wire x1="-22.86" y1="53.34" x2="-22.86" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="43.18" x2="-25.4" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="40.64" x2="-73.66" y2="40.64" width="0.1524" layer="91"/>
<label x="-99.06" y="40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="RRSPU" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="40.64" x2="-99.06" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="45.72" x2="-73.66" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="45.72" x2="-73.66" y2="40.64" width="0.1524" layer="91"/>
<junction x="-73.66" y="40.64"/>
</segment>
<segment>
<pinref part="AVRISP" gate="G$1" pin="RESET"/>
<wire x1="-149.86" y1="68.58" x2="-152.4" y2="68.58" width="0.1524" layer="91"/>
<label x="-152.4" y="68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GATE_MAIN" class="2">
<segment>
<pinref part="QR" gate="G$1" pin="G"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="-91.44" y1="-40.64" x2="-88.9" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="-40.64" x2="-88.9" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="RA2" gate="G$1" pin="1"/>
<wire x1="-88.9" y1="-40.64" x2="-86.36" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-88.9" y="-40.64"/>
<pinref part="RA1" gate="G$1" pin="2"/>
<wire x1="-88.9" y1="-33.02" x2="-88.9" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="-33.02" x2="-86.36" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-88.9" y="-33.02"/>
<pinref part="RA3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GATE_AUX" class="2">
<segment>
<pinref part="D2" gate="G$2" pin="A"/>
<pinref part="QR" gate="G$2" pin="G"/>
<wire x1="-91.44" y1="-81.28" x2="-88.9" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="-81.28" x2="-88.9" y2="-76.2" width="0.1524" layer="91"/>
<pinref part="RB2" gate="G$1" pin="1"/>
<wire x1="-88.9" y1="-81.28" x2="-86.36" y2="-81.28" width="0.1524" layer="91"/>
<junction x="-88.9" y="-81.28"/>
<pinref part="RB1" gate="G$1" pin="2"/>
<wire x1="-88.9" y1="-81.28" x2="-88.9" y2="-88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="2">
<segment>
<pinref part="RA2" gate="G$1" pin="2"/>
<pinref part="QA1" gate="G$1" pin="D"/>
<wire x1="-76.2" y1="-40.64" x2="-73.66" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BATT_MAIN" class="2">
<segment>
<pinref part="DA1" gate="G$1" pin="A"/>
<wire x1="-119.38" y1="-53.34" x2="-109.22" y2="-53.34" width="0.1524" layer="91"/>
<label x="-119.38" y="-53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BATT_AUX" class="2">
<segment>
<pinref part="DB1" gate="G$1" pin="A"/>
<wire x1="-119.38" y1="-68.58" x2="-109.22" y2="-68.58" width="0.1524" layer="91"/>
<label x="-119.38" y="-68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RAUXTOP" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="2.54" x2="-17.78" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="2.54" x2="-17.78" y2="15.24" width="0.1524" layer="91"/>
<label x="-20.32" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GATE_MAIN_B" class="2">
<segment>
<pinref part="QA1" gate="G$1" pin="G"/>
<wire x1="-76.2" y1="-33.02" x2="-66.04" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="RA3" gate="G$1" pin="2"/>
<wire x1="-66.04" y1="-33.02" x2="-60.96" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-66.04" y="-33.02"/>
<wire x1="-60.96" y1="-33.02" x2="-58.42" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="-35.56" x2="-55.88" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="C"/>
<pinref part="QA1" gate="G$2" pin="D"/>
<wire x1="-60.96" y1="-27.94" x2="-60.96" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-60.96" y="-33.02"/>
</segment>
</net>
<net name="LED_GREEN" class="0">
<segment>
<pinref part="RDAUX" gate="G$1" pin="1"/>
<pinref part="DAUX" gate="G$1" pin="A1"/>
<wire x1="-71.12" y1="25.4" x2="-68.58" y2="25.4" width="0.1524" layer="91"/>
<label x="-73.66" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="SRC_MAIN" class="2">
<segment>
<pinref part="DA1" gate="G$1" pin="C"/>
<pinref part="QR" gate="G$1" pin="S"/>
<wire x1="-104.14" y1="-53.34" x2="-99.06" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="RA1" gate="G$1" pin="1"/>
<wire x1="-99.06" y1="-53.34" x2="-93.98" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="-33.02" x2="-99.06" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-99.06" y="-53.34"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="-99.06" y1="-40.64" x2="-99.06" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-40.64" x2="-99.06" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-99.06" y="-40.64"/>
</segment>
</net>
<net name="SRC_AUX" class="2">
<segment>
<pinref part="DB1" gate="G$1" pin="C"/>
<pinref part="QR" gate="G$2" pin="S"/>
<wire x1="-104.14" y1="-68.58" x2="-99.06" y2="-68.58" width="0.1524" layer="91"/>
<pinref part="RB1" gate="G$1" pin="1"/>
<wire x1="-99.06" y1="-68.58" x2="-93.98" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="-88.9" x2="-99.06" y2="-81.28" width="0.1524" layer="91"/>
<junction x="-99.06" y="-68.58"/>
<pinref part="D2" gate="G$2" pin="C"/>
<wire x1="-99.06" y1="-81.28" x2="-99.06" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-81.28" x2="-99.06" y2="-81.28" width="0.1524" layer="91"/>
<junction x="-99.06" y="-81.28"/>
</segment>
</net>
<net name="N$10" class="2">
<segment>
<pinref part="RB2" gate="G$1" pin="2"/>
<pinref part="QA2" gate="G$2" pin="D"/>
<wire x1="-76.2" y1="-81.28" x2="-73.66" y2="-81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AUX" class="0">
<segment>
<wire x1="-83.82" y1="35.56" x2="-25.4" y2="35.56" width="0.1524" layer="91"/>
<pinref part="MCU" gate="G$1" pin="PB3"/>
<wire x1="-25.4" y1="35.56" x2="-20.32" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="40.64" x2="-20.32" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="55.88" x2="-25.4" y2="60.96" width="0.1524" layer="91"/>
<pinref part="RTPAUX" gate="G$1" pin="1"/>
<wire x1="-83.82" y1="35.56" x2="-83.82" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="30.48" x2="-83.82" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="25.4" x2="-81.28" y2="25.4" width="0.1524" layer="91"/>
<pinref part="RDAUX" gate="G$1" pin="2"/>
<wire x1="-83.82" y1="35.56" x2="-99.06" y2="35.56" width="0.1524" layer="91"/>
<junction x="-83.82" y="35.56"/>
<label x="-99.06" y="35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
<label x="-25.4" y="60.96" size="1.778" layer="95"/>
<wire x1="-81.28" y1="30.48" x2="-83.82" y2="30.48" width="0.1524" layer="91"/>
<junction x="-83.82" y="30.48"/>
</segment>
<segment>
<wire x1="-66.04" y1="-88.9" x2="-66.04" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="-101.6" x2="-63.5" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="QA2" gate="G$2" pin="G"/>
<label x="-63.5" y="-101.6" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RPD" gate="G$1" pin="2"/>
<pinref part="QA1" gate="G$2" pin="G"/>
<wire x1="-53.34" y1="-17.78" x2="-53.34" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-17.78" x2="-50.8" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-53.34" y="-17.78"/>
<wire x1="-50.8" y1="-5.08" x2="-53.34" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-5.08" x2="-53.34" y2="-17.78" width="0.1524" layer="91"/>
<label x="-50.8" y="-5.08" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="SJ1" gate="G$1" pin="2"/>
<pinref part="MCU" gate="G$1" pin="PB2"/>
<wire x1="-53.34" y1="60.96" x2="-50.8" y2="60.96" width="0.1524" layer="91"/>
<label x="-53.34" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="AVRISP" gate="G$1" pin="SCK"/>
<wire x1="-149.86" y1="76.2" x2="-152.4" y2="76.2" width="0.1524" layer="91"/>
<label x="-152.4" y="76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AGND" class="0">
<segment>
<pinref part="12RBOT" gate="G$1" pin="2"/>
<wire x1="48.26" y1="-76.2" x2="48.26" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-88.9" x2="68.58" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="12RT" gate="G$1" pin="1"/>
<wire x1="68.58" y1="-88.9" x2="68.58" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="12CVCC" gate="G$1" pin="2"/>
<wire x1="68.58" y1="-88.9" x2="106.68" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-88.9" x2="106.68" y2="-86.36" width="0.1524" layer="91"/>
<junction x="68.58" y="-88.9"/>
<pinref part="U$2" gate="G$1" pin="AGND"/>
<wire x1="106.68" y1="-88.9" x2="106.68" y2="-91.44" width="0.1524" layer="91"/>
<junction x="106.68" y="-88.9"/>
</segment>
<segment>
<wire x1="-17.78" y1="71.12" x2="-15.24" y2="68.58" width="0.1524" layer="91"/>
<pinref part="MCU" gate="G$1" pin="GND"/>
<wire x1="-25.4" y1="71.12" x2="-17.78" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="RAUXBOT" gate="G$1" pin="1"/>
<pinref part="CMCU" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="27.94" x2="-30.48" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="43.18" x2="-53.34" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="38.1" x2="-43.18" y2="27.94" width="0.1524" layer="91"/>
<junction x="-43.18" y="27.94"/>
<wire x1="-43.18" y1="17.78" x2="-43.18" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="5RBOT" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="AGND"/>
<wire x1="68.58" y1="25.4" x2="68.58" y2="17.78" width="0.1524" layer="91"/>
<wire x1="68.58" y1="17.78" x2="30.48" y2="17.78" width="0.1524" layer="91"/>
<wire x1="30.48" y1="17.78" x2="15.24" y2="17.78" width="0.1524" layer="91"/>
<wire x1="15.24" y1="17.78" x2="15.24" y2="40.64" width="0.1524" layer="91"/>
<wire x1="15.24" y1="17.78" x2="15.24" y2="15.24" width="0.1524" layer="91"/>
<junction x="15.24" y="17.78"/>
<pinref part="5CVCC" gate="G$1" pin="1"/>
<wire x1="22.86" y1="40.64" x2="15.24" y2="40.64" width="0.1524" layer="91"/>
<pinref part="5CTR" gate="G$1" pin="1"/>
<wire x1="22.86" y1="45.72" x2="15.24" y2="45.72" width="0.1524" layer="91"/>
<wire x1="15.24" y1="45.72" x2="15.24" y2="40.64" width="0.1524" layer="91"/>
<junction x="15.24" y="40.64"/>
<pinref part="5RRT" gate="G$1" pin="1"/>
<wire x1="30.48" y1="25.4" x2="30.48" y2="17.78" width="0.1524" layer="91"/>
<junction x="30.48" y="17.78"/>
</segment>
<segment>
<pinref part="QA1" gate="G$1" pin="S"/>
<wire x1="-58.42" y1="-40.64" x2="-55.88" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-40.64" x2="-55.88" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-40.64" x2="-38.1" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-55.88" y="-40.64"/>
<pinref part="QA1" gate="G$2" pin="S"/>
<pinref part="QA2" gate="G$2" pin="S"/>
<wire x1="-58.42" y1="-81.28" x2="-55.88" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="-40.64" x2="-38.1" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="AGND"/>
<wire x1="-55.88" y1="-81.28" x2="-55.88" y2="-86.36" width="0.1524" layer="91"/>
<junction x="-55.88" y="-81.28"/>
<wire x1="-45.72" y1="-27.94" x2="-38.1" y2="-27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="QA2" gate="G$1" pin="D"/>
<junction x="-15.24" y="-86.36"/>
<pinref part="QA2" gate="G$1" pin="G"/>
<pinref part="QA2" gate="G$1" pin="S"/>
<wire x1="-30.48" y1="-86.36" x2="-30.48" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-78.74" x2="-22.86" y2="-78.74" width="0.1524" layer="91"/>
<junction x="-22.86" y="-78.74"/>
<wire x1="-22.86" y1="-78.74" x2="-15.24" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-78.74" x2="-15.24" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="U$7" gate="G$1" pin="AGND"/>
<wire x1="-15.24" y1="-88.9" x2="-15.24" y2="-86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED_RED" class="0">
<segment>
<pinref part="DAUX" gate="G$1" pin="C2"/>
<wire x1="-60.96" y1="20.32" x2="-58.42" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="20.32" x2="-58.42" y2="15.24" width="0.1524" layer="91"/>
<pinref part="RDERR" gate="G$1" pin="2"/>
<wire x1="-58.42" y1="15.24" x2="-60.96" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
