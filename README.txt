
Contact:
Janne Savukoski
janne@savukoski.name
+358405684246


                    Part name: DUBEC
                     Revision: 1.4
                   Board size: 40mm x 21.375mm
              Board thickness: 1.6mm
                   Board type: Four layer
       Outer copper thickness:
       Inner copper thickness:
Solder mask color (top & bot):
 Silkscreen color (top & bot):


No special instructions.


Layer order:

TOP:    DUBEC.GTL
        DUBEC.GL2
        DUBEC.GL5
BOTTOM: DUBEC.GBL


Other files:

DUBEC.GML  Outline
DUBEC.GTS  Solder mask TOP
DUBEC.GBS  Solder mask BOTTOM
DUBEC.GTO  Silkscreen TOP
DUBEC.GBO  Silkscreen BOTTOM
DUBEC.drd  Excellon drill
DUBEC.drl  Drill rack
DUBEC.GTP  Solder stencil (TOP)


Thank you!

Regards,
Janne Savukoski
